angular.module('trackingApp', ['ui.bootstrap', 'ui.select', 'linkify', 'ngFileUpload', 'youtube-embed', 'infinite-scroll'])
    .controller('TrackingController', ['$scope', '$http', 'Upload', function ($scope, $http, Upload) {
        var Reddit = function () {
            this.items = [];
            this.busy = false;
            this.page = 0;
            this.query = '';
            this.timeoutObj = null;
            this.passTime = true;
        };
        $scope.queryText = '';

        Reddit.prototype.nextPage = function () {
            if (this.busy) return;

            if (this.timeoutObj != null) {
                clearTimeout(this.timeoutObj);
                this.timeoutObj = null;
            }
            else {
                this.passTime=false;
                this.timeoutObj = setTimeout(function () {
                    $scope.reddit.passTime = true;
                    $scope.reddit.nextPage();
                }, 1000);
            }


            if (this.passTime == false) return;

            this.busy = true;
            this.passTime = true;

            try {
                if (this.query != $scope.queryText) {
                    this.page = 0;
                    this.query = $scope.queryText;
                    this.items = [];
                }
                if ($scope.queryText == '') {
                    var url = "/admin/royalproject/json?limit=25&page=" + this.page;
                    $http.get(url).success(function (data) {
                        var items = data;
                        for (var i = 0; i < items.length; i++) {
                            this.items.push(items[i]);
                        }
                        //this.after = "t3_" + this.items[this.items.length - 1].id;
                        this.page++;
                        this.busy = false;
                    }.bind(this));
                } else {

                    var url = "/admin/royalproject/search?limit=25&page=" + this.page + "&query=" + this.query;
                    $http.get(url).success(function (data) {
                        var items = data;
                        for (var i = 0; i < items.length; i++) {
                            this.items.push(items[i]);
                        }
                        //this.after = "t3_" + this.items[this.items.length - 1].id;
                        this.page++;
                        this.busy = false;
                    }.bind(this));
                }
            } catch (e) {
                this.busy = false;

            }

        };


        $scope.reddit = new Reddit();

        $scope.hasPreview = false;
        $scope.item = {};
        $scope.list = [];
        $scope.removeID = '';
        $scope.deleteFileItem = [];
        $scope.deleteImageItem = [];
        $scope.isLoading = false;
        $scope.progressPercentage = 0;
        $scope.canEdit = true;


        var init = function () {
            var date = new Date();
            //$scope.text = '';
            $scope.item.name = '';
            $scope.item.status = '';

            $scope.item.listDate = date.getDate() + '-' + (date.getMonth() + 1) + '-' + (date.getFullYear() + 543);

            var params = {page: 0, limit: 1};
        };
        init();

        $scope.refreshIdea = function (address) {
            var params = {query: address};
            return $http.get('/admin/royalproject/search', {params: params})
                .then(function (response) {
                    $scope.projects = response.data;
                });
        };

        $scope.query = function () {
            var text = $scope.item.status;
            $http.get('/admin/tracking/parseText', {
                    params: {
                        query: text
                    }
                }
            ).
                success(function (data) {
                    if (data.image != null)
                        $scope.hasPreview = true;
                    else $scope.hasPreview = false;

                    $scope.name = data.name;
                    $scope.image = data.image;
                    $scope.description = data.description;
                    $scope.link = data.link;
                });
        }
        $scope.loadTimelineItem = function (i) {
            $scope.item.project = i;
            $scope.loadTimeline();
            window.scrollTo(0, 0);
        };
        $scope.loadTimeline = function () {
            $http.get('/admin/tracking/' + $scope.item.project.uuid).success(function (data) {
                for (var i = 0; i < data.length; i++) {
                    // get can edit from first project of the tracker

                    var obj = data[i];

                    if (i == 0) $scope.canEdit = obj.canEdit;

                    var listDate = obj.listDate;
                    listDate = new Date(listDate);
                    obj.listDateYear = listDate.getFullYear() + 543;
                }
                $scope.list = data;
                setTimeout(function () {
                        $("[rel=tooltip]").tooltip()
                    }
                    , 1000);
            });
        }
        $scope.postStatus = function () {
            var item = $scope.item;
            var status = {
                'name': item.name,
                'status': item.status,
                'listDate': parseDate(item.listDate)
            }
            $http.post('/admin/tracking/' + $scope.item.project.uuid, status).success(function () {
                $scope.loadTimeline();
                $scope.item.name = '';
                $scope.item.status = '';
                $.smallBox({
                    title: "เพิ่มความคืบหน้าโครงการเรียบร้อย",
                    content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
                    color: "#49b4441",
                    iconSmall: "fa fa-save bounce animated",
                    timeout: 4000
                })

            });

        }
        $scope.updateStatus = function () {
            var item = $scope.item;
            var status = {
                'name': $scope.editItem.name,
                'status': $scope.editItem.status,
                'listDate': parseDate($scope.editItem.listDate)
            }
            $http.put('/admin/tracking/tracker/' + $scope.editItem.uuid, status).success(function () {
                $.smallBox({
                    title: "แก้ไขความคืบหน้าโครงการเรียบร้อย",
                    content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
                    color: "#49b4441",
                    iconSmall: "fa fa-save bounce animated",
                    timeout: 4000
                });
                setTimeout(function () {
                    $scope.loadTimeline();
                }, 2000);

            });

        }

        $scope.removePost = function (id) {
            $scope.removeID = id;
        }

        $scope.removePostConfirm = function () {
            $http.delete('/admin/tracking/tracker/' + $scope.removeID).success(function () {
                $.smallBox({
                    title: "ลบการติดตามโครงการเรียบร้อย",
                    content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
                    color: "#49b4441",
                    iconSmall: "fa fa-trash-o bounce animated",
                    timeout: 4000
                });
                $scope.loadTimeline();
            });
        }

        $scope.showUpload = function (id) {
            var list = $scope.list;
            for (var i = 0; i < list.length; i++) {
                var obj = list[i];
                if (obj.uuid == id) {
                    //alert('yeah');
                    obj.hasUpload = true;
                }
            }
            $scope.list = list;
        }

        var counter = 0;

        $scope.upload = function (files, id) {
            if (!$scope.canEdit) {
                $.smallBox({
                    title: "คุณไม่สามารถอัพโหลดไฟล์เข้าไปในติดตามโครงการ โครงการนี้ได้",
                    content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
                    color: "#49b4441",
                    iconSmall: "fa fa-trash-o bounce animated",
                    timeout: 4000
                });
                return;
            }

            //alert(id);
            if (files && files.length) {
                $scope.isLoading = true;

                counter = files.length;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    uploadIndividual(file, id);
                }
                var checkAllUpload = setInterval(function () {
                    if (counter == 0) {
                        clearInterval(checkAllUpload);
                        $scope.loadTimeline();
                        $scope.isLoading = false;

                    }
                }, 1000);
            }

        };

        $scope.editPost = function (id) {
            //alert('edit');
            $http.get('/admin/tracking/tracker/' + id).success(function (data) {
                $scope.editItem = data;
                var date = new Date(data.listDate);
                $scope.editItem.listDate = date.getDate() + '-' + (date.getMonth() + 1) + '-' + (date.getFullYear() + 543);
            });
        };

        $scope.removeFileCheck = function (uuid, index) {
            $scope.deleteFileItem = [index];
            $http.get('/admin/tracking/tracker/' + uuid).success(function (data) {
                $scope.editItem = data;
            });
        };

        $scope.removeFile = function () {
            //alert(JSON.stringify($scope.deleteFileItem));
            var list = $scope.deleteFileItem;

            var output = $scope.deleteFileItem;
            //alert(JSON.stringify(output));
            $http.delete('/admin/tracking/tracker/file/' + $scope.editItem.uuid,
                {
                    params: {'deleteID': output}
                }).success(function () {

                    $.smallBox({
                        title: "ลบไฟล์การติดตามโครงการเรียบร้อย",
                        content: "<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>",
                        color: "#49b4441",
                        iconSmall: "fa fa-trash-o bounce animated",
                        timeout: 4000
                    });
                    setTimeout(function () {
                        $scope.loadTimeline();
                        $scope.deleteFileItem = [];
                        $scope.deleteImageItem = [];
                    }, 2000);
                });
        };


        $scope.captionCheck = function (uuid, index) {
            $scope.captionImageItem = index;
            $http.get('/admin/tracking/tracker/' + uuid).success(function (data) {
                $scope.editItem = data;
                $scope.captionText = $scope.editItem.images[index].caption;
            });
        }

        $scope.captionSave = function (caption) {
            var captionID = $scope.captionImageItem;

            $http.post('/admin/tracking/tracker/image/' + $scope.editItem.uuid, null, {
                    params: {'captionID': '' + captionID, 'caption': caption}

                }
            ).success(function () {
                    setTimeout(function () {
                        $scope.loadTimeline();
                        $scope.captionImageItem = [];
                    }, 2000);
                });
        }


        $scope.removeImageCheck = function (uuid, index) {
            $scope.deleteImageItem = [index];
            $http.get('/admin/tracking/tracker/' + uuid).success(function (data) {
                $scope.editItem = data;
            });
        };

        $scope.removeImage = function () {

            var output = $scope.deleteImageItem;

            $http.delete('/admin/tracking/tracker/image/' + $scope.editItem.uuid,
                {
                    params: {'deleteID': output}
                }).success(function () {
                    setTimeout(function () {
                        $scope.loadTimeline();
                        $scope.deleteFileItem = [];
                        $scope.deleteImageItem = [];
                    }, 2000);
                });
        }


        var uploadIndividual = function (file, id) {
            $http.get('/admin/tracking/upload/' + id).success(function (data) {
                data = data.substr(1, data.length - 2);
                Upload.upload({
                    url: data,
                    fields: {name: file.name},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    $scope.progressPercentage = progressPercentage;
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    counter--;
                });

            });
        };


        var parseDate = function (date) {
            var split = date.split('-');
            var day = split[0];
            var month = split[1];
            var year = split[2];
            year -= 543;
            return Date.parse(year + '-' + month + '-' + day);

        }

    }]
);

