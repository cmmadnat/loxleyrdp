/* DO NOT REMOVE : GLOBAL FUNCTIONS!
 *
 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
 *
 * // activate tooltips
 * $("[rel=tooltip]").tooltip();
 *
 * // activate popovers
 * $("[rel=popover]").popover();
 *
 * // activate popovers with hover states
 * $("[rel=popover-hover]").popover({ trigger: "hover" });
 *
 * // activate inline charts
 * runAllCharts();
 *
 * // setup widgets
 * setup_widgets_desktop();
 *
 * // run form elements
 * runAllForms();
 *
 ********************************
 *
 * pageSetUp() is needed whenever you load a page.
 * It initializes and checks for all basic elements of the page
 * and makes rendering easier.
 *
 */
var vLat = 0;
var vLng = 0;

var map, marker;
var preSettedAmphur = null, preSettedTumbon = null;

pageSetUp();

/*
 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
 * eg alert("my home function");
 *
 * var pagefunction = function() {
 *   ...
 * }
 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
 *
 */


// PAGE RELATED SCRIPTS
var newProject = $('input[name=uuid]').val() == '';
var uuid = $('input[name=uuid]').val()
var address = '';

var error = function () {
    $.smallBox({
        title: "กรุณากรอกข้อมูลให้ครบถ้วน",
        content: "กรุณาตรวจเช็คและกรอกข้อมูลให้ครบถ้วน",
        color: "#E74243",
        iconSmall: "fa fa-exclamation-triangle bounce animated",
        timeout: 4000
    });
}

// pagefunction

var pagefunction = function () {

    $('#login-form').submit(function (e) {
        e.preventDefault();
        var now = new Date();
        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serializeArray(),
            success: function () {
                var rightNow = new Date();
                var timeUsed = rightNow.getMilliseconds() - now.getMilliseconds();
                $.smallBox({
                    title: "รายการนี้ถูกลบทิ้งไปแล้ว",
                    content: "<i class='fa fa-clock-o'></i> <i>" + Math.ceil(timeUsed / 1000) + " seconds ago...</i>",
                    color: "#E74243",
                    iconSmall: "fa fa-exclamation-triangle bounce animated",
                });
                setTimeout(function () {
                    window.close();
                }, 1000);

            }
        });

    });


    $('.breadcrumb').append("<li>โครงการ</li>");
    if (newProject) {

        window.document.title = 'เพิ่มข้อมูล';
        $('.breadcrumb').append("<li>เพิ่มข้อมูล</li>");
    }
    else {
        window.document.title = 'แก้ไขข้อมูล';
        $('.breadcrumb').append("<li>แก้ไขข้อมูล</li>");
    }


    /*jslint smarttabs:true */


    var metro_style = [{
        "featureType": "transit",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#d3d3d3"
        }, {
            "visibility": "on"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "landscape",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#eee8ce"
        }]
    }, {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#b8cec9"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#000000"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [{
            "visibility": "off"
        }, {
            "color": "#ffffff"
        }]
    }, {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#000000"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#d3cdab"
        }]
    }, {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ced09d"
        }]
    }, {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }];

    /*
     * Google Maps Initialize
     */
    var metroStyleMap = new google.maps.StyledMapType(metro_style, {
        name: "Metro"
    });


    // Metro Style
    function setProvince(long_name) {
        $.each($('#selectProvince option'), function (i, val) {
            if ($(val).text() == long_name) {
                $("#selectProvince").select2("val", $(val).val());
                $("#selectProvince").change();
            }
        });
    }


    function setAmphur(long_name) {
        long_name = long_name.replace('อำเภอ ', '');
        long_name = long_name.replace('เขต ', 'เขต');
        preSettedAmphur = long_name;
    }

    function setTumbon(long_name) {
        long_name = long_name.replace('ตำบล ', '');
        long_name = long_name.replace('แขวง ', '');
        preSettedTumbon = long_name;
    }

    function generate_map_7() {

        var mapOptions = {
            scrollwheel: false,
            center: new google.maps.LatLng(13.768303912361477, 100.49409985542297),
            zoom: 8
        };
        map = new google.maps.Map(document.getElementById('map_canvas7'), mapOptions);

//            autocomplete
        var input = /** @type {HTMLInputElement} */(
            document.getElementById('pac-input'));

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        // Setup skin for the map
        map.mapTypes.set('metro_style', metroStyleMap);
        map.setMapTypeId('metro_style');

        var marker;
        if (parseFloat($('#lat').val()) != vLat) {
            var location = new google.maps.LatLng($('#lat').val(), $('#lng').val());
            map.setCenter(location);
            marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    title: "ที่ตั้งโครงการ!"
                }
            );
            var infowindow = new google.maps.InfoWindow({
                content: "สถานที่ตั้งโครงการ"
            });
            infowindow.open(map, marker);
        }
        else {
            $('#lat').val('');
            $('#lng').val('');
        }


        google.maps.event.addListener(map, 'drag', function (e) {
            map.setOptions({scrollwheel: true});
        });

        google.maps.event.addListener(map, 'click', function (e) {

            if (typeof(marker) != 'undefined')marker.setMap(null);
            marker = new google.maps.Marker({
                position: e.latLng,
                map: map,
                title: "ที่ตั้งโครงการ!"
            });
            var infowindow = new google.maps.InfoWindow({
                content: "สถานที่ตั้งโครงการ"
            });
            infowindow.open(map, marker);
            $('#lat').val(e.latLng.lat());
            $('#lng').val(e.latLng.lng());

            var geocoder = new google.maps.Geocoder();
            var newLatLng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
            geocoder.geocode({'latLng': newLatLng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var addressComponents = results[0].address_components;
                        console.log(addressComponents);
                        for (var i = 0; i < addressComponents.length; i++) {
                            var obj = addressComponents[i];
                            if (obj.types[0] == "administrative_area_level_1") {
                                console.log('จังหวัด ' + obj.long_name);
                                setProvince(obj.long_name);
                            }
                            if (obj.types[0] == "sublocality_level_1") {
                                console.log('เขต ' + obj.long_name);
                                setAmphur(obj.long_name);
                            }
                            if (obj.types[0] == "administrative_area_level_2") {
                                console.log('อำเภอ ' + obj.long_name);
                                setAmphur(obj.long_name);
                            }
                            if (obj.types[0] == "locality" && obj.long_name != 'กรุงเทพมหานคร') {
                                console.log('ตำบล ' + obj.long_name);
                                setTumbon(obj.long_name);
                            }
                            if (obj.types[0] == "sublocality_level_2") {
                                console.log('แขวง ' + obj.long_name);
                                setTumbon(obj.long_name);
                            }

                        }
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });

        $('#lat,#lng').blur(function () {
            var lat = parseFloat($('#lat').val());
            var lng = parseFloat($('#lng').val());
            if (lat != '' && lng != '' && lat != 'NaN' && lng != 'NaN') {
                var location = new google.maps.LatLng($('#lat').val(), $('#lng').val());
                map.setCenter(location);

                if (typeof(marker) != 'undefined')marker.setMap(null);

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: map,
                    title: "ที่ตั้งโครงการ!"
                });
                var infowindow = new google.maps.InfoWindow({
                    content: "สถานที่ตั้งโครงการ"
                });
                infowindow.open(map, marker);

            }


        });

        // autocomplete
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            //infowindow.close();
            //marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("ไม่พบสถานที่ดังกล่าว");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }


        })
    }

    //        xmlLoadMap();
    generate_map_7();


    $("#mainProject").select2({
        ajax: {
            url: "/admin/royalproject/search",
            type: 'GET',
            dataType: 'json',
            delay: 1000,
            data: function (params) {
                return {
                    query: 'name=' + params
                };
            },
            results: function (data, page, query) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
//                        console.log('result = ' + data);
                var r = [{text: '=ไม่มีโครงการหลัก=', id: 0}];
                var x = $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.uuid
                    }
                });
                for (var i = 0; i < x.length; i++) {
                    var obj = x[i];
                    r.push(obj);
                }
                return {
                    results: r
                };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var id = $('input[name=uuid]').val();
            $.ajax({
                url: '/admin/royalproject/' + id,
                dataType: 'json',
                success: function (data) {
                    if (data.mainProject != null) {
                        var data = {id: data.mainProject.id, text: data.mainProject.name};
//                                    console.log(JSON.stringify(data));
                    }

                    callback(data);
                }
            });
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
//            minimumInputLength: 3,
        formatInputTooShort: function () {
            return 'กรอกชื่อโครงการหลัก';
        }

    });
    $("#department").select2({
        ajax: {
            url: "/admin/department/search",
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params
                };
            },
            results: function (data, page, query) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
//                        console.log('result = ' + data);
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.uuid
                        }
                    })
                };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var id = $('input[name=uuid]').val();
            $.ajax({
                url: '/admin/royalproject/' + id,
                dataType: 'json',
                success: function (data) {
                    $(this).val('');
                    var output = [];
                    var size = data.departments.length;
                    var departments = data.departments;
                    for (var j = 0; j < size; j++) {
                        var obj = departments[j];
                        var data = {id: obj.uuid, text: obj.name};
                        output.push(data);
                    }
                    callback(output);
                }
            });
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        multiple: true,
        formatInputTooShort: function () {
            return 'กรอกหน่วยงานที่รับผิดชอบ';
        }
    });
    // region
    $('#selectRegion').change(function () {
        if ($(this).val() != '') {
            address = $('#selectRegion option:selected').text();
            codeAddress();
            $.ajax({
                url: '/admin/location/' + $(this).val() + '/province',
                success: function (data) {
                    $('#selectProvince option').remove();
                    $('#selectProvince').append('<option value="">=เลือก=</option>')
                    for (var i = 0; i < data.length; i++) {
                        $('#selectProvince').append('<option value="' + data[i].uuid + '">' + data[i].name + '</option>')
                    }
                    $('#selectProvince').removeAttr('disabled');
                    $('#selectProvince').select2('val', '');
                    
                    
                    // reset
                    $('#selectAmphur').attr('disabled',true);
                    $('#selectAmphur').select2('val', '');
                    $('#selectTumbon').attr('disabled', true);
                    $('#selectTumbon').select2('val', '');


                }

            });

        }
    });
    // province
    $('#selectProvince').change(function () {
        if ($(this).val() != '') {
            address = $('#selectProvince option:selected').text();
            codeAddress();
            $.ajax({
                url: '/admin/location/' + $(this).val() + '/amphur',
                success: function (data) {
                    $('#selectAmphur option').remove();
                    $('#selectAmphur').append('<option value="">=เลือก=</option>')
                    for (var i = 0; i < data.length; i++) {
                        $('#selectAmphur').append('<option value="' + data[i].uuid + '">' + data[i].name + '</option>')
                    }
                    $('#selectAmphur').removeAttr('disabled');
                    $('#selectTumbon').attr('disabled', true);
                    $('#selectTumbon').select2('val', '');

                    $('#selectAmphur').change();
                    if (preSettedAmphur != null) {
                        $.each($('#selectAmphur option'), function (i, val) {
                            if ($(val).text() == preSettedAmphur) {
                                $('#selectAmphur').select2('val', $(val).val());
                                $('#selectAmphur').change();
//                                    console.log('changing amphur to ' + $(val).text())
                                preSettedAmphur = null;
                            }
                        });
                    }
                }

            });

        }
    });
    $('#selectAmphur').change(function () {
        if ($(this).val() != '')

            $.ajax({
                url: '/admin/location/' + $(this).val() + '/tumbon',
                success: function (data) {
                    $('#selectTumbon option').remove();
                    $('#selectTumbon').append('<option value="">=เลือก=</option>')
                    for (var i = 0; i < data.length; i++) {
                        $('#selectTumbon').append('<option value="' + data[i].uuid + '">' + data[i].name + '</option>')
                    }
                    $('#selectTumbon').removeAttr('disabled');
                    $('#selectTumbon').change();

                    if (preSettedTumbon != null) {
                        $.each($('#selectTumbon option'), function (i, val) {
                            if ($(val).text() == preSettedTumbon) {
                                $('#selectTumbon').select2('val', $(val).val());
                                $('#selectTumbon').change();
                                console.log('changing tumbon to ' + $(val).text())
                                preSettedTumbon = null;
                            }
                        });
                    }
                }
            });
    });
    $('#selectTumbon').change(function () {
//            $('#pac-input').val($('#selectProvince option:selected').text() + ' ' + $('#selectAmphur option:selected').text() + ' ' + $('#selectTumbon option:selected').text());
//            $('#pac-input').change();
        var amphur = $('#selectAmphur option:selected').text() != '=เลือก=' ? $('#selectAmphur option:selected').text() : '';
        var tumbon = $('#selectTumbon option:selected').text() != '=เลือก=' ? $('#selectTumbon option:selected').text() : '';
        address = $('#selectProvince option:selected').text() + ' ' + amphur + ' ' + tumbon;
        codeAddress();
    });

    $('#formMenu').affix({
        offset: {
            top: 100,
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    });

    $('.superbox').SuperBox();

    $('.superbox-list').click(function () {
        $('input[name=deleteIndex]').val($(this).attr('alt'));
    });

    $('#uploadForm').submit(function (e) {
//            alert('hi');
        e.preventDefault();
        var serializeArray = $('#uploadForm').serializeArray();

        if ($('#name').val() == '' || $('#typeID').val() == '' || ($('#selectCountry option:selected').index() == 0 && $('#selectProvince').val() == '') || $('#royaltyID').val() == '') {
            error();
        }
        else {
            var now = new Date();
            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: serializeArray,
                success: function (data) {

                    var rightNow = new Date();
                    var timeUsed = rightNow.getMilliseconds() - now.getMilliseconds();


                    $.smallBox({
                        title: "บันทึกเรียบร้อยแล้ว",
                        content: "<i class='fa fa-clock-o'></i> <i>" + Math.ceil(timeUsed / 1000) + " seconds ago...</i>",
                        color: "#49b4441",
                        iconSmall: "fa fa-save bounce animated",
                        timeout: 4000
                    });

                    if (newProject) {
                        setTimeout(function () {
                            window.location = '/admin#/admin/royalproject/' + data;
                        }, 2000);
                    }

                }
            });
        }
        ;
    });
    $(document).on("keypress", 'input', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#selectCountry').change(function () {
        $('#selectProvince').select2('val', '');

        address = $(this).val();
        if (address.indexOf('สปป.') != -1) address = address.substr(4);
        codeAddress();

        var index = $("#selectCountry option:selected").index();
        if (index == 0) {
            $('#selectLocation').show();
        }
        else {
            $('#selectLocation').hide();
        }

    });

    ideaLink();

};

// end pagefunction

// run pagefunction on load

$(window).unbind('gMapsLoaded');
$(window).bind('gMapsLoaded', function () {
    loadScript("js/plugin/superbox/superbox.min-r.js", function () {
        loadScript("/js/bootstrap-datepicker-thai/js/bootstrap-datepicker.custom.js", function () {
            loadScript("/js/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js", function () {
                loadScript("/js/bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js", function () {
                    loadScript('/js/bower_components/angular-ui-select/dist/select.min.js', function () {
                        pagefunction();
                    });
                });
            });
        });
    });
});
window.loadGoogleMaps();

function codeAddress() {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            //map.setCenter(results[0].geometry.location);
            $('#lat').val(results[0].geometry.location.lat());
            $('#lng').val(results[0].geometry.location.lng());
            $('#lng').blur();

        } else {
            $.smallBox({
                title: "ค้นหาตำแหน่งไม่พบ",
                content: "ไม่สามารถหาตำแหน่งของ '" + address + "' ได้ โปรดลองใหม่อีกครั้ง",
                color: "red",
                iconSmall: "fa fa-times bounce animated",
                timeout: 4000
            });

        }
    });
}


var ideaLink = function () {
    angular.module('projectApp', ['ui.select'])
        .directive('ngUpdateHidden', function () {
            return function (scope, el, attr) {
                var model = attr['ngModel'];
                scope.$watch(model, function (nv) {
                    el.val(nv);
                });

            };
        })
        .controller('IdeaLinkController', ['$scope', '$http', function ($scope, $http) {

            $scope.item = {};
            var init = function () {
                $http.get('/admin/royalproject/' + uuid).success(function (data) {
                    $scope.list = data.ideas;
                    for (var i = 0; i < $scope.list.length; i++) {
                        var obj = $scope.list[i];
                        if (obj.detail.length > 250) {
                            obj.detail = obj.detail.substr(0, 200) + '...';
                            //console.log(obj.detail);
                            //response.data[i]=obj;
                        }
                    }
                    $scope.query = '';
                });

            }

            $scope.refreshIdea = function (address) {
                var params = {query: address};
                return $http.get('/admin/idea/search', {params: params})
                    .then(function (response) {

                        $scope.addresses = response.data;
                    });
            };
            init();
            $scope.addLink = function () {

                var ideaID = $scope.item.idea;
                $scope.item = {};

                $http.post('/admin/royalproject/linkIdea/' + uuid + '', null, {
                    params: {
                        'ideaID': ideaID.uuid
                    }
                }).success(function () {
                    init();
                });
            };

            $scope.removeIdeaLink = function (id) {
                $scope.deleteID = id;
            };

            $scope.removeIdeaLinkConfirm = function () {

                $http.post('/admin/royalproject/linkIdea/delete/' + uuid, null, {
                    params: {
                        'ideaID': $scope.deleteID
                    }
                }).success(function () {
                    init();
                });
            }
        }]);


    angular.element('#ideaLink').ready(function () {
        angular.bootstrap('#ideaLink', ['projectApp']);
    });
}
