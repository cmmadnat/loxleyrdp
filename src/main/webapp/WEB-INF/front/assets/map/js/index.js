/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var map = null;
var infowindow = null;
var UserMarker;
var image = 'assets/map/img/user.png';
var results = new Array();
var pos;
var ico;
var marker;
var PoiData = [];
var StateDiv = 0;
var mcOptions;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var InfowindowArray =[];
var defaultMarker;
var trafficLayer = new google.maps.TrafficLayer();
var chkTF = document.getElementById('chktraffic');

var minz = true;
var id = 0;
var water ='ด้านพัฒนาแหล่งน้ำ';
var agriculture ='ด้านเกษตรกร';
var environment ='ด้านสิ่งแวดล้อม';
var career ='ด้านส่งเสริมอาชีพ';
var communicate ='ด้านคมนาคมสื่อสาร';
var social ='ด้านสวัสดิการสังคม';
var health ='ด้านสาธารณสุข';
var etc ='อื่นๆ';
var noCoor = [];
var Allmarkers =[];
var searchRS =[];
var ctff = false;

//var deviceType = "Android";
var deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : 
								 (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : 
								 (navigator.userAgent.match(/iPod/i))  == "iPod" ? "iPod" : 
								 (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" :
								 (navigator.userAgent.match(/webos/i)) == "WebOS" ? "WebOS" : 
								 (navigator.userAgent.match(/windows phone/i)) == "WindowsPhone" ? "WindowsPhone" :  
								 (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

var styles = [{
				textColor: '#ffffff',
				textSize: 10
			}];

var markerClusterer;
function initialize() {
	//load();
	directionsDisplay = new google.maps.DirectionsRenderer();

	var mapOptions = {
		center: new google.maps.LatLng(13.7687145,100.4945703),
		scaleControl: true
	};
	map = new google.maps.Map(document.getElementById('map'),
			mapOptions);
	

	// Try HTML5 geolocation
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

			UserMarker = new google.maps.Marker({
					position: pos,
					map: map,
					icon: image
			});
			map.setCenter(pos);
			map.setZoom(6);
		});

		navigator.geolocation.watchPosition(function(position) {
			if(pos != position && UserMarker != null){
				UserMarker.setPosition(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
			}
		});
	}
	// Create the search box and link it to the UI element.
	var input = (document.getElementById('pac-input'));
	var ub = (document.getElementById('Ub'));
	var bp = (document.getElementById('BPanel'));
	var ti = (document.getElementById('TravelInfo'));
	var tr = (document.getElementById('traffic'));
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(ub);
	if(deviceType == 'null'){
		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(bp);
		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(tr);
		map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(ti);
	}else{
		map.controls[google.maps.ControlPosition.RIGHT_TOP].push(bp);
		map.controls[google.maps.ControlPosition.RIGHT_TOP].push(tr);
		ti.style.zIndex = "1";
		ti.style.position = "absolute";
	}
	
	var searchBox = new google.maps.places.SearchBox(input);

	// Listen for the event fired when the user selects an item from the
	// pick list. Retrieve the matching places for that item.
	google.maps.event.addListener(searchBox, 'places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		var bounds = new google.maps.LatLngBounds();
		for (var i = 0, place; place = places[i]; i++) {

		 bounds.extend(place.geometry.location);
		}

		map.fitBounds(bounds);
		map.setZoom(15);
	});

	// Bias the SearchBox results towards places that are within the bounds of the
	// current map's viewport.
	google.maps.event.addListener(map, 'bounds_changed', function() {
		var bounds = map.getBounds();
		searchBox.setBounds(bounds);
	});

	google.maps.event.addDomListener(chkTF, 'click', function() {
					if(ctff == true){            
						trafficLayer.setMap(null);
						chkTF.className = "btn btn-default btn-xs";
						ctff = false;
					}else{
						ctff = true;
						chkTF.className = "btn btn-success btn-xs";
						trafficLayer.setMap(map);

					}
	});
	

	var infoWindow = new google.maps.InfoWindow({maxWidth: 800});
	mcOptions = {gridSize: 50, 
									 maxZoom: 15, 
									 styles: [{
															height: 53,
															url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
															width: 53,
															textColor: '#000000',
															textSize: 10
															},
															{
															height: 56,
															url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
															width: 56,
															textColor: '#000000',
															textSize: 10
															},
															{
															height: 66,
															url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m3.png",
															width: 66,
															textColor: '#000000',
															textSize: 10
															},
															{
															height: 78,
															url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m4.png",
															width: 78,
															textColor: '#000000',
															textSize: 10
															},
															{
															height: 90,
															url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m5.png",
															width: 90,
															textColor: '#000000',
															textSize: 10
															}]
				};
/*(function($) {
$.ajax({
     url:"http://rdp-test.appspot.com/mobile?page=6&limit=10",
     type: 'GET',
     async: false,
     dataType: 'jsonp', // Notice! JSONP <-- P (lowercase)
     jsonpCallback: 'jsonCallback',
     contentType: "application/json",
     success:function(json){
         // do stuff with json (in this case an array)
         console.log(json);
     },
     error:function(e){
         console.log(e.message);
     }      
});
})(jQuery);*/

	markerClusterer = new MarkerClusterer(map, [], mcOptions);
	var myParser = new geoXML3.parser({
			map: map,
			singleInfoWindow:true,
			createMarker: function(placemark) {
											//Constructing marker for each Placemark node, and then add it to the markclustere
											var point = placemark.latlng;
											
											var type,ico;
											infoWindow.setContent("");
											
												if(placemark.description.match('ประเภทกิจกรรม: ด้านพัฒนาแหล่งน้ำ')){
													ico='assets/map/img/8.png';
													type=water;
												}else if(placemark.description.match('ประเภทกิจกรรม: ด้านส่งเสริมอาชีพ')){
													ico='assets/map/img/job.png';
													type=career;
												}else if(placemark.description.match('ประเภทกิจกรรม: ด้านเกษตรกร')){
													ico='assets/map/img/farm.png';
													type=agriculture;
												}else if(placemark.description.match('ประเภทกิจกรรม: ด้านสิ่งแวดล้อม')){
													ico='assets/map/img/nature.png';
													type=environment;
												}else if(placemark.description.match('ประเภทกิจกรรม: ด้านคมนาคมสื่อสาร')){
													ico='assets/map/img/commu.png';
													type=communicate;
												}else if(placemark.description.match('ประเภทกิจกรรม: ด้านสวัสดิการสังคม')){
													ico='assets/map/img/edu.png';
													type=social;
												}else if(placemark.description.match('ประเภทกิจกรรม: ด้านสาธารณสุข')){
													ico='assets/map/img/heal.png';
													type=health;
												}else if(placemark.description.match('ประเภทกิจกรรม: อื่นๆ')){
													ico='assets/map/img/etc2.png';
													type=etc;
												}

											PoiData.push({
												id:id,
												name:placemark.name,
												type:type,
												lat:point.lat().toString(),
												lng:point.lng().toString(),
												marker:makeMarker(ico,point,placemark.name)
											});

											PoiData[id].marker.info = infoWindow;

											
											var SDid = "slider"+id;
											//console.log(SDid);
											/*var imgSlider = document.createElement('ul');
											imgSlider.id = SDid;
											imgSlider.className = "rslides"
											imgSlider.innerHTML='<li><img src="img/travel/01.jpg" /></li>'
																			+'<li><img src="img/travel/02.jpg" /></li>'
																			+'<li><img src="img/travel/03.jpg" /></li>'
																			+'<li><img src="img/travel/04.jpg" /></li>'
																			+'<li><img src="img/travel/06.jpg" /></li>'
																			+'<li><img src="img/travel/06.jpg" /></li>';*/
											google.maps.event.addListener(PoiData[id].marker, "click", function(e) {

												if(placemark.latlng.lat() != 0 && placemark.latlng.lng() != 0){
												 content = '<div class="noscrollbar"><strong>' + placemark.name + '</strong><br>' + placemark.description + '<br>'+
														'<br><CENTER><button class="btn btn-default" type="button" onclick="direction('+placemark.latlng.lat()+','+placemark.latlng.lng()+',\''+placemark.name+'\')">การนำทาง</button> <button type="button" class="btn btn-success">ดูข้อมูลเพิ่มเติม</button></CENTER><br></div>';
												}else{
													content = '<div class="noscrollbar"><strong>' + placemark.name + '</strong><br>' + placemark.description + 
														'<br><CENTER><button class="btn btn-default" type="button" onclick="direction(13.7687145,100.4945703,\''+placemark.name+'\')">การนำทาง</button> <button type="button" class="btn btn-success">ดูข้อมูลเพิ่มเติม</button></CENTER><br></div>';
												}
												
												infoWindow.setContent(content);
												infoWindow.close();                           
												infoWindow.open(map, this);

												this.info = infowindow;
												 /*$(function() {
														try{
															console.log('in');
															$(SDid).responsiveSlides({
																auto: false,
																pager: true,
																nav: true,
																namespace: "large-btns"
															});
														}catch(e){
															consolo.log(e);
														}
												 });*/
											});

											markerClusterer.addMarker(PoiData[id].marker);
											id++;
			},
			afterParse:function(c){
				/*console.log(markerClusterer)
				console.log(PoiData);
				console.log(PoiData.length);
				console.log(c);*/
			}
	});
try{
	myParser.parse('/assets/map/data.kml');
}catch(e){
	console.log(e);
}

//console.log(Allmarkers);
	$(document).ready(function(){
		$("#BPanel").click(function(){
			hideDiv();       
		});
		$("#PHead").click(function(){
			hideDiv();
		});
	});
//map.setZoom = 8;
}//end init

if(deviceType != 'null'){
	 document.getElementById('pac-input').style.width = '200px'; 
	 document.getElementById('map').style.width = '100%';
	 document.getElementById('map').style.height = '100%';    
	 //var el = document.getElementById( 'header' );
	 //el.parentNode.removeChild( el );
	 //document.getElementById('BPanel').style.display = 'none';
}

function getULocation(){
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			if(UserMarker != null){
				pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				UserMarker.setPosition(pos);
				map.setCenter(pos);
			}   
		});
	 }
}

function makeMarker(ico,point,name){
	var dpoint = new google.maps.LatLng(13.7687145,100.4945703);
	if(point.lat() != 0 && point.lng() != 0){
	return new google.maps.Marker({
												position: point,
												map:map,
												icon:ico,
												title:name
											});
	}else{
	 return new google.maps.Marker({
												position: dpoint,
												icon:ico,
												map:null,
												title:name
											});
	}
}

function toggle(div){
	if(div == 1){
		$("#filter").animate({height: 'toggle'});
	}else{
		$("#searchPro").animate({height: 'toggle'});
	}
}

function filter(){
	var chkWater = document.getElementById('water');
	var chkFarming = document.getElementById('farming');
	var chkEnv = document.getElementById('env');
	var chkCareer = document.getElementById('career');
	var chkInformation = document.getElementById('infomation');
	var chkSocial = document.getElementById('social');
	var chkHealth = document.getElementById('health');
	var chkEtc = document.getElementById('etc');

	markerClusterer.clearMarkers();

	if(chkWater.checked){
		manageMarker(water);
	}

	if(chkFarming.checked){
		manageMarker(agriculture);
	}

	if(chkEnv.checked){
		manageMarker(environment);
	}

	if(chkCareer.checked){
		manageMarker(career);
	}

	if(chkInformation.checked){
		manageMarker(communicate);
	}

	if(chkSocial.checked){
		manageMarker(social);
	}

	if(chkHealth.checked){
		manageMarker(health);
	}

	if(chkEtc.checked){
		manageMarker(etc);
	}

	markerClusterer.repaint();  
}

function manageMarker(type){

	for(index = 0;index < PoiData.length;index++) {
			//console.log(PoiData[index].id); 
				if(PoiData[index].type.match(type.toString())){
					//console.log(PoiData[index].marker);
					markerClusterer.addMarker(PoiData[index].marker);
				}
	}

	console.log(markerClusterer);
}

function search(){
	var txt = document.getElementById('searchTxt').value;
	var divR = document.getElementById('Dresult');
	var result = document.createElement("div");
	var html;
	result.className = "result";
	if(divR.innerHTML != ""){
		divR.innerHTML = "";
	}
	for(index = 0;index < PoiData.length;index++) {
		if(txt.match(PoiData[index].name)){
			if(PoiData[index].lat != 0 && PoiData[index].lng != 0){
				html.push('<strong>ชื่อโครงการ:' + PoiData[index].name + '</strong><br>ประเภทโครงการ:' + PoiData[index].type + 
														'<br><button class="btn btn-default" type="button" onclick="direction('+PoiData[index].lat+','+PoiData[index].lng+',\''+PoiData[index].name+'\')">การนำทาง</button> <button type="button" class="btn btn-success">ดูข้อมูลเพิ่มเติม</button><br>');
			}else{
				html.push('<strong>ชื่อโครงการ:' + PoiData[index].name + '</strong><br>ประเภทโครงการ:' + PoiData[index].type + 
														'<br><button class="btn btn-default" type="button" onclick="direction(13.7687145,100.4945703,\''+PoiData[index].name+'\')">การนำทาง</button> <button type="button" class="btn btn-success">ดูข้อมูลเพิ่มเติม</button><br>');
			}
			result.onclick = (function() {
						var pt = PoiData[index].lat;
						var pn = PoiData[index].lng;
						return function(){
							console.log(pt);
							map.setCenter(new google.maps.LatLng(pt,pn)); map.setZoom(15);
						}
				})();
			divR.appendChild(result);
		}else if(txt.value = ""){
			if(PoiData[index].lat != 0 && PoiData[index].lng != 0){
				html.push('<strong>ชื่อโครงการ:' + PoiData[index].name + '</strong><br>ประเภทโครงการ:' + PoiData[index].type + 
														'<br><button class="btn btn-default" type="button" onclick="direction('+PoiData[index].lat+','+PoiData[index].lng+',\''+PoiData[index].name+'\')">การนำทาง</button> <button type="button" class="btn btn-success">ดูข้อมูลเพิ่มเติม</button><br>');
			}else{
				html.push('<strong>ชื่อโครงการ:' + PoiData[index].name + '</strong><br>ประเภทโครงการ:' + PoiData[index].type + 
														'<br><button class="btn btn-default" type="button" onclick="direction(13.7687145,100.4945703,\''+PoiData[index].name+'\')">การนำทาง</button> <button type="button" class="btn btn-success">ดูข้อมูลเพิ่มเติม</button><br>');
			}
		}
		console.log(html);  
	}
	result.innerHTML = html;
}

function direction(endLat,endLng,nameD){
	
	start = new google.maps.LatLng(UserMarker.getPosition().lat(),UserMarker.getPosition().lng());
	end = new google.maps.LatLng(endLat,endLng);

	var request = {
			origin:start,
			destination:end,
			durationInTraffic:true,
			travelMode: google.maps.TravelMode.DRIVING
	};

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {

			directionsDisplay.setDirections(response);
			console.log(response);
			console.log(status);

			var tools = document.createElement("div");
			var infobox = document.getElementById("TravelInfo");
			var content = document.createElement("div");
			var title = document.createElement("div");
			var detail = document.createElement("div");

			if(deviceType != "null"){
				infobox.style.width = '100%';
				infobox.style.height = '100%';
			}else{
				infobox.style.maxWidth = "400px";
				infobox.style.maxHeight = "350px";
			}

			if(infobox.innerHTML != ""){
				clspanel();
			}

			directionsDisplay.setMap(map);

			content.id = "infoCon";
			tools.innerHTML = "<button type='button' class='btn btn-primary  glyphicon glyphicon-minus 'onclick='minzpanel();'></button>"+
			"<button type='button' class='btn btn-danger glyphicon glyphicon-remove' onclick='clspanel();'></button>";
			tools.style = "cursor: pointer; float: right;";

			content.style = "display: inline; float: left; max-width: inherit;";
			detail.id = "Dpanel";
			if(deviceType != "null"){
				detail.className = "detail-Direction-mobile";
				//tools.style = "cursor: pointer; float: right; right: 100%;";
			}else{
				detail.className = "detail-Direction-window";
			}  
			title.innerHTML = "<br>จากตำแหน่งปัจุบันที่คุณอยู่ไปยัง <p class='text-primary text-uppercase'>"+nameD+"</p> เป็นระยะทาง:"+response.routes[0].legs[0].distance.text+"</br> ใช้เวลาในการเดินทางโดยประมาณ:"+response.routes[0].legs[0].duration.text+"</br>รายละเอียดการเดินทาง:<br>";
			title.style = "padding-left: 3px; white-space: normal;";
			
			infobox.appendChild(tools);
			content.appendChild(title);
			content.appendChild(detail);
			infobox.appendChild(content);
			directionsDisplay.setPanel(detail); 
			infobox.className = "Tinfo";
			
		}else{
			console.log(status);
			console.log(response);
		}
	});

}

function clspanel(){
	var Tinfo = document.getElementById("TravelInfo");
	Tinfo.innerHTML = "";
	Tinfo.className = "Tinfonone";
	directionsDisplay.setMap(null);
}

function minzpanel(){
	var Cinfo = document.getElementById("infoCon");
	var ti = document.getElementById('TravelInfo');
	var PB = document.getElementById('PanelBody');
	var newTI;
	//var detail;
	if(deviceType == 'null'){
		if(minz == true){
		//detail = Cinfo.innerHTML;
		Cinfo.style.display = "none";
		minz = false;
		}else{
			console.log(minz);
			Cinfo.style.display = "inline";
			minz = true;
		}
	}else{
		if(minz == true){
		//detail = Cinfo.innerHTML;
		Cinfo.style.display = "none";
		map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(ti);
		ti.style.width = "auto";
		ti.style.height = 'auto';
		minz = false;
		}else{
			Cinfo.style.display = "inline-block";
			newTI = map.controls[google.maps.ControlPosition.BOTTOM_LEFT].getAt(0);
			map.controls[google.maps.ControlPosition.BOTTOM_LEFT].removeAt(0);
			newTI.style.width = '100%';
			newTI.style.height = '100%';
			PB.appendChild(newTI);
			minz = true;
		}
	}
	
	

}

function hideDiv(){
	if(StateDiv == 0){
					if(deviceType != 'null'){
						$("#map").animate({width: 'toggle'});
						$("#panel").width('100%');
					}else{
						$("#map").animate({width: '65%'});
					}          
					//$("#BPanel").animate({left:'-33%'});
					$("#panel").animate({width: 'toggle'},'slow');
					StateDiv = 1;
				}else{
					if(deviceType != 'null'){
						$("#map").animate({width: 'toggle'},'slow');
						$("#panel").animate({width: 'toggle'});
					}else{
						$("#map").animate({width: '100%'},'slow');
						//$("#BPanel").animate({left:"1.2%"},'slow');
						$("#panel").animate({width: 'toggle'}); 
					}
					StateDiv = 0;
	}    
};

																				
			 

try{google.maps.event.addDomListener(window, 'load', initialize);}catch(e){console.log(e);}



