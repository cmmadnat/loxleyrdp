package com.cmmadnat.sso.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Created by cmmad_000 on 7/3/2015.
 */
public class SSORole {

    Long id;

    String name;

    Integer identifier =0;

    Boolean canEditUser = false;
    Boolean canEditRole = false;
    Boolean canEditSetting = false;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCanEditUser() {
        return canEditUser;
    }

    public void setCanEditUser(Boolean canEditUser) {
        this.canEditUser = canEditUser;
    }

    public Boolean getCanEditRole() {
        return canEditRole;
    }

    public void setCanEditRole(Boolean canEditRole) {
        this.canEditRole = canEditRole;
    }

    public Boolean getCanEditSetting() {
        return canEditSetting;
    }

    public void setCanEditSetting(Boolean canEditSetting) {
        this.canEditSetting = canEditSetting;
    }


    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
