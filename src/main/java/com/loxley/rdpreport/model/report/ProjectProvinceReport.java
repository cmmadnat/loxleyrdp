package com.loxley.rdpreport.model.report;

import java.io.Serializable;

/**
 * Created by cmmad_000 on 6/17/2015.
 */
public class ProjectProvinceReport implements Serializable{
    private String province;
    private String water;
    private String farm;
    private String environment;
    private String occupation;
    private String medical;
    private String education;
    private String commute;
    private String mix;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getWater() {
        return water;
    }


    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getFarm() {
        return farm;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setoccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getMedical() {
        return medical;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    public void setCommute(String commute) {
        this.commute = commute;
    }

    public String getCommute() {
        return commute;
    }

    public void setMix(String mix) {
        this.mix = mix;
    }

    public String getMix() {
        return mix;
    }
}
