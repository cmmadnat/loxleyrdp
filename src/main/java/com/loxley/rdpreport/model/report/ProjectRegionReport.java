package com.loxley.rdpreport.model.report;

import java.io.Serializable;

/**
 * Created by cmmad_000 on 6/16/2015.
 */
public class ProjectRegionReport implements Serializable {
    String name;

    String total;
    private Integer areaRainy=0;
    private Integer areaSummer=0;
    private Integer village=0;
    private Integer household=0;
    private Integer citizen=0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Integer getAreaRainy() {
        return areaRainy;
    }

    public void setAreaRainy(Integer areaRainy) {
        this.areaRainy = areaRainy;
    }

    public Integer getAreaSummer() {
        return areaSummer;
    }

    public void setAreaSummer(Integer areaSummer) {
        this.areaSummer = areaSummer;
    }

    public Integer getVillage() {
        return village;
    }

    public void setVillage(Integer village) {
        this.village = village;
    }

    public Integer getHousehold() {
        return household;
    }

    public void setHousehold(Integer household) {
        this.household = household;
    }

    public Integer getCitizen() {
        return citizen;
    }

    public void setCitizen(Integer citizen) {
        this.citizen = citizen;
    }
}
