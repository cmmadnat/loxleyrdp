package com.loxley.rdpreport.model.report;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by cmmad_000 on 6/17/2015.
 */
@Data
public class ProjectRoyaltyReport implements Serializable {
    String royaltyName;

    String total;
    Integer areaRainy = 0;
    Integer areaSummer = 0;
    Integer village = 0;
    Integer household = 0;
    Integer citizen = 0;


}