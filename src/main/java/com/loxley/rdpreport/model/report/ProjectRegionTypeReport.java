package com.loxley.rdpreport.model.report;

import java.io.Serializable;

/**
 * Created by cmmad_000 on 7/16/2015.
 */
public class ProjectRegionTypeReport implements Serializable{
    String region;
    String type;
    int total;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
