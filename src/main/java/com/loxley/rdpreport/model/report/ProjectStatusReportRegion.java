package com.loxley.rdpreport.model.report;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by cmmad_000 on 6/24/2015.
 */
@Data
public class ProjectStatusReportRegion implements Serializable {
    String name;

    String total;
    String complete = "-";
    String only_data = "-";
    String only_photo = "-";
    String not_input = "-";
}