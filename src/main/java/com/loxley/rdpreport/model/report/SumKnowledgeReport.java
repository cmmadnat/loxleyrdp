package com.loxley.rdpreport.model.report;

import java.io.Serializable;

/**
 * Created by cmmad_000 on 6/24/2015.
 */
public class SumKnowledgeReport implements Serializable{
    String name;
    int total;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

}
