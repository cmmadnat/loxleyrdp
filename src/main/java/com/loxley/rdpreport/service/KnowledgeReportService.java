package com.loxley.rdpreport.service;

import com.loxley.rdpreport.model.report.SumKnowledgeReport;

import java.util.List;

/**
 * Created by cmmad_000 on 6/24/2015.
 */
public interface KnowledgeReportService {
    List<SumKnowledgeReport> sumKnowledgeReports();
}
