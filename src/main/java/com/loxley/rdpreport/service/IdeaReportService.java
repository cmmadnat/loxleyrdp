package com.loxley.rdpreport.service;

import com.loxley.rdpreport.model.report.SumIdeaReport;

import java.util.List;

/**
 * Created by cmmad_000 on 6/16/2015.
 */
public interface IdeaReportService {
    List<SumIdeaReport> ideaSumReport();
}
