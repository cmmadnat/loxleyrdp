package com.loxley.rdpreport.service;

import com.loxley.rdpreport.model.report.*;

import java.util.List;

/**
 * Created by cmmad_000 on 6/16/2015.
 */
public interface ProjectReportService {
    List<ProjectRegionReport> reportByRegion();

    List<ProjectProvinceReport> reportByProvince();

    List<ProjectRoyaltyReport> reportByRoyalty();

    List<ProjectTypeReport> reportByType();

    List<ProjectMinistryReport> reportByMinistry();

    List<ProjectStatusReport> reportByStatus();

    List<ProjectRegionTypeReport> reportByRegionAndType();

    List<ProjectStatusReportRegion> reportByStatusRegion();
}
