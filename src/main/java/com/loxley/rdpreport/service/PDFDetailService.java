package com.loxley.rdpreport.service;

import java.io.IOException;

/**
 * Created by cmmad_000 on 8/6/2015.
 */
public interface PDFDetailService {
    byte[] testGoogle() throws IOException, InterruptedException;

    byte[] getPDF(String path) throws IOException, InterruptedException;
}
