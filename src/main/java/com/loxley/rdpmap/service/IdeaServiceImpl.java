package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.search.*;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.taskqueue.*;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import com.google.appengine.repackaged.org.joda.time.DateTime;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.Setting;
import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.domain.old.Province;
import com.loxley.rdpmap.domain.old.Royalty;
import com.loxley.rdpmap.util.ThaiDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 4/9/2015.
 */
public class IdeaServiceImpl implements IdeaService {

    public static final String OLD_SITE_PREFIX = "http://km.rdpb.go.th/Content/uploads/images";
    public static final String NEW_SITE_PREFIX = "http://storage.googleapis.com/rdp-archive";
    private static final int UPDATE_LIMIT = 500;
    private static final int FIRST_PAGE_COUNT = 3;
    public static final String PDF_HOST = "http://27.254.68.103:8081";
    @Autowired
    ProjectService projectService;
    @Autowired
    RoyaltyService royaltyService;

    HashSet<Integer> deekaIDs = new HashSet<Integer>();

    Logger logger = LoggerFactory.getLogger(IdeaServiceImpl.class);


    private static final String INDEX_NAME = "ideaIndex";
    private Index index;
    private boolean check = false;
    @Autowired
    private SettingService settingService;

    @Override
    public Integer count() {

        if (!check) {
            check = true;
            init();
        }
        return ofy().load().type(Idea.class).count();
    }

    @Override
    public void saveIdeas(List<Idea> values) {
        if (!check) {
            check = true;
            init();
        }

        for (Idea value : values) {
            DateTime date = new DateTime(value.getDate());
            date = date.plusDays(1);
            value.setDate(date.toDate());
            processIdea(value);
        }
        Collection<Idea> list = ofy().save().entities(values).now().values();
        for (Idea idea : list) {
            createIndexIdea(idea);
        }
    }

    @Override
    public List<Idea> findAll(int page, int limit) {
        if (!check) {
            check = true;
            init();
        }
        return ofy().load().type(Idea.class).order("-date").limit(limit).offset(page * limit).list();
    }

    @Override
    public long countSearch(String query) {
        if (!check) {
            check = true;
            init();
        }

        long count = index.search(query).getNumberFound();
        return count;
    }

    @Override
    public List<Idea> search(String query, int page, int limit) {
        if (!check) {
            check = true;
            init();
        }

        if (query.isEmpty()) return findAll(page, limit);

        QueryOptions option = QueryOptions.newBuilder()
                .setLimit(limit)
                .setOffset(limit * page)
                .setSortOptions(SortOptions.newBuilder()
                        .addSortExpression(SortExpression.newBuilder()
                                .setDirection(SortExpression.SortDirection.DESCENDING)
                                .setExpression("date").build()))
                .build();

        Results<ScoredDocument> list = index.search(Query.newBuilder().setOptions(option).build(query));
        ArrayList<Idea> output = new ArrayList<Idea>();

        for (ScoredDocument scoredDocument : list) {
            Idea one = findOne(Long.valueOf(scoredDocument.getId()));
            if (one != null) output.add(one);
        }

        return output;
    }

    @Override
    public Idea findOne(Long aLong) {

        Idea idea = ofy().load().type(Idea.class).id(aLong).now();
        if (idea != null && idea.getProvinceID() != null && idea.getProvinceID().getUuid() == null) {
            Province id1 = ofy().load().type(Province.class).filter("id", idea.getProvinceID().getId()).first().now();
            if (id1 != null) idea.getProvinceID().setUuid(id1.getUuid());
        }
        return idea;
    }

    @Override
    public void wipe() {
        if (!check) {
            check = true;
            init();
        }

//        for (Idea idea : all) {
//            ids.add(idea.getUuid().toString());
//        }
//        List<List<String>> partition = Lists.partition(ids, 200);
//        for (List<String> strings : partition) {
//            index.delete(strings);
//        }
//        ids.clear();

        List<Idea> all = ofy().load().type(Idea.class).list();

        ArrayList<String> ids = new ArrayList<String>();

        GetResponse<Document> search = index.getRange(GetRequest.newBuilder().setReturningIdsOnly(true).setLimit(1000).build());
        for (Document document : search) {
            ids.add(document.getId());
        }
        List<List<String>> partition = Lists.partition(ids, 200);
        for (List<String> strings : partition) {
            index.delete(strings);
        }


        ofy().delete().entities(all);


    }

    @Override
    public void delete(Long id) {
        Idea idea = findOne(id);
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

        List<RoyalProjectFile> files = idea.getFiles();
        for (RoyalProjectFile file : files) {
            blobstoreService.delete(new BlobKey(file.getKey()));
        }
        List<RoyalProjectImage> images = idea.getImages();
        for (RoyalProjectImage image : images) {
            blobstoreService.delete(new BlobKey(image.getKey()));
        }

        projectService.stripIdea(idea);
        index.delete(id + "");
        ofy().delete().entity(idea);
    }

    @Override
    public void updateIndex() {
        com.google.appengine.api.taskqueue.Queue defaultQueue = QueueFactory.getDefaultQueue();
        int count = ofy().load().type(Idea.class).count();
        int numberOfPage = count / UPDATE_LIMIT + 1;

        while (true) {
            List<String> docIds = new ArrayList<String>();
            // Return a set of doc_ids.
            GetRequest request = GetRequest.newBuilder().setReturningIdsOnly(true).build();
            GetResponse<Document> response = index.getRange(request);
            if (response.getResults().isEmpty()) {
                break;
            }
            for (Document doc : response) {
                docIds.add(doc.getId());
            }
            index.delete(docIds);
        }


        for (int i = 0; i < numberOfPage; i++) {
            defaultQueue.add(TaskOptions.Builder.withUrl("/import/index/idea").param("page", i + ""));
        }
    }

    @Override
    public void updateIndexPage(int page) {
        if (!check) {
            check = true;
            init();
        }


        List<Idea> list = ofy().load().type(Idea.class).offset(UPDATE_LIMIT * page).limit(UPDATE_LIMIT).list();

        for (Idea idea : list) {

            if (idea.getDisplay() != null && !idea.getDisplay().isEmpty() && idea.getFiles().isEmpty()) {
                RoyalProjectFile file = new RoyalProjectFile();
                file.setFileName(PDF_HOST + "/idea/" + idea.getUuid() + "/pdf");
                idea.getFiles().add(file);
            }
            
            processIdea(idea);
            convertImage(idea);
            createIndexIdea(idea);
        }

        ofy().save().entities(list);
    }

    @Override
    public List<Project> findRelatedProject(Idea target) {
        return ofy().load().type(Project.class).filter("ideas", Ref.create(target)).list();
    }

    @Override
    public void update(Long id, Idea one) {
        processIdea(one);
        one.setTime(new Date());
        ofy().save().entities(one);

        createIndexIdea(one);
    }

    @Override
    public Long save(Idea idea) {
        processIdea(idea);
        Key<Idea> now = ofy().save().entity(idea).now();
        createIndexIdea(idea);

        return now.getId();
    }

    @Override
    public List<Idea> getFirstpageIdeas() {
        final ObjectMapper objectMapper = new ObjectMapper();

        Setting firstPageIdeaUsageCount = settingService.get("firstPageIdeaUsageCount");
        // count usage to 200 and reset the page index
        if (firstPageIdeaUsageCount == null || firstPageIdeaUsageCount.getValue().equals("200")) {
            settingService.set("firstPageIdeaUsageCount", "0");
            firstPageIdeaUsageCount = new Setting();
            firstPageIdeaUsageCount.setValue("0");
        }


        Setting firstPageIdeas = settingService.get("firstPageIdeas");
        if (firstPageIdeas == null || firstPageIdeaUsageCount.getValue().equals("0")) {
            final ArrayList<Long> ids = new ArrayList<Long>();
            final List<Idea> list = ofy().load().type(Idea.class).limit(100).order("-date").list();
            int counter = 0;
            do {
                Idea item = list.get(counter++);
                if (item.getImages().size() != 0 || (item.getImgCover() != null && item.getImgCover().isEmpty()))
                    ids.add(item.getUuid());
            } while (ids.size() < FIRST_PAGE_COUNT && counter < list.size());
            final String s;
            try {
                s = objectMapper.writeValueAsString(ids);
                settingService.set("firstPageIdeas", s);
                firstPageIdeas = new Setting();
                firstPageIdeas.setValue(s);
            } catch (JsonProcessingException e) {
                logger.error("serialize json in get first page");
            }
        }

        final int next = Integer.parseInt(firstPageIdeaUsageCount.getValue()) + 1;
        settingService.set("firstPageIdeaUsageCount", next + "");

        final String value = firstPageIdeas.getValue();
        final ArrayList<String> o;
        try {
            o = objectMapper.readValue(value, new TypeReference<ArrayList<String>>() {
            });
            final ArrayList<Long> ids = new ArrayList<Long>();
            for (String s : o) {
                ids.add(Long.valueOf(s));
            }
            if (ids.size() != 0) {
                final Collection<Idea> values = ofy().load().type(Idea.class).ids(ids).values();

                if (values.size() != ids.size()) settingService.set("firstPageIdeaUsageCount", "0");

                return new ArrayList<Idea>(values);
            } else return findAll(0, FIRST_PAGE_COUNT);

        } catch (IOException e) {
            logger.error("error deserialize the json from setting");
        }

        return findAll(0, FIRST_PAGE_COUNT);
    }

    private void processIdea(Idea idea) {

        // move the old deeka to the new one
//        if (idea.getSource() == 0) idea.setSource(1);
//        if (deekaIDs.contains(idea.getRoyaltyID().getId())) {
//            idea.setSource(2);
//            int oldID = idea.getRoyaltyID().getId() - 7;
//
//            Royalty byOldID = royaltyService.findByOldID(oldID);
//            idea.getRoyaltyID().setUuid(byOldID.getUuid());
//            idea.getRoyaltyID().setId(byOldID.getId());
//            idea.getRoyaltyID().setName(byOldID.getName());
//        }

        if (idea.getSource() == 0) idea.setSource(1);

        int oldID;
        if (deekaIDs.contains(idea.getRoyaltyID().getId())) {
            idea.setSource(2);
            oldID = idea.getRoyaltyID().getId() - 7;
        }
        else oldID = idea.getRoyaltyID().getId();

        // there is a name change so the project must use royalty id to name the entity again
        Royalty byOldID = royaltyService.findByOldID(oldID);
        idea.getRoyaltyID().setUuid(byOldID.getUuid());
        idea.getRoyaltyID().setId(byOldID.getId());
        idea.getRoyaltyID().setName(byOldID.getName());

        if (idea.getAddress() != null) {
            String[] split = idea.getAddress().split(" ");
            for (int i = 0; i < split.length; i++) {
                String s = split[i];
                if (s.trim().startsWith("จังหวัด")) {
                    split[i] = "";
                }
            }
            idea.setAddress(StringUtils.join(split, ' ').trim());
        }

        DateTime startDate = new DateTime(idea.getDate());
        startDate.withMinuteOfHour(DateTime.now().getMinuteOfHour());
        startDate.withHourOfDay(DateTime.now().getHourOfDay());

        if (startDate.getYear() > 2400) idea.setDate(startDate.minusYears(543).toDate());
        idea.setYear((short) startDate.getYear());

        if (idea.getYear() < 2400) idea.setYear((short) (idea.getYear() + 543));


    }

    private void convertImage(Idea idea) {
        if (idea.getImgFront() != null && idea.getImgFront().startsWith(OLD_SITE_PREFIX)) {
            idea.setImgFront(idea.getImgFront().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX));
        }
        if (idea.getImgCover() != null && idea.getImgCover().startsWith(OLD_SITE_PREFIX)) {
            idea.setImgCover(idea.getImgCover().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX));
        }


        if (idea.getImages().size() == 0 && idea.getImgFront() != null) {
            RoyalProjectImage royalProjectImage = new RoyalProjectImage();
            royalProjectImage.setServeURL(idea.getImgFront());
            idea.getImages().add(royalProjectImage);
            update(idea.getUuid(), idea);
        }

        // convert from detail as well
        if (idea.getImages().size()==1 && idea.getImages().get(0).getServeURL().equals(idea.getImgFront())){
            org.jsoup.nodes.Document doc = Jsoup.parse(idea.getDisplay());
            Elements img = doc.select("img");

            HashSet<String> redundantCheck = new HashSet<String>();


            for (Element element : img) {
                String src = element.attr("src").replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX);
                if (exists(src) && !redundantCheck.contains(src)) {
                    RoyalProjectImage royalProjectImage = new RoyalProjectImage();
                    royalProjectImage.setServeURL(src);
                    idea.getImages().add(royalProjectImage);
                    redundantCheck.add(src);
                }
            }
        }
    }

    private void createIndexIdea(Idea idea) {
        if (!check) {
            check = true;
            init();
        }
        try {
            DateTime date = new DateTime(idea.getDate());
            String monthString = ThaiDateFormat.getMonth(date.getMonthOfYear());

            Document.Builder document = Document.newBuilder().setId(idea.getUuid() + "")
                    .addField(Field.newBuilder().setName("address").setText(idea.getAddress()))
                    .addField(Field.newBuilder().setName("province").setAtom(idea.getProvinceID() != null ? idea.getProvinceID().getName() : ""))
                    .addField(Field.newBuilder().setName("year").setText(idea.getYear() + ""))
                    .addField(Field.newBuilder().setName("royalty").setNumber(idea.getRoyaltyID() != null ? idea.getRoyaltyID().getId() : 0))
                    .addField(Field.newBuilder().setName("source").setNumber(idea.getSource()))
                    .addField(Field.newBuilder().setName("detail").setText(idea.getDetail()))
                    .addField(Field.newBuilder().setName("month").setText(monthString))
                    .addField(Field.newBuilder().setName("date").setDate(idea.getDate() != null ? idea.getDate() : new Date()));
            index.put(document);
        } catch (Exception e) {
            e.printStackTrace();
//            logger.warn("not running in appengine");
        }

    }

    private void init() {
        for (int i = 8; i < 13; i++) {
            deekaIDs.add(i);
        }
        IndexSpec indexSpec = IndexSpec.newBuilder().setName(INDEX_NAME).build();
        index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
    }
    public static boolean exists(String URLName) {
        try {
//            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
