package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.LogEntry;
import com.loxley.rdpmap.domain.ProjectTracker;
import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Knowledge;
import com.loxley.rdpmap.domain.old.Project;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by cmmad_000 on 7/16/2015.
 */
public interface LogService {
//    login 1
//    logout 1
//    อนุมัติ 1
//    โครงการ 4 (ค้นหา อัพเดต สร้าง ลบ)
//    พระราชดำริ 4
//    แนวคิด 4
//    ความคืบหน้า 4
//    dashboard 1

    public void login(Principal principal);

    public void logout(Principal principal);

    public void approve(Principal principal);

    void searchProject(Principal principal);

    void createProject(Project project, Principal principal);

    void updateProject(Project project, Principal principal);

    void deleteProject(Project project, Principal pricipal);

    // idea
    void searchIdea(Principal principal);

    void createIdea(Idea idea, Principal principal);

    void updateIdea(Idea idea, Principal principal);

    void deleteIdea(Idea idea, Principal pricipal);

    // knowledge
    void searchKnowledge(Principal principal);

    void createKnowledge(Knowledge knowledge, Principal principal);

    void updateKnowledge(Knowledge knowledge, Principal principal);

    void deleteKnowledge(Knowledge knowledge, Principal pricipal);

    // tracking
    void searchTracking(Principal principal);

    void createTracking(Principal principal);

    void updateTracking(Principal principal);

    void deleteTracking(Principal pricipal);

    // dashboard
    void dashboard(Principal principal);


    // find
    List<LogEntry> findByDate(Date date);

}
