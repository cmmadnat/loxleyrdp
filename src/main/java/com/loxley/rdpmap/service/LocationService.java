package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.old.Amphur;
import com.loxley.rdpmap.domain.old.District;
import com.loxley.rdpmap.domain.old.Province;
import com.loxley.rdpmap.domain.old.Region;
import com.loxley.rdpmap.domain.old.embedded.EAmphur;
import com.loxley.rdpmap.domain.old.embedded.EProvince;

import java.util.List;

/**
 * Created by cmmad_000 on 3/20/2015.
 */
public interface LocationService {
    int countAmphur();

    int countProvince();

    int countRegion();

    List<Amphur> findAllAmphur();

    List<Province> findAllProvince();

    List<Region> findAllRegion();

    List<District> findAllDistrict();

    Province findOneProvince(Long aLong);

    List<Amphur> findAmphurByProvince(Long provinceID);

    void reset();

    List<District> findTumbonByAmphur(Long amphurID);

    Amphur findOneAmphur(Long amphurID);

    Amphur findAmphurByOldID(Integer id);

    District findOneDistrict(Long aLong);

    EProvince findOneProvinceByOldID(Integer id);

    EAmphur findOneAmphurByOldID(Integer id);

    void updateTumbon();

    List<Region> findAllCountry();

    Province findProvinceByName(String country);

    List<Province> findProvinceByRegion(Long regionID);
}
