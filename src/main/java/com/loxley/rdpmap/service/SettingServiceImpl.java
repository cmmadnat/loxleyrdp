package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.loxley.rdpmap.domain.Setting;
import com.loxley.rdpmap.ui.Slider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 3/14/2015.
 */
public class SettingServiceImpl implements SettingService {
    Logger logger = LoggerFactory.getLogger(SettingService.class);

    ObjectMapper mapper = new ObjectMapper();


    Boolean check = false;
    private List<Slider> sliders = null;

    public void init() {
        if (getSlider().size() == 0) {
            List<Slider> newSlider = new ArrayList<Slider>();

            Slider slider1 = new Slider("line1", "line2", "line3", "sub1", "sub2",
                    "http://km.rdpb.go.th/Content/uploads/files/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%A3%E0%B8%B2%E0%B8%8A%E0%B8%94%E0%B8%B3%E0%B8%A3%E0%B8%B4%20901%20v1.jpg", 0);
            Slider slider2 = new Slider("line1", "line2", "line3", "sub1", "sub2",
                    "http://km.rdpb.go.th/Content/uploads/files/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%A3%E0%B8%B2%E0%B8%8A%E0%B8%94%E0%B8%B3%E0%B8%A3%E0%B8%B4%20901.jpg", 1);
            newSlider.add(slider1);
            newSlider.add(slider2);
            putSlider(newSlider);
        }
    }

    @Override
    public void putSlider(List<Slider> newSlider) {
        Setting target = ofy().load().type(Setting.class).first().now();
        if (target == null) {
            target = new Setting();
            target.setKey("slider");
        }

        try {
            if (target.getValue() != null && !target.getValue().isEmpty())
                sliders = mapper.readValue(target.getValue(), new TypeReference<List<Slider>>() {
                });
            else
                sliders = new ArrayList<Slider>();
            sliders.addAll(newSlider);
            target.setValue(mapper.writeValueAsString(sliders));
        } catch (JsonProcessingException e) {

        } catch (IOException e) {

        }

        ofy().save().entity(target);
    }

    @Override
    public List<Slider> getSlider() {

        if (!check) {
            check = true;
            init();
        }

        // cache slider by object

        if (sliders != null) return sliders;

        Setting target = ofy().load().type(Setting.class).filter("key", "slider").first().now();
        if (target == null) return new ArrayList<Slider>();

        try {
            sliders = mapper.readValue(target.getValue(), new TypeReference<List<Slider>>() {
            });
            return sliders;
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("can't deserialize slider");
        }
        return new ArrayList<Slider>();
    }

    @Override
    public void deleteSlider(int index) {
        Slider slider = sliders.get(index);
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        if (slider.getKey() != null && !slider.getKey().isEmpty()) {
            try {
                blobstoreService.delete(new BlobKey(slider.getKey()));
            } catch (Exception e) {
                logger.error("error removing the key");
            }
        }

        sliders.remove(index);

        Setting target = ofy().load().type(Setting.class).filter("key", "slider").first().now();
        try {
            target.setValue(mapper.writeValueAsString(sliders));
            ofy().save().entity(target);

        } catch (JsonProcessingException e) {
            logger.error("error setting value for slider");
        }

    }

    @Override
    public Setting get(String key) {

        Setting setting = ofy().load().type(Setting.class).filter("key", key).first().now();
        return setting;
    }

    @Override
    public void set(String latLngPage, String value) {
        Setting setting = get(latLngPage);
        if (setting == null) {
            setting = new Setting();
            setting.setKey(latLngPage);
        }
        setting.setValue(value);
        ofy().save().entity(setting);
    }

}
