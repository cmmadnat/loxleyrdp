package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loxley.rdpmap.domain.old.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 3/28/2015.
 */
public class DepartmentServiceImpl implements DepartmentService, ResourceLoaderAware {
    private boolean check = false;
    private ResourceLoader loader;

    ObjectMapper mapper = new ObjectMapper();

    Logger logger = LoggerFactory.getLogger(DepartmentServiceImpl.class);

    @Override
    public List<Department> search(String query) {
        if (!check) {
            check = true;
            init();

        }
        return ofy().load().type(Department.class).filter("name >=", query).filter("name <", query + "\uFFFD").list();

    }

    private void init() {
        if (count() == 0) {
            try {
                File file = loader.getResource("classpath:department.json").getFile();
                List<Department> departments = mapper.readValue(file, new TypeReference<List<Department>>() {
                });
                ofy().save().entities(departments);
            } catch (IOException e) {
                logger.error("error loading department json");

            }
        }

    }

    @Override
    public int count() {
        return ofy().load().type(Department.class).count();
    }

    @Override
    public Department findOne(Long aLong) {
        return ofy().load().type(Department.class).id(aLong).now();
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.loader = resourceLoader;
    }
}
