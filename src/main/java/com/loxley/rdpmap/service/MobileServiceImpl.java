package com.loxley.rdpmap.service;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.GeoPt;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.mobile.MobileImage;
import com.loxley.rdpmap.domain.mobile.MobileLocation;
import com.loxley.rdpmap.domain.old.Project;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 5/21/2015.
 */
public class MobileServiceImpl implements MobileService {
    @Autowired
    ProjectService projectService;

    @Override
    public void saveLocation(Long id, String lat, String lng, String name) {
        MobileLocation target = new MobileLocation();

        Project project = new Project();
        project.setUuid(id);

        target.setProject(project);
        target.setUsername(name);
        target.setLocation(new GeoPt(Float.valueOf(lat), Float.valueOf(lng)));
        ofy().save().entity(target);
    }

    @Override
    public void saveImage(Long id, String keyString, String servingUrl, String username) {
        MobileImage mobileImage = new MobileImage();
        Project project = new Project();
        project.setUuid(id);

        mobileImage.setProject(project);
        mobileImage.setKey(keyString);
        mobileImage.setServingUrl(servingUrl);
        mobileImage.setUsername(username);
        ofy().save().entities(mobileImage);
    }

    @Override
    public List<MobileImage> findAllImage() {
        List<MobileImage> list = ofy().load().type(MobileImage.class).order("-date").list();
        ArrayList<MobileImage> removeList = new ArrayList<MobileImage>();
        for (MobileImage mobileImage : list) {
            if (mobileImage.getProject() == null) {
                removeList.add(mobileImage);
            }
        }
        if (removeList.isEmpty())
            return list;
        else {
            for (MobileImage mobileImage : removeList) {
                discardChangePhoto(mobileImage.getId());
            }
            return ofy().load().type(MobileImage.class).order("-date").list();
        }
    }

    @Override
    public void commitChangePhoto(Long id) {
        MobileImage now = ofy().load().type(MobileImage.class).id(id).now();
        Project project = now.getProject();

        RoyalProjectImage image = new RoyalProjectImage();
        image.setServeURL(now.getServingUrl());
        image.setKey(now.getKey());

        project.getImages().add(image);
        projectService.update(project.getUuid(), project);

        ofy().delete().entity(now);
    }

    @Override
    public void discardChangePhoto(Long id) {
        MobileImage now = ofy().load().type(MobileImage.class).id(id).now();
        try {
            BlobstoreServiceFactory.getBlobstoreService().delete(new BlobKey(now.getKey()));
        } catch (Exception e) {

        }

        ofy().delete().entity(now);
    }

    @Override
    public List<MobileLocation> findAllLocation() {
        List<MobileLocation> list = ofy().load().type(MobileLocation.class).order("-date").list();
        ArrayList<MobileLocation> removeList = new ArrayList<MobileLocation>();
        for (MobileLocation mobileLocation : list) {
            if (mobileLocation.getProject() == null) {
                removeList.add(mobileLocation);
            }
        }
        if (removeList.isEmpty())
            return list;
        else {
            for (MobileLocation mobileLocation : removeList) {
                discardChangeLocation(mobileLocation.getId());
            }
            return ofy().load().type(MobileLocation.class).order("-date").list();
        }
    }

    @Override
    public void discardChangeLocation(Long id) {
        MobileLocation now = ofy().load().type(MobileLocation.class).id(id).now();
        ofy().delete().entity(now);
    }

    @Override
    public void commitChangeLocation(Long id) {
        MobileLocation now = ofy().load().type(MobileLocation.class).id(id).now();
        Project project = now.getProject();
        project.setLocation(now.getLocation());

        projectService.update(project.getUuid(), project);

        ofy().delete().entity(now);

    }

    @Override
    public Integer countAwaitApprove() {
        return ofy().load().type(MobileImage.class).count() + ofy().load().type(MobileLocation.class).count();
    }


}
