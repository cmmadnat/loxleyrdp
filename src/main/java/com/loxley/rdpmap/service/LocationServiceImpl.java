package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.datastore.GeoPt;
import com.loxley.rdpmap.domain.old.Amphur;
import com.loxley.rdpmap.domain.old.District;
import com.loxley.rdpmap.domain.old.Province;
import com.loxley.rdpmap.domain.old.Region;
import com.loxley.rdpmap.domain.old.embedded.EAmphur;
import com.loxley.rdpmap.domain.old.embedded.EProvince;
import com.loxley.rdpmap.domain.old.embedded.ERegion;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;
import static com.googlecode.objectify.ObjectifyService.register;

/**
 * Created by cmmad_000 on 3/20/2015.
 */
public class LocationServiceImpl implements LocationService, ResourceLoaderAware {

//    ภาคกลาง:14.881087, 100.261230
//    ภาคเหนือ:18.229351, 99.492188
//    ภาคตะวันออกเฉียงเหนือ:16.235772, 102.985840
//
//    ภาคใต้:8.450639, 99.206543
//    ต่างประเทศ(ลาว) :18.206296,103.889022

    Boolean check = false;
    Boolean checking = false;
    Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);
    private ResourceLoader loader;
    private List<Amphur> amphurs;
    private List<Province> province;

    public void init() {
        if (checking) return;

        checking = true;
        ObjectMapper mapper = new ObjectMapper();
        checkAmphur(mapper);
        checkProvice(mapper);
        checkRegion(mapper);
        checkDistrict(mapper);
        checkCountry();
    }

    private void checkCountry() {
        List<Region> allRegion = findAllCountry();
        boolean found = false;
        for (Region region : allRegion) {
            if (region.getName().equals("ไทย")) found = true;
        }
        if (!found) {
            Region region = new Region();
            region.setName("ไทย");
            region.setIsCountry(true);
            ofy().save().entity(region).now();
        }
        found = false;
        for (Region region : allRegion) {
            if (region.getName().equals("สปป.ลาว")) found = true;
        }
        if (!found) {
            Region region = new Region();
            region.setName("สปป.ลาว");
            region.setIsCountry(true);
            ofy().save().entity(region).now();
        }
    }

    private void checkDistrict(ObjectMapper mapper) {
        if (countDistrict() == 0) try {
            File file = loader.getResource("classpath:district.json").getFile();
            List<District> districts = mapper.readValue(file, new TypeReference<List<District>>() {
            });
            ofy().save().entities(districts);
        } catch (IOException e) {
            logger.error("cannot get province file");
        }
    }

    private int countDistrict() {
        return ofy().load().type(District.class).count();
    }

    private void checkRegion(ObjectMapper mapper) {
        if (countRegion() == 0) try {
            File file = loader.getResource("classpath:region.json").getFile();
            List<Region> regions = mapper.readValue(file, new TypeReference<List<Region>>() {
            });
            ofy().save().entities(regions).now();
        } catch (IOException e) {
            logger.error("cannot get province file");
        }
        List<Region> allRegion = findAllRegion();

        for (Region region : allRegion) {
            if (region.getName().equals("ต่างประเทศ")) {
                region.setName("สปป.ลาว");
                region.setIsCountry(true);
                Province provinceByName = findProvinceByName("สปป.ลาว");

                provinceByName.setIsCountry(true);
                ERegion eRegion = new ERegion();
                try {
                    BeanUtils.copyProperties(eRegion, region);
                } catch (IllegalAccessException e) {
                } catch (InvocationTargetException e) {
                }
                provinceByName.setRegionID(eRegion);
                ofy().save().entity(provinceByName);

            }

            String regionName = "ภาคกลาง";
            float lng = 100.261230f;
            float lat = 14.881087f;
            checkRegionLocation(region, regionName, lng, lat);

            regionName = "ภาคตะวันออกเฉียงเหนือ";
            lng = 102.985840f;
            lat = 16.235772f;
            checkRegionLocation(region, regionName, lng, lat);

            regionName = "ภาคใต้";
            lng = 99.206543f;
            lat = 8.450639f;
            checkRegionLocation(region, regionName, lng, lat);

            regionName = "ภาคเหนือ";
            lng = 99.492188f;
            lat = 18.229351f;
            checkRegionLocation(region, regionName, lng, lat);

            regionName = "สปป.ลาว";
            lng = 103.889022f;
            lat = 18.206296f;
            checkRegionLocation(region, regionName, lng, lat);

        }

    }

    private void checkRegionLocation(Region region, String regionName, float lng, float lat) {
        if (region.getName().equals(regionName) && region.getLocation().getLatitude() == 0) {
            region.setLocation(new GeoPt(lat, lng));
            ofy().save().entity(region);
        }
    }

    private void checkProvice(ObjectMapper mapper) {
        if (countProvince() == 0) try {
            File file = loader.getResource("classpath:province.json").getFile();
            List<Province> provinces = mapper.readValue(file, new TypeReference<List<Province>>() {
            });

            ofy().save().entities(provinces);
        } catch (IOException e) {
            logger.error("cannot get province file", e);
        }
    }

    private void checkAmphur(ObjectMapper mapper) {
        if (countAmphur() == 0) try {
            File file = loader.getResource("classpath:amphur.json").getFile();
            List<Amphur> amphurs = mapper.readValue(file, new TypeReference<List<Amphur>>() {
            });

            ofy().save().entities(amphurs);
        } catch (IOException e) {
            logger.error("cannot get amphur file");
        }
    }

    @Override
    public int countAmphur() {
        return ofy().load().type(Amphur.class).count();
    }

    @Override
    public int countProvince() {
        return ofy().load().type(Province.class).count();
    }

    @Override
    public int countRegion() {
        return ofy().load().type(Region.class).count();
    }

    @Override
    public List<Amphur> findAllAmphur() {

        if (this.amphurs == null)
            return this.amphurs = ofy().load().type(Amphur.class).order("name").list();
        else return this.amphurs;
    }

    @Override
    public List<Province> findAllProvince() {
        if (check == false) {
            check = true;
            init();
        }
        if (this.province == null)
            return this.province = ofy().load().type(Province.class).order("name").list();
        else return this.province;
    }

    @Override
    public List<Region> findAllRegion() {
        List<Region> list = ofy().load().type(Region.class).filter("isCountry", false).list();
        return list;
    }

    @Override
    public List<District> findAllDistrict() {
        return ofy().load().type(District.class).order("name").list();

    }

    @Override
    public Province findOneProvince(Long aLong) {
        if (aLong == 0) return null;
        return ofy().load().type(Province.class).id(aLong).now();
    }

    @Override
    public List<Amphur> findAmphurByProvince(Long provinceID) throws NullPointerException {
        if (provinceID == 0) return null;
        Integer id = findOneProvince(provinceID).getId();
        List<Amphur> list = ofy().load().type(Amphur.class).filter("provinceID.id", id).list();

        return list;
    }

    @Override
    public void reset() {
        List<Amphur> list = ofy().load().type(Amphur.class).list();
        ofy().delete().entities(list);
        List<District> list1 = ofy().load().type(District.class).list();
        ofy().delete().entities(list1);
        List<Province> list2 = ofy().load().type(Province.class).list();
        ofy().delete().entities(list2);
        init();
    }

    @Override
    public List<District> findTumbonByAmphur(Long amphurID) throws NullPointerException {
        if (amphurID == null || amphurID == 0) return null;
        Amphur amphur = findOneAmphur(amphurID);
        return ofy().load().type(District.class).filter("amphurID.id", amphur.getId()).list();
    }

    @Override
    public Amphur findOneAmphur(Long amphurID) {
        if (amphurID == null || amphurID == 0) return null;
        return ofy().load().type(Amphur.class).id(amphurID).now();
    }

    @Override
    public Amphur findAmphurByOldID(Integer id) {
        return ofy().load().type(Amphur.class).filter("id", id).first().now();

    }

    @Override
    public District findOneDistrict(Long aLong) {
        if (aLong == null || aLong == 0) return null;
        return ofy().load().type(District.class).id(aLong).now();
    }

    @Override
    public EProvince findOneProvinceByOldID(Integer id) {
        Province province = ofy().load().type(Province.class).filter("id", id).first().now();
        EProvince target = new EProvince();
        try {
            BeanUtils.copyProperties(target, province);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return target;

    }

    @Override
    public EAmphur findOneAmphurByOldID(Integer id) {

        Amphur amp = ofy().load().type(Amphur.class).filter("id", id).first().now();
        EAmphur target = new EAmphur();
        try {
            BeanUtils.copyProperties(target, amp);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return target;
    }

    @Override
    public void updateTumbon() {
        final List<District> allDistrict = findAllDistrict();
        ofy().save().entities(allDistrict);
    }


    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.loader = resourceLoader;
    }

    @Override
    public List<Region> findAllCountry() {
        List<Region> isCountry = ofy().load().type(Region.class).filter("isCountry", true).list();
        if (isCountry.get(0).getName() != "ไทย") {
            int counter = 0;
            int index = 0;

            for (Region region : isCountry) {
                if (region.getName().equals("ไทย")) index = counter;
                else counter++;
            }

            Collections.swap(isCountry, 0, index);
        }
        return isCountry;
    }

    @Override
    public Province findProvinceByName(String country) {
        return ofy().load().type(Province.class).filter("name", country).first().now();
    }


    boolean checkRegionProvice = false;

    @Override
    public List<Province> findProvinceByRegion(Long regionID) {
        if (!checkRegionProvice) {
            checkRegionProvice = true;
            if (ofy().load().type(Province.class).filter("regionIndex", regionID).count() == 0) {
                List<Province> allProvince = findAllProvince();
                List<Region> allRegion = findAllRegion();
                for (Province province1 : allProvince) {
                    for (Region region : allRegion) {
                        if (province1.getRegionID().getName().equals(region.getName())){
                            province1.setRegionIndex(region.getUuid());
                        }
                    }
                }
                ofy().save().entities(allProvince);
            }
        }

        return ofy().load().type(Province.class).filter("regionIndex", regionID).list();
    }
}
