package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.search.*;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.common.collect.Lists;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.Setting;
import com.loxley.rdpmap.domain.old.*;
import com.loxley.rdpmap.domain.old.embedded.EDepartment;
import com.loxley.rdpmap.domain.old.embedded.EProjectDepartment;
import com.loxley.rdpmap.domain.old.embedded.EProjectState;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 3/4/2015.
 */
public class ProjectServiceImpl implements ProjectService, ResourceLoaderAware {
    private static final int UPDATE_LIMIT = 500;
    public static final String OLD_SITE_PREFIX = "http://km.rdpb.go.th/Content/uploads/images";
    private static final String NEW_SITE_PREFIX = "http://storage.googleapis.com/rdp-archive";
    private static final int LAT_LNG_IMPORT_LIMIT = 25;
    public static final String PDF_HOST = "http://27.254.68.103:8081";
    public static final int FIRST_PAGE_IMPORT_COUNT = 200;


    @Autowired
    SettingService settingService;
    @Autowired
    RoyaltyService royaltyService;
    @Value("${google.key}")
    String googleKey;
    @Value("${exporter.host}")
    String exporterURL;

    public static float DEFAULT_LAT = 0f;
    public static float DEFAULT_LNG = 0f;

    HashSet<Integer> deekaIDs = new HashSet<Integer>();

    private final String INDEX_NAME = "projectIndex";
    Logger logger = LoggerFactory.getLogger(ProjectService.class);
    ObjectMapper mapper = new ObjectMapper();
    Boolean check = false;
    private Index index;
    private ResourceLoader loader;
    private List<ProjectType> type;

    String[] status = new String[]{"ไม่ได้เพิ่มข้อมูล", "ไม่ครบถ้วนมีเฉพาะข้อมูล",
            "ไม่ครบถ้วนมีเฉพาะรูป", "ครบถ้วน", "ยังไม่ระบุพิกัดที่ตั้งโครงการ"};

    private ArrayList<EProjectState> states;
    String[] source = new String[]{"พระราชดำริ", "ราษฎรขอพระราชทานพระมหากรุณา (ฎีกา)", "หน่วยงานขอพระราชทาน"};
    private EProjectState defaultState;

    @Override
    public String translateSource(int i) {
        return source[i - 1];
    }

    @Override
    public List<String> findAllState() {
        ArrayList<String> strings = Lists.newArrayList(status);
        return strings;
    }


    public void init() {
        for (int i = 8; i < 13; i++) {
            deekaIDs.add(i);
        }

        try {
            IndexSpec indexSpec = IndexSpec.newBuilder().setName(INDEX_NAME).build();
            index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
        } catch (Exception e) {
            logger.warn("not running in app engine");
        }
        // count the state
        if (ofy().load().type(ProjectType.class).count() == 0) {

            try {
                List<ProjectType> list = mapper.readValue(loader.getResource("classpath:projecttype.json").getFile(), new TypeReference<List<ProjectType>>() {
                });
                ofy().save().entities(list);
            } catch (IOException e) {
                logger.error("can't import project state json");
            }
        }

        states = new ArrayList<EProjectState>();
        for (String s : status) {
            EProjectState es = new EProjectState();
            es.setName(s);
            states.add(es);
        }

        defaultState = new EProjectState();
        defaultState.setName("ยังไม่ระบุพิกัดที่ตั้งโครงการ");
    }

    @Override
    public void saveProjects(List<Project> values) {
        for (Project value : values) {
            value.setStateID(defaultState);
            processProject(value);
        }
        Collection<Project> list = ofy().save().entities(values).now().values();
        for (Project project : list) {
            createIndexProject(project);
        }


    }

    private void createIndexProject(Project project) {

        try {
            Document.Builder document = Document.newBuilder().setId(project.getUuid() + "")
                    .addField(Field.newBuilder().setName("name").setText(project.getName()))
                    .addField(Field.newBuilder().setName("type").setText(project.getTypeID() != null ? project.getTypeID().getName() : ""))
                    .addField(Field.newBuilder().setName("region").setText(project.getProvinceID().getRegionID() != null ? project.getProvinceID().getRegionID().getName() : ""))
                    .addField(Field.newBuilder().setName("province").setText(project.getProvinceID() != null ? project.getProvinceID().getName() : ""))
                    .addField(Field.newBuilder().setName("state").setAtom(project.getStateID() != null ? project.getStateID().getName() : ""))
                    .addField(Field.newBuilder().setName("year").setText(project.getYear() + ""))
                    .addField(Field.newBuilder().setName("royalty").setNumber(project.getRoyaltyID() != null ? project.getRoyaltyID().getId() : 0))
                    .addField(Field.newBuilder().setName("location")
                            .setGeoPoint(new GeoPoint(project.getLocation().getLatitude(), project.getLocation().getLongitude())))
                    .addField(Field.newBuilder().setName("source").setNumber(project.getSource()));
            index.put(document);
        } catch (Exception e) {
            logger.warn("not running in appengine", e);
        }

    }

    @Override
    public List<Project> findAll(int page, int limit) {
        return ofy().load().type(Project.class).order("-year").limit(limit).offset(page * limit).list();
    }


    @Override
    public Project findOne(Long id) {
        Project project = ofy().load().type(Project.class).id(id).now();
        if (project != null)
            processProject(project);

        return project;
    }


    private void processProject(Project project) {
        if (project.getUuid() != null && project.getMainProject() != null &&
                project.getUuid() == project.getMainProject().getUuid())
            project.setMainProject(null);
        // move the old deeka to the new one
        if (project.getSource() == 0) project.setSource(1);

        int oldID;
        if (deekaIDs.contains(project.getRoyaltyID().getId())) {
            project.setSource(2);
            oldID = project.getRoyaltyID().getId() - 7;
        } else oldID = project.getRoyaltyID().getId();

        // there is a name change so the project must use royalty id to name the entity again
        Royalty byOldID = royaltyService.findByOldID(oldID);
        project.getRoyaltyID().setUuid(byOldID.getUuid());
        project.getRoyaltyID().setId(byOldID.getId());
        project.getRoyaltyID().setName(byOldID.getName());


        if (project.getProvinceID() != null && project.getProvinceID().getUuid() == null) {
            Province id1 = ofy().load().type(Province.class).filter("id", project.getProvinceID().getId()).first().now();
            if (id1 != null) project.getProvinceID().setUuid(id1.getUuid());
        }
        if (project.getAmphurID() != null && project.getAmphurID().getUuid() == null) {
            Amphur id1 = ofy().load().type(Amphur.class).filter("id", project.getAmphurID().getId()).first().now();
            if (id1 != null) project.getAmphurID().setUuid(id1.getUuid());
        }
        if (project.getDistrictID() != null && project.getDistrictID().getUuid() == null) {
            District id1 = ofy().load().type(District.class).filter("id", project.getDistrictID().getId()).first().now();
            if (id1 != null) project.getDistrictID().setUuid(id1.getUuid());
        }


        if (project.getDepartmentID() != null && project.getDepartments().isEmpty()) {
            EDepartment target = new EDepartment();
            try {
                final Department department = ofy().load().type(Department.class).filter("id", project.getDepartmentID().getId()).first().now();
                BeanUtils.copyProperties(target, project.getDepartmentID());
                project.getDepartments().add(target);
            } catch (IllegalAccessException e) {
                logger.error("add department to collection");
            } catch (InvocationTargetException e) {
                logger.error("add department to collection");
            }
        }
        for (EProjectDepartment eProjectDepartment : project.getProjectDepartmentCollection()) {
            EDepartment target = null;
            try {
                final Department department = ofy().load().type(Department.class).filter("id", eProjectDepartment.getDepartmentID()).first().now();
                if (department == null) continue;
                target = new EDepartment();
                BeanUtils.copyProperties(target, department);
                project.getDepartments().add(target);
            } catch (IllegalAccessException e) {
                logger.error("add department to collection");
            } catch (InvocationTargetException e) {
                logger.error("add department to collection");
            }
        }

        ArrayList<Idea> ideas = project.getIdeas();
        ideas.removeAll(Collections.singleton(null));

        // move the pic from old location to gcs
        if (project.getImgFront() != null && project.getImgFront().startsWith(OLD_SITE_PREFIX)) {
            project.setImgFront(project.getImgFront().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX));
        }
        if (project.getImgCover() != null && project.getImgCover().startsWith(OLD_SITE_PREFIX)) {
            project.setImgCover(project.getImgCover().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX));
        }

        processMonthYear(project);
    }


    @Override
    public void update(Long id, Project target) {
        target.setUuid(id);
        createIndexProject(target);

        processProject(target);

        target.setTime(new Date());
        ofy().save().entities(target);
    }

    private void processMonthYear(Project target) {
        // process month, year
        if (target.getTotalMonth() < 0) target.setTotalMonth(0);
        if (target.getTotalYear() < 0) target.setTotalYear(0);

        int addYear = target.getTotalMonth() / 12;
        if (addYear != 0) {
            target.setTotalYear(target.getTotalYear() + addYear);
            target.setTotalMonth(target.getTotalMonth() % 12);
        }

        DateTime startDate = new DateTime(target.getStartDate());
        if (startDate.getYear() > 2400) target.setStartDate(startDate.minusYears(543).toDate());
        DateTime finishDate = new DateTime(target.getFinishDate());
        if (finishDate.getYear() > 2400) target.setFinishDate(finishDate.minusYears(543).toDate());


    }

    @Override
    public void delete(Project project) {
        if (!check) {
            check = true;
            init();
        }

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

        List<RoyalProjectImage> images = project.getImages();
        for (RoyalProjectImage image : images) {
            blobstoreService.delete(new BlobKey(image.getKey()));
        }
        List<RoyalProjectFile> files = project.getFiles();
        for (RoyalProjectFile file : files) {
            blobstoreService.delete(new BlobKey(file.getKey()));
        }

        ofy().delete().entities(project);
        index.delete(project.getUuid().toString());
    }

    @Override
    public int count() {
        return ofy().load().type(Project.class).count();
    }

    @Override
    public List<Project> search(String query, int page, int limit) {
        if (!check) {
            check = true;
            init();
        }

        if (query.isEmpty() || query.equals("name=")) return findAll(page, limit);

        QueryOptions option = QueryOptions.newBuilder()
                .setLimit(limit)
                .setOffset(limit * page)
                .setSortOptions(
                        SortOptions.newBuilder()
                                .addSortExpression(
                                        SortExpression.newBuilder().
                                                setDirection(SortExpression.SortDirection.DESCENDING).
                                                setExpression("year")))
                .build();
        Results<ScoredDocument> list = index.search(Query.newBuilder().setOptions(option).build(query));
        List<Project> output = new ArrayList<Project>();

        for (ScoredDocument scoredDocument : list) {
            Project one = findOne(Long.valueOf(scoredDocument.getId()));
            if (one != null) output.add(one);
        }

        return output;
    }

    @Override
    public long countSearch(String query) {
        QueryOptions request = QueryOptions.newBuilder()
                .setNumberFoundAccuracy(200)
                .build();
        Query q = Query.newBuilder().setOptions(request).build(query);
        long numberFound = index.search(q).getNumberFound();
        return numberFound;

    }

    @Override
    public Long save(Project project) {
//        processMonthYear(project);
        processProject(project);
        Key<Project> key = ofy().save().entity(project).now();
        project.setUuid(key.getId());
        createIndexProject(project);
        return key.getId();
    }

    @Override
    public List<ProjectType> findAllType() {
        if (!check) {
            check = true;
            init();
        }

        if (this.type == null)
            this.type = ofy().load().type(ProjectType.class).list();

        return this.type;
    }

    @Override
    public ProjectType findOneType(Long aLong) {
        return ofy().load().type(ProjectType.class).id(aLong).now();
    }

    @Override
    public void updateIndex() {
        if (!check) {
            check = true;
            init();
        }
        com.google.appengine.api.taskqueue.Queue defaultQueue = QueueFactory.getDefaultQueue();
        int count = ofy().load().type(Project.class).count();
        int numberOfPage = count / UPDATE_LIMIT + 1;

        while (true) {
            List<String> docIds = new ArrayList<String>();
            // Return a set of doc_ids.
            GetRequest request = GetRequest.newBuilder().setReturningIdsOnly(true).build();
            GetResponse<Document> response = index.getRange(request);
            if (response.getResults().isEmpty()) {
                break;
            }
            for (Document doc : response) {
                docIds.add(doc.getId());
            }
            index.delete(docIds);
        }

        for (int i = 0; i < numberOfPage; i++) {
            defaultQueue.add(TaskOptions.Builder.withUrl("/import/index/project").param("page", i + ""));
        }
    }

    @Override
    public void stripIdea(Idea idea) {
        List<Project> projects = ofy().load().type(Project.class).filter("ideas", Ref.create(idea)).list();
        for (Project project : projects) {
            ArrayList<Idea> ideas = project.getIdeas();
            ArrayList<Idea> removeList = new ArrayList<Idea>();
            for (Idea idea1 : ideas) {
                if (idea1.getUuid().equals(idea.getUuid())) {
                    removeList.add(idea1);
                }
            }
            ideas.removeAll(removeList);
            // set idea
            project.setIdeas(ideas);
            update(project.getUuid(), project);
        }

    }

    @Override
    public List<Project> getFirstpageProjects() {
        final ObjectMapper objectMapper = new ObjectMapper();

        Setting firstPageUsageCount = settingService.get("firstPageUsageCount");
        // count usage to 200 and reset the page index
        if (firstPageUsageCount == null || firstPageUsageCount.getValue().equals("200")) {
            settingService.set("firstPageUsageCount", "0");
            firstPageUsageCount = new Setting();
            firstPageUsageCount.setValue("0");
        }


        Setting firstPageProjects = settingService.get("firstPageProjects");
        if (firstPageProjects == null || firstPageUsageCount.getValue().equals("0")) {
            final ArrayList<Long> ids = new ArrayList<Long>();
            final List<Project> list = ofy().load().type(Project.class).limit(FIRST_PAGE_IMPORT_COUNT).order("-year").list();
            int counter = 0;
            do {
                Project item = list.get(counter++);
                if (item.getImages().size() != 0 || (item.getImgCover() != null && item.getImgCover().isEmpty()))
                    ids.add(item.getUuid());
            } while (ids.size() < 4 && counter < FIRST_PAGE_IMPORT_COUNT);
            final String s;
            try {
                s = objectMapper.writeValueAsString(ids);
                settingService.set("firstPageProjects", s);
                firstPageProjects = new Setting();
                firstPageProjects.setValue(s);
            } catch (JsonProcessingException e) {
                logger.error("serialize json in get first page");
            }
        }

        final int next = Integer.parseInt(firstPageUsageCount.getValue()) + 1;
        settingService.set("firstPageUsageCount", next + "");

        final String value = firstPageProjects.getValue();
        final ArrayList<String> o;
        try {
            o = objectMapper.readValue(value, new TypeReference<ArrayList<String>>() {
            });
            final ArrayList<Long> ids = new ArrayList<Long>();
            for (String s : o) {
                ids.add(Long.valueOf(s));
            }
            if (ids.size() != 0) {
                final Collection<Project> values = ofy().load().type(Project.class).ids(ids).values();

                if (values.size() != ids.size()) settingService.set("firstPageUsageCount", "0");

                return new ArrayList<Project>(values);
            } else return findAll(0, 4);

        } catch (IOException e) {
            logger.error("error deserialize the json from setting");
        }

        return findAll(0, 4);
    }


    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.loader = resourceLoader;
    }

    @Override
    public ArrayList<EProjectState> getProjectStates() {
        return states;
    }

    @Override
    public void updateIndexPage(Integer page) {
        if (!check) {
            check = true;
            init();
        }
        ArrayList<String> ids = new ArrayList<String>();

        List<Project> list = ofy().load().type(Project.class).offset(UPDATE_LIMIT * page).limit(UPDATE_LIMIT).list();

        for (Project project : list) {

            // if the project pic is empty, try to look into the pro for image
            if (project.getImages().size() == 0) {
                HashSet<String> redundantCheck = new HashSet<String>();

                if (project.getImgCover() != null) {
                    String cover = project.getImgCover().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX);
                    if (exists(cover)) {
                        RoyalProjectImage royalProjectImage = new RoyalProjectImage();
                        royalProjectImage.setServeURL(cover);
                        project.getImages().add(royalProjectImage);
                        redundantCheck.add(cover);
                    }
                }
                if (project.getDisplay() != null) {
                    org.jsoup.nodes.Document doc = Jsoup.parse(project.getDisplay());
                    Elements img = doc.select("img");

                    for (Element element : img) {
                        String src = element.attr("src").replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX);
                        if (exists(src) && !redundantCheck.contains(src)) {
                            RoyalProjectImage royalProjectImage = new RoyalProjectImage();
                            royalProjectImage.setServeURL(src);
                            project.getImages().add(royalProjectImage);
                            redundantCheck.add(src);
                        }
                    }
                }
                if (project.getImages().size() != 0) {
                    update(project.getUuid(), project);
                }
            }

            if (project.getDisplay() != null && !project.getDisplay().isEmpty() && project.getFiles().isEmpty()) {
                RoyalProjectFile file = new RoyalProjectFile();
                file.setFileName(PDF_HOST + "/project/" + project.getUuid() + "/pdf");
                project.getFiles().add(file);
            }

            processProject(project);
            createIndexProject(project);
        }
        ofy().save().entities(list);
    }

    @Override
    public void wipe() {
        if (!check) {
            check = true;
            init();
        }

        List<Project> all = ofy().load().type(Project.class).list();

        ArrayList<String> ids = new ArrayList<String>();


        while (true) {
            List<String> docIds = new ArrayList<String>();
            // Return a set of doc_ids.
            GetRequest request = GetRequest.newBuilder().setReturningIdsOnly(true).build();
            GetResponse<Document> response = index.getRange(request);
            if (response.getResults().isEmpty()) {
                break;
            }
            for (Document doc : response) {
                docIds.add(doc.getId());
            }
            index.delete(docIds);
        }


        ofy().delete().entities(all);
    }

    @Override
    public Boolean importLatLng(int page) {
        List<Project> all = findAll(page, LAT_LNG_IMPORT_LIMIT);
        for (Project project : all) {
            // ask google for geocoding result
            final float latitude = project.getLocation().getLatitude();
            final float longitude = project.getLocation().getLongitude();
            if (latitude == DEFAULT_LAT && longitude == DEFAULT_LNG) {
                CloseableHttpClient httpclient = HttpClients.createDefault();
                HttpPost httppost = new HttpPost(exporterURL + "/location");

                // Request parameters and other properties.
                List<NameValuePair> params = new ArrayList<NameValuePair>(2);
                String query =
                        project.getDistrictID() != null ? project.getDistrictID().getName() : "";
                query += " ";
                query += project.getAmphurID() != null ? project.getAmphurID().getName() : "";
                query += " ";
                query += project.getProvinceID() != null ? project.getProvinceID().getName() : "";


                params.add(new BasicNameValuePair("query", query));

                try {
                    httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        InputStream instream = entity.getContent();
                        try {
                            // do something useful
                            final String s = IOUtils.toString(instream);
                            if (!s.equals("error")) {
                                final String[] split = s.split(",");
                                project.setLocation(new GeoPt(Float.valueOf(split[0]),
                                        Float.valueOf(split[1])));
                            }
                        } finally {
                            instream.close();
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    logger.error("set entity failed");
                } catch (ClientProtocolException e) {
                    logger.error("execute getting project location error");
                } catch (IOException e) {
                    logger.error("execute getting project location error");

                }
                ofy().save().entity(project);
            }
        }
        return all.size() == 0;
    }

    public static boolean exists(String URLName) {
        try {
//            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void checkRight(String region, Project project) {
        final String name = project.getProvinceID().getRegionID().getName();
        if (region.equals("0") || region.equals("_0")) {
            project.setCanEdit(true);
            return;
        }

        if (name.equals("หลายพื้นที่ดำเนินการ") || name.equals("สปป.ลาว")) {
            project.setCanEdit(true);
            return;
        }
        if (name.equals("ภาคกลาง")) {
            if (region.equals("1")) project.setCanEdit(true);
            return;
        }
        if (name.equals("ภาคตะวันออกเฉียงเหนือ")) {
            if (region.equals("2")) project.setCanEdit(true);
            return;
        }

        if (name.equals("ภาคเหนือ")) {
            if (region.equals("3")) project.setCanEdit(true);
            return;
        }
        if (name.equals("ภาคใต้")) {
            if (region.equals("4")) project.setCanEdit(true);
            return;
        }
    }

}
