package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.RoyalProject;
import com.loxley.rdpmap.domain.old.Project;

import java.util.List;

public interface RoyalProjectService {

	List<RoyalProject> findAll(int i, int pageLimit);

	int pageCount(int pageLimit);


	RoyalProject findOne(Long id);


	void delete(Long id);


	List<RoyalProject> convert(List<Project> search);
}
