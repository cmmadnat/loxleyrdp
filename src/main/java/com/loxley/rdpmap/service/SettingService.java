package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.Setting;
import com.loxley.rdpmap.ui.Slider;

import java.util.List;

/**
 * Created by cmmad_000 on 3/14/2015.
 */
public interface SettingService {
    void putSlider(List<Slider> newSlider);

    List<Slider> getSlider();

    void deleteSlider(int index);

    Setting get(String latLngPage);

    void set(String latLngPage, String value);
}
