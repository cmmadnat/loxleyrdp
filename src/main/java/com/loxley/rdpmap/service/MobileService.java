package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.mobile.MobileImage;
import com.loxley.rdpmap.domain.mobile.MobileLocation;

import java.util.List;

/**
 * Created by cmmad_000 on 5/21/2015.
 */
public interface MobileService {
    void saveLocation(Long id, String lat, String lng, String name);

    void saveImage(Long id, String keyString, String servingUrl, String username);

    List<MobileImage> findAllImage();

    void commitChangePhoto(Long id);

    void discardChangePhoto(Long id);

    List<MobileLocation> findAllLocation();

    void discardChangeLocation(Long id);

    void commitChangeLocation(Long id);

    Integer countAwaitApprove();
}
