package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.domain.old.ProjectType;
import com.loxley.rdpmap.domain.old.embedded.EProjectState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cmmad_000 on 3/4/2015.
 */
public interface ProjectService {
    String translateSource(int i);

    List<String> findAllState();

    void saveProjects(List<Project> values);

    List<Project> findAll(int page, int limit);

    Project findOne(Long id);

    void update(Long id, Project target);

    int count();

    List<Project> search(String query, int page, int limit);

    void delete(Project project);

    long countSearch(String query);

    Long save(Project project);

    List<ProjectType> findAllType();

    ProjectType findOneType(Long aLong);

    void updateIndex();

    void stripIdea(Idea idea);

    List<Project> getFirstpageProjects();

    ArrayList<EProjectState> getProjectStates();

    void updateIndexPage(Integer page);

    void wipe();

    Boolean importLatLng(int i);

    void checkRight(String region, Project project);
}
