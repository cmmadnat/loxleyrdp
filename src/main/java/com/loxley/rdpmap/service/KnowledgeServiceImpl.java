package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.search.*;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.Key;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.Setting;
import com.loxley.rdpmap.domain.old.*;
import com.loxley.rdpmap.domain.old.embedded.EKnowledgeType;
import org.apache.commons.beanutils.BeanUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by thitiwat on 6/14/15.
 */
public class KnowledgeServiceImpl implements KnowledgeService, ResourceLoaderAware {
    private static final int UPDATE_LIMIT = 100;
    private static final String INDEX_NAME = "knowledgeIndex";
    private static final int FIRST_PAGE_COUNT = 3;
    public static final String PDF_HOST = "http://27.254.68.103:8081";
    private ResourceLoader loader;
    ObjectMapper mapper = new ObjectMapper();

    public static final String OLD_SITE_PREFIX = "http://km.rdpb.go.th/Content/uploads/images";
    private static final String NEW_SITE_PREFIX = "http://storage.googleapis.com/rdp-archive";

    Logger logger = LoggerFactory.getLogger(KnowledgeServiceImpl.class);

    boolean check = false;
    private Index index;
    @Autowired
    private SettingService settingService;
    @Autowired
    private RoyaltyService royaltyService;

    public void init() {
        IndexSpec indexSpec = IndexSpec.newBuilder().setName(INDEX_NAME).build();
        index = SearchServiceFactory.getSearchService().getIndex(indexSpec);


        if (ofy().load().type(KnowledgeType.class).count() == 0) {
            try {
                List<ProjectType> list =
                        mapper.readValue(loader.getResource("classpath:knowledgeType.json").getFile(),
                                new TypeReference<List<KnowledgeType>>() {
                                });
                ofy().save().entities(list);
            } catch (IOException e) {
                logger.error("can't import knowledge state json");
            }
        }
        if (ofy().load().type(Knowledge.class).count() == 0) {
            try {
                List<Knowledge> readValue = mapper.readValue(loader.getResource("classpath:knowledge.json").getFile(), new TypeReference<List<Knowledge>>() {
                });
                for (Knowledge knowledge : readValue) {
                    processKnowledge(knowledge);
                    createIndexKnowledge(knowledge);
                }
                ofy().save().entities(readValue);
            } catch (JsonMappingException e) {
                logger.error("can't import knowledge json");
            } catch (JsonParseException e) {
                logger.error("can't import knowledge json");
            } catch (IOException e) {
                logger.error("can't import knowledge json");
            }
        }
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.loader = resourceLoader;
    }

    @Override
    public List<KnowledgeType> findAllType() {
        if (!check) {
            check = true;
            init();
        }
        return ofy().load().type(KnowledgeType.class).list();

    }

    @Override
    public Long save(Knowledge knowledge) {
        final Key<Knowledge> now = ofy().save().entity(knowledge).now();
        createIndexKnowledge(knowledge);
        return now.getId();

    }

    @Override
    public List<Knowledge> findAll(int page, int limit) {

        if (!check) {
            check = true;
            init();
        }

        return ofy().load().type(Knowledge.class).limit(limit).order("-time").offset(page * limit).list();
    }

    @Override
    public KnowledgeType findOneType(Long aLong) {
        return ofy().load().type(KnowledgeType.class).id(aLong).now();
    }

    @Override
    public Knowledge findOne(Long id) {
        if (!check) {
            check = true;
            init();
        }

        final Knowledge knowledge = ofy().load().type(Knowledge.class).id(id).now();
        if (knowledge == null) return null;
        if (knowledge.getTypeID() != null && knowledge.getTypeID().getUuid() == null) {
            KnowledgeType oneType = findTypeByOldID(knowledge.getTypeID().getId());
            EKnowledgeType eKnowledgeType = new EKnowledgeType();
            try {
                BeanUtils.copyProperties(eKnowledgeType, oneType);
                knowledge.setTypeID(eKnowledgeType);
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
        }
        processKnowledge(knowledge);
        return knowledge;
    }

    @Override
    public void update(Long id, Knowledge knowledge) {
        knowledge.setUuid(id);
        knowledge.setTime(new Date());
        createIndexKnowledge(knowledge);
        ofy().save().entity(knowledge);
    }

    @Override
    public void saveKnowledges(List<Knowledge> values) {
        for (Knowledge value : values) {
            createIndexKnowledge(value);
        }
        ofy().save().entities(values);
    }

    @Override
    public void delete(Long id) {
        final Knowledge one = findOne(id);
        index.delete(id + "");
        if (one != null) ofy().delete().entity(one);
    }

    @Override
    public int count() {
        return ofy().load().type(Knowledge.class).count();
    }

    @Override
    public void updateIndex() {
        com.google.appengine.api.taskqueue.Queue defaultQueue = QueueFactory.getDefaultQueue();
        int count = ofy().load().type(Knowledge.class).count();
        int numberOfPage = count / UPDATE_LIMIT + 1;

        while (true) {
            List<String> docIds = new ArrayList<String>();
            // Return a set of doc_ids.
            GetRequest request = GetRequest.newBuilder().setReturningIdsOnly(true).build();
            GetResponse<Document> response = index.getRange(request);
            if (response.getResults().isEmpty()) {
                break;
            }
            for (Document doc : response) {
                docIds.add(doc.getId());
            }
            index.delete(docIds);
        }


        for (int i = 0; i < numberOfPage; i++) {
            defaultQueue.add(TaskOptions.Builder.withUrl("/import/index/km").param("page", i + ""));
        }
    }

    @Override
    public void updateIndexPage(Integer page) {
        if (!check) {
            check = true;
            init();
        }


        List<Knowledge> list = ofy().load().type(Knowledge.class).offset(UPDATE_LIMIT * page).limit(UPDATE_LIMIT).list();

        for (Knowledge knowledge : list) {

            if (knowledge.getDisplay() != null && !knowledge.getDisplay().isEmpty() && knowledge.getFiles().isEmpty()) {
                RoyalProjectFile file = new RoyalProjectFile();
                file.setFileName(PDF_HOST + "/knowledge/" + knowledge.getUuid() + "/pdf");
                knowledge.getFiles().add(file);
            }
            if (knowledge.getImages().size() == 0 && knowledge.getImgFront() != null) {
                RoyalProjectImage royalProjectImage = new RoyalProjectImage();
                royalProjectImage.setServeURL(knowledge.getImgFront());
                knowledge.getImages().add(royalProjectImage);
                update(knowledge.getUuid(), knowledge);
            }
            // convert from detail as well
            if (knowledge.getImages().size() == 1 && knowledge.getImages().get(0).getServeURL().equals(knowledge.getImgFront())) {
                org.jsoup.nodes.Document doc = Jsoup.parse(knowledge.getDisplay());
                Elements img = doc.select("img");

                HashSet<String> redundantCheck = new HashSet<String>();


                for (Element element : img) {
                    String src = element.attr("src").replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX);
                    if (exists(src) && !redundantCheck.contains(src)) {
                        RoyalProjectImage royalProjectImage = new RoyalProjectImage();
                        royalProjectImage.setServeURL(src);
                        knowledge.getImages().add(royalProjectImage);
                        redundantCheck.add(src);
                    }
                }
            }

            processKnowledge(knowledge);
            createIndexKnowledge(knowledge);
        }


        ofy().save().entities(list);
    }

    @Override
    public List<Knowledge> search(String query, int page, int limit) {
        if (!check) {
            check = true;
            init();
        }

//        .setSortOptions(
//                SortOptions.newBuilder()
//                        .addSortExpression(
//                                SortExpression.newBuilder().
//                                        setDirection(SortExpression.SortDirection.DESCENDING).
//                                        setExpression("year")))
        QueryOptions option = QueryOptions.newBuilder()
                .setLimit(limit)
                .setOffset(limit * page)
                .setSortOptions(
                        SortOptions.newBuilder().addSortExpression(
                                SortExpression.newBuilder().setDirection(SortExpression.SortDirection.DESCENDING)
                                        .setExpression("date")))
                .build();
        Results<ScoredDocument> list = index.search(Query.newBuilder().setOptions(option).build(query));
        List<Knowledge> output = new ArrayList<Knowledge>();

        for (ScoredDocument scoredDocument : list) {
            Knowledge one = findOne(Long.valueOf(scoredDocument.getId()));
            if (one != null) output.add(one);
        }

        return output;

    }

    @Override
    public long countSearch(String query) {
        QueryOptions request = QueryOptions.newBuilder()
                .setNumberFoundAccuracy(200)
                .build();
        Query q = Query.newBuilder().setOptions(request).build(query);
        long numberFound = index.search(q).getNumberFound();
        return numberFound;
    }

    @Override
    public List<Knowledge> getFirstPageKnowledges() {
        final ObjectMapper objectMapper = new ObjectMapper();

        Setting firstPageKnowledgeUsageCount = settingService.get("firstPageKnowledgeUsageCount");
        // count usage to 200 and reset the page index
        if (firstPageKnowledgeUsageCount == null || firstPageKnowledgeUsageCount.getValue().equals("200")) {
            settingService.set("firstPageKnowledgeUsageCount", "0");
            firstPageKnowledgeUsageCount = new Setting();
            firstPageKnowledgeUsageCount.setValue("0");
        }


        Setting firstPageKnowledges = settingService.get("firstPageKnowledges");
        if (firstPageKnowledges == null || firstPageKnowledgeUsageCount.getValue().equals("0")) {
            final ArrayList<Long> ids = new ArrayList<Long>();
            final List<Knowledge> list = ofy().load().type(Knowledge.class).limit(100).order("-time").list();
            int counter = 0;
            do {
                Knowledge item = list.get(counter++);
                if (item.getImages().size() != 0 || (item.getImgCover() != null && item.getImgCover().isEmpty()))
                    ids.add(item.getUuid());
            } while (ids.size() < FIRST_PAGE_COUNT && counter < list.size());
            final String s;
            try {
                s = objectMapper.writeValueAsString(ids);
                settingService.set("firstPageKnowledges", s);
                firstPageKnowledges = new Setting();
                firstPageKnowledges.setValue(s);
            } catch (JsonProcessingException e) {
                logger.error("serialize json in get first page");
            }
        }

        final int next = Integer.parseInt(firstPageKnowledgeUsageCount.getValue()) + 1;
        settingService.set("firstPageKnowledgeUsageCount", next + "");

        final String value = firstPageKnowledges.getValue();
        final ArrayList<String> o;
        try {
            o = objectMapper.readValue(value, new TypeReference<ArrayList<String>>() {
            });
            final ArrayList<Long> ids = new ArrayList<Long>();
            for (String s : o) {
                ids.add(Long.valueOf(s));
            }
            if (ids.size() != 0) {
                final Collection<Knowledge> values = ofy().load().type(Knowledge.class).ids(ids).values();

                if (values.size() != ids.size()) settingService.set("firstPageKnowledgeUsageCount", "0");

                return new ArrayList<Knowledge>(values);
            } else return findAll(0, FIRST_PAGE_COUNT);

        } catch (IOException e) {
            logger.error("error deserialize the json from setting");
        }

        return findAll(0, FIRST_PAGE_COUNT);


    }

    private void createIndexKnowledge(Knowledge knowledge) {
        if (!check) {
            check = true;
            init();
        }
        try {
            Document.Builder document = Document.newBuilder().setId(knowledge.getUuid() + "")
                    .addField(Field.newBuilder().setName("name").setText(knowledge.getName()))
                    .addField(Field.newBuilder().setName("detail").setText(knowledge.getDetail()))
                    .addField(Field.newBuilder().setName("type").setText(knowledge.getTypeID().getName()))
                    .addField(Field.newBuilder().setName("royalty").setNumber(knowledge.getRoyaltyID() != null ? knowledge.getRoyaltyID().getId() : 0))
                    .addField(Field.newBuilder().setName("date").setDate(knowledge.getTime() != null ? knowledge.getTime() : new Date()));
            index.put(document);
        } catch (Exception e) {
            e.printStackTrace();
//            logger.warn("not running in appengine");
        }
    }

    public void processKnowledge(Knowledge knowledge) {


        int oldID = knowledge.getRoyaltyID().getId();

        // there is a name change so the project must use royalty id to name the entity again
        Royalty byOldID = royaltyService.findByOldID(oldID);
        knowledge.getRoyaltyID().setUuid(byOldID.getUuid());
        knowledge.getRoyaltyID().setId(byOldID.getId());
        knowledge.getRoyaltyID().setName(byOldID.getName());

        // move the pic from old location to gcs
        if (knowledge.getImgFront() != null && knowledge.getImgFront().startsWith(OLD_SITE_PREFIX)) {
            knowledge.setImgFront(knowledge.getImgFront().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX));
        }
        if (knowledge.getImgCover() != null && knowledge.getImgCover().startsWith(OLD_SITE_PREFIX)) {
            knowledge.setImgCover(knowledge.getImgCover().replace(OLD_SITE_PREFIX, NEW_SITE_PREFIX));
        }
    }

    public KnowledgeType findTypeByOldID(Integer id) {
        return ofy().load().type(KnowledgeType.class).filter("id", id).first().now();
    }


    public static boolean exists(String URLName) {
        try {
//            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
