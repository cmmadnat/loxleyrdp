package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.old.Department;

import java.util.List;

/**
 * Created by cmmad_000 on 3/28/2015.
 */
public interface DepartmentService {
    List<Department> search(String query);

    int count();

    Department findOne(Long aLong);
}
