package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.old.Royalty;
import com.loxley.rdpmap.domain.old.embedded.ERoyalty;

import java.util.List;

/**
 * Created by cmmad_000 on 3/22/2015.
 */
public interface RoyaltyService {
    int count();

    List<Royalty> findAll();

    Royalty findOne(Long aLong);

    Royalty findByOldID(int i);
}
