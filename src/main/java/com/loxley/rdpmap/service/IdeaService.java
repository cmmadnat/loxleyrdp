package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Project;

import java.util.List;

/**
 * Created by cmmad_000 on 4/9/2015.
 */
public interface IdeaService {
    Integer count();

    void saveIdeas(List<Idea> values);

    List<Idea> findAll(int page, int limit);

    long countSearch(String query);

    List<Idea> search(String query, int page, int limit);

    Idea findOne(Long aLong);

    void wipe();

    void delete(Long id);

    void updateIndex();

    void updateIndexPage(int page);

    List<Project> findRelatedProject(Idea target);

    void update(Long id, Idea one);

    Long save(Idea project);

    List<Idea> getFirstpageIdeas();
}
