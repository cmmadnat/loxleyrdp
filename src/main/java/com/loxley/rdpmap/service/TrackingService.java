package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.ProjectTracker;

import java.util.List;

/**
 * Created by cmmad_000 on 5/19/2015.
 */
public interface TrackingService {
    List<ProjectTracker> findForProject(Long id);

    long postStatus(Long id, ProjectTracker projectTracker, String name);

    void delete(Long id);

    ProjectTracker findOne(Long trackingID);

    void update(ProjectTracker one);

}
