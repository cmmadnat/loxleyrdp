package com.loxley.rdpmap.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loxley.rdpmap.domain.old.Royalty;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 3/22/2015.
 */
public class RoyaltyServiceImpl implements RoyaltyService, ResourceLoaderAware {
    Boolean check = false;
    private ResourceLoader loader;

    ObjectMapper mapper = new ObjectMapper();
    org.slf4j.Logger logger = LoggerFactory.getLogger(RoyaltyServiceImpl.class);

    public void init() {
        if (count() == 0) {
            File file = null;
            try {
                file = loader.getResource("classpath:royalty.json").getFile();
                List<Royalty> royalties = mapper.readValue(file, new TypeReference<List<Royalty>>() {
                });
                ofy().save().entities(royalties);
            } catch (IOException e) {
                logger.error("can't load the royalty file");
            }

        }
    }

    @Override
    public int count() {

        return ofy().load().type(Royalty.class).count();
    }

    @Override
    public List<Royalty> findAll() {
        if (!check) {
            check = true;
            init();
        }
        return ofy().load().type(Royalty.class).order("priority").list();
    }

    @Override
    public Royalty findOne(Long aLong) {
        return ofy().load().type(Royalty.class).id(aLong).now();
    }

    @Override
    public Royalty findByOldID(int i) {
        return ofy().load().type(Royalty.class).filter("id", i).first().now();
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.loader = resourceLoader;
    }
}
