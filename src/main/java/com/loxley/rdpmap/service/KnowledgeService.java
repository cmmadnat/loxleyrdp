package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Knowledge;
import com.loxley.rdpmap.domain.old.KnowledgeType;

import java.util.List;

/**
 * Created by thitiwat on 6/14/15.
 */
public interface KnowledgeService {
    List<KnowledgeType> findAllType();

    Long save(Knowledge knowledge);

    List<Knowledge> findAll(int page, int limit);

    KnowledgeType findOneType(Long aLong);

    Knowledge findOne(Long id);

    void update(Long id, Knowledge idea);

    void saveKnowledges(List<Knowledge> values);

    void delete(Long id);

    int count();

    void updateIndex();

    void updateIndexPage(Integer page);

    List<Knowledge> search(String query, int page, int limit);

    long countSearch(String query);

    List<Knowledge> getFirstPageKnowledges();
}
