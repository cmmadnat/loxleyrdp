package com.loxley.rdpmap.service;

import com.loxley.rdpmap.domain.LogEntry;
import com.loxley.rdpmap.domain.ProjectTracker;
import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Knowledge;
import com.loxley.rdpmap.domain.old.Project;
import org.joda.time.DateTime;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 7/16/2015.
 */
public class LogServiceImpl implements LogService {

    public static final String LOG_UPDATE_IDEA = "ปรับปรุงข้อมูล พระราชดำริ ";
    public static final String LOG_DELETE_IDEA = "ลบข้อมูล พระราชดำริ ";
    public static final String LOG_CREATE_IDEA = "บันทึกข้อมูล พระราชดำริ ";
    public static final String LOG_SEARCH_IDEA = "ค้นหาข้อมูล พระราชดำริ ";

    public static final String LOG_UPDATE_PROJECT = "ปรับปรุงข้อมูล โครงการฯ ";
    public static final String LOG_DELETE_PROJECT = "ลบข้อมูล โครงการฯ ";
    public static final String LOG_CREATE_PROJECT = "บันทึกข้อมูล โครงการฯ ";
    public static final String LOG_SEARCH_PROJECT = "ค้นหาข้อมูล โครงการฯ ";

    public static final String LOG_UPDATE_KNOWLEDGE = "ปรับปรุงข้อมูล แนวคิดฯ ";
    public static final String LOG_DELETE_KNOWLEDGE = "ลบข้อมูล แนวคิดฯ ";
    public static final String LOG_CREATE_KNOWLEDGE = "บันทึกข้อมูล แนวคิดฯ ";
    public static final String LOG_SEARCH_KNOWLEDGE = "ค้นหาข้อมูล แนวคิดฯ ";


    public static final String LOG_UPDATE_PROJECT_TRACKING = "ปรับปรุงข้อมูล ติดตามโครงการฯ ";
    public static final String LOG_DELETE_PROJECT_TRACKING = "ลบข้อมูล ติดตามโครงการฯ ";
    public static final String LOG_CREATE_PROJECT_TRACKING = "บันทึกข้อมูล ติดตามโครงการฯ ";
    public static final String LOG_SEARCH_PROJECT_TRACKING = "ค้นหาข้อมูล ติดตามโครงการฯ ";

    @Override
    public void login(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setUsername(principal.getName());
        logEntry.setAction("Login");
        logEntry.setUrl(null);
        ofy().save().entity(logEntry);
    }

    @Override
    public void logout(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setUsername(principal.getName());
        logEntry.setAction("Logout");
        logEntry.setUrl(null);
        ofy().save().entity(logEntry);
    }

    @Override
    public void approve(Principal principal) {

    }


    @Override
    public List<LogEntry> findByDate(Date date) {
        DateTime after = new DateTime(date);
        DateTime before = new DateTime(date);
        after = after.withHourOfDay(0).withMinuteOfHour(0);
        before = before.withHourOfDay(0).withMinuteOfHour(0).plusDays(1);


        List<LogEntry> list = ofy().load().type(LogEntry.class).filter("date > ", after.toDate()).filter("date < ", before.toDate()).order("date").list();
        return list;
    }

    @Override
    public void searchProject(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_SEARCH_PROJECT);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void updateProject(Project project, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_UPDATE_PROJECT + project.getName());
        logEntry.setUrl("/admin#/admin/royalproject/" + project.getUuid());
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void createProject(Project project, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_CREATE_PROJECT + project.getName());
        logEntry.setUrl("/admin#/admin/royalproject/" + project.getUuid());
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void deleteProject(Project project, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_DELETE_PROJECT + project.getName());
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    //    idea
    @Override
    public void searchIdea(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_SEARCH_IDEA);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void createIdea(Idea idea, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_CREATE_IDEA + idea.getAddress());
        logEntry.setUrl("/admin#/admin/idea/" + idea.getUuid());
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void updateIdea(Idea idea, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_UPDATE_IDEA + idea.getAddress());
        logEntry.setUrl("/admin#/admin/idea/" + idea.getUuid());
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void deleteIdea(Idea idea, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_DELETE_IDEA + idea.getAddress());
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void searchKnowledge(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_SEARCH_KNOWLEDGE);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void createKnowledge(Knowledge knowledge, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_CREATE_KNOWLEDGE + knowledge.getName());
        logEntry.setUrl("/admin#/admin/km/" + knowledge.getUuid());
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void updateKnowledge(Knowledge knowledge, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_UPDATE_KNOWLEDGE + knowledge.getName());
        logEntry.setUrl("/admin#/admin/km/" + knowledge.getUuid());
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void deleteKnowledge(Knowledge knowledge, Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_DELETE_KNOWLEDGE + knowledge.getName());
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void searchTracking(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_SEARCH_PROJECT_TRACKING);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void createTracking(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_CREATE_PROJECT_TRACKING);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void updateTracking(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_UPDATE_PROJECT_TRACKING);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void deleteTracking(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction(LOG_DELETE_PROJECT_TRACKING);
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }

    @Override
    public void dashboard(Principal principal) {
        LogEntry logEntry = new LogEntry();
        logEntry.setAction("Dashboard");
        logEntry.setUrl(null);
        logEntry.setUsername(principal.getName());
        ofy().save().entity(logEntry);
    }
}
