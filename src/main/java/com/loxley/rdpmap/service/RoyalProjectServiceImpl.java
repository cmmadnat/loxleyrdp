package com.loxley.rdpmap.service;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.loxley.rdpmap.domain.RoyalProject;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.domain.old.Royalty;
import com.loxley.rdpmap.domain.old.embedded.EDepartment;
import com.loxley.rdpmap.domain.old.embedded.ERegion;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class RoyalProjectServiceImpl implements RoyalProjectService {

    @Autowired
    LocationService locationService;
    @Autowired
    ProjectService projectService;
    @Autowired
    RoyaltyService royaltyService;

    private BlobstoreService blobStoreService;
    private List<Integer> deekaIDs = new ArrayList<Integer>();

    @PostConstruct
    public void init() {

        for (int i = 8; i < 13; i++) {
            this.deekaIDs.add(i);
        }
        this.blobStoreService = BlobstoreServiceFactory.getBlobstoreService();
    }

    @Override
    public List<RoyalProject> findAll(int i, int pageLimit) {
        List<Project> projects = ofy().load().type(Project.class).offset(pageLimit * i)
                .limit(pageLimit).list();
        ArrayList<RoyalProject> royalProjects = new ArrayList<RoyalProject>();

        for (Project project : projects) {
            royalProjects.add(convert(project));
        }
        return royalProjects;

    }

    private RoyalProject convert(Project project) {

        if (deekaIDs.contains(project.getRoyaltyID().getId())) {
            project.setSource(2);
            int oldID = project.getRoyaltyID().getId() - 7;

            Royalty byOldID = royaltyService.findByOldID(oldID);
            project.getRoyaltyID().setUuid(byOldID.getUuid());
            project.getRoyaltyID().setId(byOldID.getId());
            project.getRoyaltyID().setName(byOldID.getName());
        }

        RoyalProject target = new RoyalProject();

        List<EDepartment> departments = project.getDepartments();
        for (EDepartment department : departments) {
            target.getDepartments().add(department.getName());
        }
        target.setCreateDate(project.getStartDate());

        target.setName(project.getName());
        target.setId(project.getUuid());
        target.setLocation(project.getLocation());
        target.setImages(project.getImages());
        target.setRoyal(project.getRoyaltyID().getName());
        target.setCategory("โครงการอันเนื่องมาจากพระราชดำริ");
        target.setActivityCategory(project.getTypeID().getName());
        target.setProvince(project.getProvinceID().getName());
        target.setDistrict((project.getDistrictID() != null) ? project.getDistrictID().getName() : "");
        target.setSubDistrict((project.getAmphurID() != null) ? project.getAmphurID().getName() : "");
        target.setUrl("");
        target.setPublish(true);
        target.setImages(project.getImages());
        target.setDetail(project.getDetail());
        ERegion regionID = locationService.findOneProvinceByOldID(project.getProvinceID().getId()).getRegionID();
        target.setRegion(regionID.getName());

        target.setAreaRainy(project.getAreaRainy());
        target.setAreaSummer(project.getAreaSummer());
        target.setHousehold(project.getHousehold());
        target.setVillage(project.getVillage());
        target.setCitizen(project.getCitizen());

        target.setYear(Integer.valueOf(project.getYear()));
        target.setIsMainProject(project.getMainProject() == null);

        target.setFiles((ArrayList<RoyalProjectFile>) project.getFiles());
        target.setResult(project.getResult());

        if (project.getDepartments().size() != 0)
            target.setMinistry(project.getDepartments().get(0).getMinistryID().getName());

        if (target.getImages().size() == 0 && project.getImgFront() != null && !project.getImgFront().isEmpty()) {
            RoyalProjectImage image = new RoyalProjectImage();
            image.setServeURL(project.getImgFront());
            target.getImages().add(image);
        }


        target.setVillageName(project.getVillageName());
        target.setAddress(project.getAddress());

//        String status;
//        String source;
        if (project.getStateID() == null) {
            target.setStatus("ยังไม่ระบุพิกัดที่ตั้งโครงการ");
        } else
            target.setStatus(project.getStateID().getName());
        target.setSource(projectService.translateSource(project.getSource()));

        if (project.getTypeID() != null)
            target.setIcon("http://projects.rdpb.go.th/img/iconforrdpb/png/icon0" + project.getTypeID().getId() + ".png");

        return target;
    }

    @Override
    public int pageCount(int pageLimit) {
        int count = ofy().load().type(RoyalProject.class).count();
        count = count / pageLimit;
        if (count == 0)
            count++;
        return count;
    }


    @Override
    public RoyalProject findOne(Long id) {
        return convert(ofy().load().type(Project.class).id(id).now());
    }


    @Override
    public void delete(Long id) {
        RoyalProject oldOne = findOne(id);
        if (oldOne != null) {
            List<RoyalProjectImage> images = oldOne.getImages();
            for (RoyalProjectImage royalProjectImage : images) {
                blobStoreService
                        .delete(new BlobKey(royalProjectImage.getKey()));
            }
            ofy().delete().entity(oldOne);
        }
    }

    @Override
    public List<RoyalProject> convert(List<Project> search) {
        List<RoyalProject> result = new ArrayList<RoyalProject>();
        for (Project project : search) {
            result.add(convert(project));
        }
        return result;
    }


}
