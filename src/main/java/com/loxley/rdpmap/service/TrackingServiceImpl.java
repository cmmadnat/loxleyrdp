package com.loxley.rdpmap.service;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.common.base.Splitter;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Result;
import com.loxley.rdpmap.domain.ProjectTracker;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.util.LinkExtractor;
import org.opengraph.MetaElement;
import org.opengraph.OpenGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by cmmad_000 on 5/19/2015.
 */
public class TrackingServiceImpl implements TrackingService {
    Logger logger = LoggerFactory.getLogger(TrackingServiceImpl.class);

    @Override
    public List<ProjectTracker> findForProject(Long id) {
        Project project = new Project();
        project.setUuid(id);
        return ofy().load().type(ProjectTracker.class).filter("project", Ref.create(project)).order("-listDate").list();
    }

    @Override
    public long postStatus(Long id, ProjectTracker projectTracker, String name) {
        Project project = new Project();
        project.setUuid(id);
        projectTracker.setProject(project);

        projectTracker.setAuthor(name);

        parseLink(projectTracker);


        Result<Key<ProjectTracker>> entity = ofy().save().entity(projectTracker);
        return entity.now().getId();
    }

    private void parseLink(ProjectTracker projectTracker) {
        List<String> strings = LinkExtractor.extractUrls(projectTracker.getStatus());
        if (strings.size() != 0) projectTracker.setLink(strings.get(0));

        if (!strings.isEmpty()) {
            OpenGraph site =
                    null;
            try {
                site = new OpenGraph(strings.get(0), true);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
            MetaElement[] properties = site.getProperties();
            for (MetaElement property : properties) {
//                System.out.println(property.getProperty() + " " + property.getContent());

            }
            MetaElement[] titles = site.getProperties("title");
            MetaElement[] images = site.getProperties("image");
            MetaElement[] descriptions = site.getProperties("description");
            if (images != null && images.length != 0) {
                String content = images[0].getContent();
                if (!content.startsWith("http")) content = "http:" + content;
                projectTracker.setImageURL(content);
            }
            if (titles != null && titles.length != 0) projectTracker.setTitle(titles[0].getContent());
            if (descriptions != null && descriptions.length != 0)
                projectTracker.setDescription(descriptions[0].getContent());
//            if (descriptions != null && descriptions.length != 0) target.setDescription(descriptions[0].getContent());
        }
        if (!strings.isEmpty()) {
            String url = strings.get(0);
            URL aURL = null;
            try {
                aURL = new URL(url);
                if (aURL.getHost().toLowerCase().endsWith("youtube.com")) {

                    String query = url.split("\\?")[1];
                    final Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(query);
                    String v = map.get("v");
                    projectTracker.setYoutube(v);

                }
            } catch (MalformedURLException e) {
            }
        }
    }

    @Override
    public void delete(Long id) {
        ProjectTracker projectTracker = ofy().load().type(ProjectTracker.class).id(id).now();

        if (projectTracker == null) return;

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        ArrayList<RoyalProjectImage> images = projectTracker.getImages();
        for (RoyalProjectImage image : images) {
            try {
                BlobKey blobKey = new BlobKey(image.getKey());
                imagesService.deleteServingUrl(blobKey);
                blobstoreService.delete(blobKey);
            } catch (Exception e) {

            }
        }

        ArrayList<RoyalProjectFile> files = projectTracker.getFiles();
        for (RoyalProjectFile file : files) {
            BlobKey blobKey = new BlobKey(file.getKey());
            try {
                blobstoreService.delete(blobKey);
            } catch (Exception e) {

            }
        }


        ofy().delete().entity(projectTracker).now();
    }

    @Override
    public ProjectTracker findOne(Long trackingID) {
        return ofy().load().type(ProjectTracker.class).id(trackingID).now();
    }

    @Override
    public void update(ProjectTracker one) {
        parseLink(one);

        ofy().save().entity(one);
    }

}
