package com.loxley.rdpmap.ui;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by cmmad_000 on 3/14/2015.
 */
public class Slider {
    int order = 0;
    String line1, line2, line3;
    String key;
    String imgURL;
    String sub1, sub2;

    public Slider() {
        line1 = line2 = line3 = imgURL = sub1 = sub2 = "";
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonCreator
    public Slider(@JsonProperty("line1") String line1, @JsonProperty("line2") String line2, @JsonProperty("line3") String line3,
                  @JsonProperty("sub1") String sub1, @JsonProperty("sub2") String sub2,
                  @JsonProperty("imgURL") String imgURL, @JsonProperty("order") int order) {
        this.line1 = line1;
        this.line2 = line2;
        this.line3 = line3;
        this.imgURL = imgURL;
        this.sub1 = sub1;
        this.sub2 = sub2;
        this.order = order;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getSub1() {
        return sub1;
    }

    public void setSub1(String sub1) {
        this.sub1 = sub1;
    }

    public String getSub2() {
        return sub2;
    }

    public void setSub2(String sub2) {
        this.sub2 = sub2;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
