package com.loxley.rdpmap.ui;

import com.loxley.rdpmap.domain.old.embedded.EProvince;
import com.loxley.rdpmap.domain.old.embedded.ERoyalty;

/**
 * Created by cmmad_000 on 7/23/2015.
 */
public class SearchResult {
    String url, name, detail, imageURL;
    EProvince province;
    ERoyalty royalty;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public EProvince getProvince() {
        return province;
    }

    public void setProvince(EProvince province) {
        this.province = province;
    }

    public ERoyalty getRoyalty() {
        return royalty;
    }

    public void setRoyalty(ERoyalty royalty) {
        this.royalty = royalty;
    }
}
