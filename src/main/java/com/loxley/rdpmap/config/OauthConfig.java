package com.loxley.rdpmap.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

import java.util.Arrays;

/**
 * Created by thitiwat on 4/19/15.
 */
@Configuration
@EnableOAuth2Client
public class OauthConfig {

    @Autowired
    private OAuth2ClientContext oauth2Context;

    String host = "http://sso.rdp-test.appspot.com";

    private String accessTokenUri = host + "/oauth/token";

    private String userAuthorizationUri = host + "/oauth/authorize";


    public static final String CLIENT_ID = "my-trusted-client";
    public static final String CLIENT_SECRET = "secret";


    @Configuration
    @EnableResourceServer
    protected static class ResourceServerConfig extends ResourceServerConfigurerAdapter {


        // Mehotds
        @Override
        public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
            resources.resourceId("oauth2-resource");
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            // @formatter:off
            http.headers().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
                    .authorizeRequests()
                    .antMatchers("/mobile/upload/**").authenticated().and()
                    .authorizeRequests()
                    .antMatchers("/mobile/tracking/upload/**").authenticated().and()
                    .authorizeRequests()
                    .antMatchers("/admin/**").authenticated().and()
                    .formLogin().loginPage("/login").and()
                    .logout().logoutUrl("/logoutReal").logoutSuccessUrl("http://sso.rdp-test.appspot.com/logout").and()
                    .csrf().disable();
            ;

            // @formatter:on
        }
    }

    //    @Configuration
//    protected static class ResourceConfiguration {
//    public static String host = "http://localhost:8080";


    @Bean
    public OAuth2ProtectedResourceDetails trusted() {
        AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
        details.setId("sparklr/trusted");
        details.setClientId(CLIENT_ID);
        details.setAccessTokenUri(accessTokenUri);
        details.setUserAuthorizationUri(userAuthorizationUri);
        details.setScope(Arrays.asList("read"));

        return details;
    }


    @Bean
    public OAuth2RestTemplate trustedClientRestTemplate() {
        return new OAuth2RestTemplate(trusted(), oauth2Context);
    }
//    }
}
