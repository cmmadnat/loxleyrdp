package com.loxley.rdpmap.web;

import com.google.appengine.repackaged.org.joda.time.format.DateTimeFormatter;
import com.loxley.rdpmap.domain.LogEntry;
import com.loxley.rdpmap.service.LogService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * Created by cmmad_000 on 7/16/2015.
 */
@Controller
@RequestMapping("/admin/log")
public class LogController {
    @Autowired
    LogService logService;

    @RequestMapping(headers = "Accept=application/json")
    @ResponseBody
    public List<LogEntry> findLog(@RequestParam(value = "date", required = false) String dateString) {
        Date date;
        if (dateString == null) date = new Date();
        else {
            DateTime parse = DateTime.parse(dateString, DateTimeFormat.forPattern("dd-MM-yyyy"));
            parse = parse.minusYears(543);
            date = parse.toDate();
        }
        return logService.findByDate(date);
    }

    @RequestMapping
    public String index() {
        return "log";
    }


}
