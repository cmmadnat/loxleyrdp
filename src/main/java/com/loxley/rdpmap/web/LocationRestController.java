package com.loxley.rdpmap.web;

import com.loxley.rdpmap.domain.old.Amphur;
import com.loxley.rdpmap.domain.old.District;
import com.loxley.rdpmap.domain.old.Province;
import com.loxley.rdpmap.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by cmmad_000 on 3/27/2015.
 */
@RestController
@RequestMapping("/admin/location")
public class LocationRestController {
    @Autowired
    LocationService locationService;

    @RequestMapping("reset")
    public String reset() {
        locationService.reset();
        return "success";
    }

    @RequestMapping("{id}/province")
    public List<Province> getProvinceByRegion(@PathVariable("id") Long regionID) {
        return locationService.findProvinceByRegion(regionID);

    }

    @RequestMapping("{id}/amphur")
    public List<Amphur> getDistrictByProvince(@PathVariable("id") Long provinceID) {
        return locationService.findAmphurByProvince(provinceID);

    }

    @RequestMapping("{id}/tumbon")
    public List<District> getSubDistrictByDistrict(@PathVariable("id") Long amphurID) {
        return locationService.findTumbonByAmphur(amphurID);
    }
}
