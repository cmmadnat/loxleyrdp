package com.loxley.rdpmap.web;

import com.cmmadnat.sso.domain.AppUser;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loxley.rdpmap.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by thitiwat on 4/19/15.
 */
@Controller
public class LoginLogoutController {
    @Autowired
    OAuth2RestTemplate trustedClientRestTemplate;
    private String host = "http://sso.rdp-test.appspot.com";

    @Autowired
    LogService logService;

    @RequestMapping("/logout")
    public String logout(Principal principal) {
        logService.logout(principal);
        return "redirect:/logoutReal";
    }

    @RequestMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        String redirectUrl = getRedirectUrl(request, response);
        try {
            String name = trustedClientRestTemplate.getForObject(new URI(host + "/api/me"), String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            AppUser appUser = objectMapper.readValue(name, AppUser.class);

//            0 = super admin, 1 = admin, 2 = supervisor, 3 = user
            // super and admin can do everyting
            // supervisor can grant the change request
            // user can't grant request

            Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

            if (appUser.getRole().getIdentifier() <= 1) {
                authorities.add(new SimpleGrantedAuthority("ROLE_MANAGE_USER"));
            }
            // supervisor has identifier between 20 - 30
            if (appUser.getRole().getIdentifier() < 30) {
                authorities.add(new SimpleGrantedAuthority("ROLE_APPROVE"));
                authorities.add(new SimpleGrantedAuthority("ROLE_IDEA_KM"));
            }
            // get region from identifier
            final int regionCode = appUser.getRole().getIdentifier() % 10;
            authorities.add(new SimpleGrantedAuthority("ROLE_REGION_" + regionCode));

            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(appUser.getUsername(), null, authorities);
            SecurityContextHolder.getContext().setAuthentication(auth);

            // log
            logService.login(auth);
        } catch (RestClientException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:" + redirectUrl;
    }

    protected String getRedirectUrl(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            SavedRequest savedRequest =
                    new HttpSessionRequestCache().getRequest(request, response);
            if (savedRequest != null) {
                return savedRequest.getRedirectUrl();
            }
        }

    /* return a sane default in case data isn't there */
        return request.getContextPath() + "/";
    }

}
