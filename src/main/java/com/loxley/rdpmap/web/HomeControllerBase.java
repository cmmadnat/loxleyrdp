package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Knowledge;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.service.ProjectService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * Created by cmmad_000 on 2/29/2016.
 */
public class HomeControllerBase {

    @Autowired
    ProjectService projectService;

    protected void processLogoAndDate(Project target) {
        List<RoyalProjectFile> files = target.getFiles();
        for (RoyalProjectFile file : files) {
            if (file.getFileName().endsWith("pdf"))
                file.setLogo("fa fa-file-pdf-o");
            else if (file.getFileName().toLowerCase().endsWith("doc") || file.getFileName().toLowerCase().endsWith("docx"))
                file.setLogo("fa fa-file-word-o");
            else if (file.getFileName().toLowerCase().endsWith("xls") || file.getFileName().toLowerCase().endsWith("xlsx"))
                file.setLogo("fa fa-file-excel-o");
            else if (file.getFileName().toLowerCase().endsWith("zip") || file.getFileName().toLowerCase().endsWith("rar"))
                file.setLogo("fa fa-file-zip-o");
            else if (file.getFileName().toLowerCase().endsWith("jpg") || file.getFileName().toLowerCase().endsWith("png") || file.getFileName().toLowerCase().endsWith("gif"))
                file.setLogo("fa fa-file-photo-o");
            else file.setLogo("fa fa-file-o");
            file.setLogo(file.getLogo() + " ");
        }

        DateTime startDate = new DateTime(target.getStartDate());
        DateTime finishDate = new DateTime(target.getFinishDate());

        if (target.getStartDate() != null && startDate.getYear() < 2400) {
            startDate = startDate.plusYears(543);
            target.setStartDate(startDate.toDate());
//            target.getStartDate().setYear(target.getStartDate().getYear() + 543);
        }
        if (target.getFinishDate() != null && finishDate.getYear() < 2400) {
            finishDate = finishDate.plusYears(543);
            target.setFinishDate(finishDate.toDate());
//            target.getFinishDate().setYear(target.getFinishDate().getYear() + 543);
        }

    }

    protected void processLogoAndURL(List<Project> projects) {
        for (Project project : projects) {
            try {
                project.setUrl("/projects/" + project.getUuid() + "/" + URLEncoder.encode(project.getName().replace("/", "-"), "UTF-8"));
                processProject(project);
            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
            }
            Integer id = project.getTypeID().getId();
            if (id == null) id = 1;
            switch (id) {
                case 1:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon01.gif");
                    break;
                case 2:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon02.gif");
                    break;
                case 3:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon03.gif");
                    break;
                case 4:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon04.gif");
                    break;
                case 5:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon05.gif");
                    break;
                case 6:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon06.gif");
                    break;
                case 7:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon07.gif");
                    break;
                case 8:
                    project.setProjectTypeLogo("/img/iconforrdpb/icon08.gif");
                    break;

            }
//            idea.setProjectTypeLogo();
        }
    }

    protected void processProject(Project project) {
        if (project.getImages().isEmpty()) {
            RoyalProjectImage royalProjectImage = new RoyalProjectImage();
            royalProjectImage.setServeURL(project.getImgFront());
            project.getImages().add(royalProjectImage);
        } else {
            for (RoyalProjectImage royalProjectImage : project.getImages()) {
                royalProjectImage.setServeURL(royalProjectImage.getServeURL());
            }
        }
    }

    protected void processIdea(Idea idea) {
        if (idea.getImages().isEmpty()) {
            RoyalProjectImage royalProjectImage = new RoyalProjectImage();
            royalProjectImage.setServeURL(idea.getImgFront());
            idea.getImages().add(royalProjectImage);
        } else {
            for (RoyalProjectImage royalProjectImage : idea.getImages()) {
                royalProjectImage.setServeURL(royalProjectImage.getServeURL());
            }
        }
    }

    protected void processKnowledge(Knowledge knowledge) {
        if (knowledge.getImages().isEmpty()) {
            RoyalProjectImage royalProjectImage = new RoyalProjectImage();
            royalProjectImage.setServeURL(knowledge.getImgFront());
            knowledge.getImages().add(royalProjectImage);
        } else {
            for (RoyalProjectImage royalProjectImage : knowledge.getImages()) {
                royalProjectImage.setServeURL(royalProjectImage.getServeURL());
            }
        }
    }

    protected void processLogo(List<RoyalProjectFile> files) {

        for (RoyalProjectFile file : files) {
            if (file.getFileName().endsWith("pdf"))
                file.setLogo("fa fa-file-pdf-o");
            else if (file.getFileName().toLowerCase().endsWith("doc") || file.getFileName().toLowerCase().endsWith("docx"))
                file.setLogo("fa fa-file-word-o");
            else if (file.getFileName().toLowerCase().endsWith("xls") || file.getFileName().toLowerCase().endsWith("xlsx"))
                file.setLogo("fa fa-file-excel-o");
            else if (file.getFileName().toLowerCase().endsWith("zip") || file.getFileName().toLowerCase().endsWith("rar"))
                file.setLogo("fa fa-file-zip-o");
            else if (file.getFileName().toLowerCase().endsWith("jpg") || file.getFileName().toLowerCase().endsWith("png") || file.getFileName().toLowerCase().endsWith("gif"))
                file.setLogo("fa fa-file-photo-o");
            else file.setLogo("fa fa-file-o");
            file.setLogo(file.getLogo() + " ");
        }
    }

    protected void processProjectWithFile(@PathVariable("id") Long id, @Validated Project project, HttpServletRequest request, Project one, String fileName) {
        BlobstoreService service = BlobstoreServiceFactory.getBlobstoreService();
        Map<String, List<BlobKey>> uploads = service.getUploads(request);
        Map<String, List<BlobInfo>> blobInfos = service.getBlobInfos(request);

        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        int counter = 0;

        List<BlobKey> file = uploads.get("file");
        if (file != null && !file.isEmpty() && fileName != null)
            for (BlobKey blobKey : file) {
                if (blobInfos.get("file").get(counter).getSize() == 0) continue;
                RoyalProjectFile e = new RoyalProjectFile();
                e.setKey(blobKey.getKeyString());
                e.setFileName(fileName);
                one.getFiles().add(e);
            }
        counter = 0;
        List<BlobKey> images = uploads.get("image");
        if (images != null && !images.isEmpty())
            for (BlobKey blobKey : images) {
                try {
                    if (blobInfos.get("image").get(counter++).getSize() == 0) continue;
                    RoyalProjectImage e = new RoyalProjectImage();
                    e.setKey(blobKey.getKeyString());
                    ServingUrlOptions option = ServingUrlOptions.Builder.withBlobKey(blobKey);
                    e.setServeURL(imagesService.getServingUrl(option));
                    one.getImages().add(e);
                } catch (IllegalArgumentException e) {

                }
            }

//        project.setLocation(one.getLocation());
//        project.setFiles(one.getFiles());
//        project.setDisplay(one.getDisplay());
//        project.setImages(one.getImages());
//        projectService.update(id, project);
        processProject(id, project, request, one);
    }

    protected void processProject(@PathVariable("id") Long id, @Validated Project project, HttpServletRequest request, Project one) {

        project.setLocation(one.getLocation());
        project.setFiles(one.getFiles());
        project.setDisplay(one.getDisplay());
        project.setImages(one.getImages());
        project.setIdeas(one.getIdeas());
        project.setId(one.getId());
        projectService.update(id, project);
    }
}
