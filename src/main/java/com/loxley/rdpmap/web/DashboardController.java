package com.loxley.rdpmap.web;

import com.loxley.rdpmap.service.LogService;
import com.loxley.rdpreport.model.report.*;
import com.loxley.rdpreport.service.IdeaReportService;
import com.loxley.rdpreport.service.KnowledgeReportService;
import com.loxley.rdpreport.service.ProjectReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("admin/dashboard")
public class DashboardController {
    @Autowired
    IdeaReportService ideaReportService;
    @Autowired
    ProjectReportService projectReportService;
    @Autowired
    KnowledgeReportService knowledgeReportService;

    @Autowired
    LogService logService;
    private List<ProjectRoyaltyReport> projectRoyaltyReports;
    private ArrayList<SumIdeaReport> sumIdeaReports;
    private List<ProjectTypeReport> projectTypeReports;
    private List<ProjectRegionReport> projectRegionReports;
    private ProjectRegionReport first;
    private List<ProjectStatusReport> projectStatusReports;
    private List<SumKnowledgeReport> sumKnowledgeReports;
    private ArrayList<String> ideas;
    private ArrayList<String> ideaStat;
    private ArrayList<String> regions;
    private ArrayList<String> regionStat;
    private ArrayList<String> types;
    private ArrayList<String> typeStat;
    private ArrayList<String> royaltys;
    private ArrayList<String> royaltyStat;
    private ArrayList<String> knowledges;
    private ArrayList<String> knowledgeStat;
    private ArrayList<String> status;
    private ArrayList<String> statusStat;
    private SumIdeaReport totalSumIdeaReport;
    private ProjectRoyaltyReport totalProjectRoyaltyReport;
    private ProjectTypeReport totalProjectTypeReport;
    private ProjectRegionReport totalProjectRegionReport;
    private ProjectStatusReport totalProjectStatusReport;
    private SumKnowledgeReport totalSumKnowledgeReport;

    int refreshCount = 200;
    private List<ProjectStatusReportRegion> projectStatusReportRegions;
    private ProjectStatusReportRegion totalProjectStatusReportRegions;



    @ModelAttribute
    private void getModelAttribute(Model model) {
        if (sumIdeaReports == null || refreshCount-- < 0) {
            refreshCount = 200;
            projectRoyaltyReports = projectReportService.reportByRoyalty();
            projectTypeReports = projectReportService.reportByType();
            for (ProjectTypeReport projectTypeReport : projectTypeReports) {
                projectTypeReport.setName(projectTypeReport.getName().replaceAll("โครงการพัฒนา", ""));
            }
            projectRegionReports = projectReportService.reportByRegion();

            // swap position
            first = projectRegionReports.get(0);
            projectRegionReports.remove(0);
            projectRegionReports.add(first);

            sumIdeaReports = (ArrayList<SumIdeaReport>) ideaReportService.ideaSumReport();
            projectStatusReports = projectReportService.reportByStatus();
            sumKnowledgeReports = knowledgeReportService.sumKnowledgeReports();


            ideas = new ArrayList<String>();
            ideaStat = new ArrayList<String>();
            for (SumIdeaReport projectRegionReport : sumIdeaReports) {
                ideas.add(projectRegionReport.getName());
                ideaStat.add(String.valueOf(projectRegionReport.getTotal()));
            }


            regions = new ArrayList<String>();
            regionStat = new ArrayList<String>();
            for (ProjectRegionReport projectRegionReport : projectRegionReports) {
                regions.add(projectRegionReport.getName());
                regionStat.add(projectRegionReport.getTotal());
            }


            types = new ArrayList<String>();
            typeStat = new ArrayList<String>();
            for (ProjectTypeReport projectRegionReport : projectTypeReports) {
                types.add(projectRegionReport.getName());
                typeStat.add(projectRegionReport.getTotal());
            }


            royaltys = new ArrayList<String>();
            royaltyStat = new ArrayList<String>();
            for (ProjectRoyaltyReport projectRegionReport : projectRoyaltyReports) {
                royaltys.add(projectRegionReport.getRoyaltyName());
                royaltyStat.add(projectRegionReport.getTotal());
            }


            knowledges = new ArrayList<String>();
            knowledgeStat = new ArrayList<String>();
            for (SumKnowledgeReport knowledgeReport : sumKnowledgeReports) {
                knowledges.add(knowledgeReport.getName());
                knowledgeStat.add(String.valueOf(knowledgeReport.getTotal()));
            }


            status = new ArrayList<String>();
            statusStat = new ArrayList<String>();
            for (ProjectStatusReport statusReport : projectStatusReports) {
                status.add(statusReport.getName());
                statusStat.add(String.valueOf(statusReport.getTotal()));
            }

            totalSumIdeaReport = getTotalSumIdeaReport(sumIdeaReports);
            totalProjectRoyaltyReport = getTotalProjectRoyaltyReport(projectRoyaltyReports);
            totalProjectTypeReport = getTotalProjectTypeReport(projectTypeReports);
            totalProjectRegionReport = getTotalProjectRegionReport(projectRegionReports);
            totalProjectStatusReport = getTotalProjectStatusReport(projectStatusReports);
            totalSumKnowledgeReport = getTotalSumKnowledgeReport(sumKnowledgeReports);

            // new report
            projectStatusReportRegions = projectReportService.reportByStatusRegion();
            totalProjectStatusReportRegions = getTotalProjectStatusReportRegion(projectStatusReportRegions);
            for (ProjectStatusReportRegion projectStatusReportRegion : projectStatusReportRegions) {
                if (!projectStatusReportRegion.getTotal().equals("-"))
                    projectStatusReportRegion.setTotal(String.format("%,d", Integer.parseInt(projectStatusReportRegion.getTotal())));
                if (!projectStatusReportRegion.getComplete().equals("-"))
                    projectStatusReportRegion.setComplete(String.format("%,d", Integer.parseInt(projectStatusReportRegion.getComplete())));
                if (!projectStatusReportRegion.getOnly_data().equals("-"))
                    projectStatusReportRegion.setOnly_data(String.format("%,d", Integer.parseInt(projectStatusReportRegion.getOnly_data())));
                if (!projectStatusReportRegion.getOnly_photo().equals("-"))
                    projectStatusReportRegion.setOnly_photo(String.format("%,d", Integer.parseInt(projectStatusReportRegion.getOnly_photo())));
                if (!projectStatusReportRegion.getNot_input().equals("-"))
                    projectStatusReportRegion.setNot_input(String.format("%,d", Integer.parseInt(projectStatusReportRegion.getNot_input())));
            }
        }
        model.addAttribute("sumIdeaReports", sumIdeaReports);
        model.addAttribute("sumIdeaReportsTotal", totalSumIdeaReport);

        model.addAttribute("projectRoyaltyReports", projectRoyaltyReports);
        model.addAttribute("projectRoyaltyReportsTotal", totalProjectRoyaltyReport);

        model.addAttribute("projectTypeReports", projectTypeReports);
        model.addAttribute("projectTypeReportsTotal", totalProjectTypeReport);

        model.addAttribute("projectRegionReports", projectRegionReports);
        model.addAttribute("projectRegionReportsTotal", totalProjectRegionReport);

        model.addAttribute("projectStatusReports", projectStatusReports);
        model.addAttribute("projectStatusReportsTotal", totalProjectStatusReport);

        model.addAttribute("sumKnowledgeReports", sumKnowledgeReports);
        model.addAttribute("sumKnowledgeReportsTotal", totalSumKnowledgeReport);

        model.addAttribute("ideas", ideas);
        model.addAttribute("ideaStat", ideaStat);

        model.addAttribute("regions", regions);
        model.addAttribute("regionStat", regionStat);

        model.addAttribute("status", status);
        model.addAttribute("statusStat", statusStat);

        model.addAttribute("knowledges", knowledges);
        model.addAttribute("knowledgeStat", knowledgeStat);

        model.addAttribute("types", types);
        model.addAttribute("typeStat", typeStat);

        model.addAttribute("royaltys", royaltys);
        model.addAttribute("royaltyStat", royaltyStat);


        model.addAttribute("projectStatusReportsByRegion", projectStatusReportRegions);
        model.addAttribute("projectStatusReportsByRegionTotal", totalProjectStatusReportRegions);


//        projectStatusReportsByRegion
//        projectStatusReportsByRegionTotal

    }

    private ProjectStatusReportRegion getTotalProjectStatusReportRegion(List<ProjectStatusReportRegion> projectStatusReportRegions) {
        int total = 0, c1 = 0, c2 = 0, c3 = 0, c4 = 0;
        for (ProjectStatusReportRegion projectStatusReportRegion : projectStatusReportRegions) {
            total += Integer.parseInt(projectStatusReportRegion.getTotal());
            c1 += !projectStatusReportRegion.getComplete().equals("-") ? Integer.parseInt(projectStatusReportRegion.getComplete()) : 0;
            c2 += !projectStatusReportRegion.getOnly_data().equals("-") ? Integer.parseInt(projectStatusReportRegion.getOnly_data()) : 0;
            c3 += !projectStatusReportRegion.getOnly_photo().equals("-") ? Integer.parseInt(projectStatusReportRegion.getOnly_photo()) : 0;
            c4 += !projectStatusReportRegion.getNot_input().equals("-") ? Integer.parseInt(projectStatusReportRegion.getNot_input()) : 0;
        }

        ProjectStatusReportRegion output = new ProjectStatusReportRegion();
        output.setTotal(String.format("%,d", total));
        output.setComplete(String.format("%,d", c1));
        output.setOnly_data(String.format("%,d", c2));
        output.setOnly_photo(String.format("%,d", c3));
        output.setNot_input(String.format("%,d", c4));
        return output;
    }

    @RequestMapping
    public String index(Model model, Principal principal) {
        logService.dashboard(principal);

        return "dashboard";
    }


    private SumKnowledgeReport getTotalSumKnowledgeReport(List<SumKnowledgeReport> sumKnowledgeReports) {
        int total = 0;
        for (SumKnowledgeReport sumKnowledgeReport : sumKnowledgeReports) {
            total += sumKnowledgeReport.getTotal();
        }
        SumKnowledgeReport target = new SumKnowledgeReport();
        target.setName("ยอดรวม");
        target.setTotal(total);
        return target;
    }

    private ProjectStatusReport getTotalProjectStatusReport(List<ProjectStatusReport> projectStatusReports) {

        ProjectStatusReport target = new ProjectStatusReport();
        target.setName("ยอดรวม");

        int total = 0, rainy = 0, summer = 0, household = 0, village = 0, citizen = 0;

        for (ProjectStatusReport p : projectStatusReports) {
            total += Integer.parseInt(p.getTotal());
            rainy += p.getAreaRainy();
            summer += p.getAreaSummer();
            household += p.getHousehold();
            village += p.getVillage();
            citizen += p.getCitizen();
            // thousand seperator for total (string)
            p.setTotal(String.format(Locale.US, "%,d", Integer.parseInt(p.getTotal())));
        }
        target.setTotal(String.format(Locale.US, "%,d", total));
        target.setAreaRainy(rainy);
        target.setAreaSummer(summer);
        target.setHousehold(household);
        target.setVillage(village);
        target.setCitizen(citizen);
        return target;
    }

    private ProjectRegionReport getTotalProjectRegionReport(List<ProjectRegionReport> projectRegionReports) {
        ProjectRegionReport target
                = new ProjectRegionReport();
        target.setName("ยอดรวม");

        int total = 0, rainy = 0, summer = 0, household = 0, village = 0, citizen = 0;

        for (ProjectRegionReport p : projectRegionReports) {
            total += Integer.parseInt(p.getTotal());
            rainy += p.getAreaRainy();
            summer += p.getAreaSummer();
            household += p.getHousehold();
            village += p.getVillage();
            citizen += p.getCitizen();
            // thousand seperator for total (string)
            p.setTotal(String.format(Locale.US, "%,d", Integer.parseInt(p.getTotal())));
        }
        target.setTotal(String.format(Locale.US, "%,d", total));
        target.setAreaRainy(rainy);
        target.setAreaSummer(summer);
        target.setHousehold(household);
        target.setVillage(village);
        target.setCitizen(citizen);
        return target;
    }

    private ProjectTypeReport getTotalProjectTypeReport(List<ProjectTypeReport> projectTypeReports) {
        ProjectTypeReport target = new ProjectTypeReport();
        target.setName("ยอดรวม");

        int total = 0, rainy = 0, summer = 0, household = 0, village = 0, citizen = 0;

        for (ProjectTypeReport p : projectTypeReports) {
            total += Integer.parseInt(p.getTotal());
            rainy += p.getAreaRainy();
            summer += p.getAreaSummer();
            household += p.getHousehold();
            village += p.getVillage();
            citizen += p.getCitizen();
            // thousand seperator for total (string)
            p.setTotal(String.format(Locale.US, "%,d", Integer.parseInt(p.getTotal())));
        }
        target.setTotal(String.format(Locale.US, "%,d", total));
        target.setAreaRainy(rainy);
        target.setAreaSummer(summer);
        target.setHousehold(household);
        target.setVillage(village);
        target.setCitizen(citizen);
        return target;

    }

    private ProjectRoyaltyReport getTotalProjectRoyaltyReport(List<ProjectRoyaltyReport> projectRoyaltyReports) {

        ProjectRoyaltyReport target
                = new ProjectRoyaltyReport();
        target.setRoyaltyName("ยอดรวม");

        int total = 0, rainy = 0, summer = 0, household = 0, village = 0, citizen = 0;

        for (ProjectRoyaltyReport projectRoyaltyReport : projectRoyaltyReports) {
            total += Integer.parseInt(projectRoyaltyReport.getTotal());
            rainy += projectRoyaltyReport.getAreaRainy();
            summer += projectRoyaltyReport.getAreaSummer();
            household += projectRoyaltyReport.getHousehold();
            village += projectRoyaltyReport.getVillage();
            citizen += projectRoyaltyReport.getCitizen();
            // thousand seperator for total (string)
            projectRoyaltyReport.setTotal(String.format(Locale.US, "%,d", Integer.parseInt(projectRoyaltyReport.getTotal())));
        }
        target.setTotal(String.format(Locale.US, "%,d", total));
        target.setAreaRainy(rainy);
        target.setAreaSummer(summer);
        target.setHousehold(household);
        target.setVillage(village);
        target.setCitizen(citizen);
        return target;
    }

    private SumIdeaReport getTotalSumIdeaReport(List<SumIdeaReport> sumIdeaReports) {
        int total = 0;
        for (SumIdeaReport sumIdeaReport : sumIdeaReports) {
            total += sumIdeaReport.getTotal();
        }

        SumIdeaReport target = new SumIdeaReport();
        target.setName("ยอดรวม");
        target.setTotal(total);
        return target;
    }
}
