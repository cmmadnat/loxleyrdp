package com.loxley.rdpmap.web;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.*;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.repackaged.com.google.common.collect.FluentIterable;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.*;
import com.loxley.rdpmap.service.IdeaService;
import com.loxley.rdpmap.service.LocationService;
import com.loxley.rdpmap.service.LogService;
import com.loxley.rdpmap.service.RoyaltyService;
import com.loxley.rdpmap.util.ThaiDateFormat;
import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/idea")
public class IdeaController extends IdeaControllerBase {
    private static final int LIMIT_LENGTH = 250;
    @Autowired
    IdeaService ideaService;
    @Autowired
    RoyaltyService royaltyService;
    @Autowired
    LocationService locationService;

    @Autowired
    LogService logService;


    @RequestMapping
    public String index(Model model, Principal principal) {

        logService.searchIdea(principal);

        List<Province> provinces = locationService.findAllProvince();
        List<Royalty> royalties = royaltyService.findAll();
        model.addAttribute("provinces", provinces);
        model.addAttribute("royalties", royalties);
        model.addAttribute("months", ThaiDateFormat.getMonths());
        return "idea";
    }

    @RequestMapping("json")
    @ResponseBody
    public List<Idea> list(@RequestParam(defaultValue = "0", required = false) int page, @RequestParam(defaultValue = "10", required = false) int limit) {

        List<Idea> all = ideaService.findAll(page, limit);
        for (Idea idea : all) {
            idea.setThaiDate(ThaiDateFormat.formatDateShort(idea.getDate()));
            if (idea.getDetail().length() > LIMIT_LENGTH)
                idea.setDetail(idea.getDetail().substring(0, LIMIT_LENGTH) + "...");
        }
        return all;
    }

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String newIdea(Model model) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        String uploadUrl = "/admin/idea/new";
        Idea target = new Idea();

        DateTime beDate = new DateTime(target.getDate());
        beDate = beDate.withYear(DateTime.now().getYear() + 543);
        target.setDate(beDate.toDate());

        model.addAttribute("target", target);
        model.addAttribute("royalty", royaltyService.findAll());
        model.addAttribute("provinces", locationService.findAllProvince());
        model.addAttribute("relatedProject", new ArrayList<Project>());
        model.addAttribute("topImage", new ArrayList<RoyalProjectImage>());
        model.addAttribute("uploadUrl", uploadUrl);

        return "ideaNew";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    @ResponseBody
    public String newIdeaPost(Model model, @Validated Idea idea, BindingResult bindingResult, HttpServletRequest request, Principal principal) {


        Long id = ideaService.save(idea);

        processIdea(idea, idea, request);
        ideaService.update(id, idea);
        idea.setUuid(id);

        logService.createIdea(idea, principal);

        return id + "";
    }


    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public String findOne(@PathVariable("id") Long id, Model model) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

        Idea target = ideaService.findOne(id);
        if (target == null) return "ideaDeleted";

        DateTime beDate = new DateTime(target.getDate());
        beDate = beDate.withYear(target.getYear());
        target.setDate(beDate.toDate());

        model.addAttribute("target", target);
        model.addAttribute("royalty", royaltyService.findAll());
        model.addAttribute("provinces", locationService.findAllProvince());
        model.addAttribute("relatedProject", ideaService.findRelatedProject(target));
        model.addAttribute("topImage", FluentIterable.from(target.getImages()).limit(7).toList());
//        model.addAttribute("uploadUrl", blobstoreService.createUploadUrl("/admin/idea/" + id));
        model.addAttribute("uploadUrl", "/admin/idea/" + id);
        return "ideaNew";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String update(@PathVariable("id") Long id,
                         @Validated Idea idea, BindingResult bindingResult, HttpServletRequest request, Principal principal) {
        Idea one = ideaService.findOne(id);

        processIdea(one, idea, request);

        ideaService.update(id, idea);
        logService.updateIdea(idea, principal);
        return "redirect:/admin#/admin/idea/" + id;
    }


    @RequestMapping("count")
    @ResponseBody
    public Integer countThought() {
        return ideaService.count();
    }

    @RequestMapping(value = "searchCount", headers = "Accept=application/json")
    @ResponseBody
    public long countSearch(@RequestParam("query") String query) {
        return ideaService.countSearch(query);
    }

    @RequestMapping("search")
    @ResponseBody
    public List<Idea> search(@RequestParam(defaultValue = "0", required = false) int page,
                             @RequestParam(defaultValue = "10", required = false) int limit,
                             @RequestParam("query") String query) {
        List<Idea> all = ideaService.search(query, page, limit);
        for (Idea idea : all) {
            idea.setThaiDate(ThaiDateFormat.formatDateShort(idea.getDate()));
            if (idea.getDetail().length() > LIMIT_LENGTH)
                idea.setDetail(idea.getDetail().substring(0, LIMIT_LENGTH) + "...");

        }
        return all;
    }

    @RequestMapping("delete")
    public String delete(@RequestParam("id") Long id, Principal principal, @RequestParam(value = "close", required = false) Boolean close) {
        Idea idea = ideaService.findOne(id);

        if (idea == null) return "ideaDeleted";

        ideaService.delete(id);
        logService.deleteIdea(idea, principal);
        if (close != null)
            return "closeTab";
        else return "redirect:/admin#/admin/idea";
    }

    @RequestMapping(value = "gallery/{id}/delete", method = RequestMethod.POST)
    public String galleryDelete(@PathVariable("id") Long id, Model model, HttpServletRequest request,
                                @RequestParam("deleteIndex") int index) {
        Idea idea = ideaService.findOne(id);

        RoyalProjectImage target = idea.getImages().get(index);

        if (target.getKey() != null)
            try {
                BlobstoreService service = BlobstoreServiceFactory.getBlobstoreService();
                service.delete(new BlobKey(target.getKey()));
            } catch (Exception e) {

            }
        idea.getImages().remove(index);

        ideaService.update(id, idea);


        return "redirect:/admin/idea/" + id + "/uploadPhoto";
    }

    @RequestMapping("download/{key}/{fileName}")
    public void download(@PathVariable("key") String key, HttpServletResponse response) throws IOException {
        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();
        BlobKey blobKey = new BlobKey(key);
        service.serve(blobKey, response);
    }

    @RequestMapping("{id}/deleteFile")
    public String deleteFile(@PathVariable("id") Long id, @RequestParam("deleteIndex") List<Integer> deleteIndex) {

        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();
        Idea idea = ideaService.findOne(id);

        List<RoyalProjectFile> files = idea.getFiles();

        ArrayList<RoyalProjectFile> deleteItem = new ArrayList<RoyalProjectFile>();
        for (Integer integer : deleteIndex) {
            RoyalProjectFile royalProjectFile = files.get(integer);
            deleteItem.add(royalProjectFile);
        }

        for (RoyalProjectFile royalProjectFile : deleteItem) {
            BlobKey key = new BlobKey(royalProjectFile.getKey());
            service.delete(key);
        }

        files.removeAll(deleteItem);
        idea.setFiles(files);
        ideaService.update(id, idea);

        return "redirect:/admin#/admin/idea/" + id;
    }

    @RequestMapping("{id}/deleteFile/file")
    public String deleteFileSingle(@PathVariable("id") Long id, @RequestParam("deleteIndex") Integer target) {
        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();
        Idea idea = ideaService.findOne(id);

        processLogo(idea.getFiles());

        List<RoyalProjectFile> files = idea.getFiles();

        ArrayList<RoyalProjectFile> deleteItem = new ArrayList<RoyalProjectFile>();

        RoyalProjectFile r = files.get(target);
        deleteItem.add(r);

        for (RoyalProjectFile royalProjectFile : deleteItem) {
            if (royalProjectFile.getKey() == null) continue;
            BlobKey key = new BlobKey(royalProjectFile.getKey());
            service.delete(key);
        }

        files.removeAll(deleteItem);
        idea.setFiles(files);
        ideaService.update(id, idea);

        return "redirect:/admin/idea/" + id + "/uploadFile";
    }



    @RequestMapping(value = "{id}/uploadPhoto", method = RequestMethod.GET)
    public String uploadPhoto(@PathVariable("id") Long id, Model model) {
        Idea idea = ideaService.findOne(id);

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        final String gsBucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName()
                + "/ideaPhoto";
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(gsBucketName);

        model.addAttribute("uploadURL", blobstoreService.createUploadUrl("/admin/idea/" + id + "/uploadPhoto", option));
        model.addAttribute("target", idea);
        model.addAttribute("type", "idea");
        model.addAttribute("typeName", "พระราชดำริ");
        model.addAttribute("canEdit", true);
        return "partial/uploadPhoto";
    }

    @RequestMapping(value = "{id}/uploadFile", method = RequestMethod.GET)
    public String uploadFile(@PathVariable("id") Long id, Model model) {
        Idea idea = ideaService.findOne(id);

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        final String gsBucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName()
                + "/ideaFile";
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(gsBucketName);


        model.addAttribute("uploadURL", blobstoreService.createUploadUrl("/admin/idea/" + id + "/uploadFile", option));
        model.addAttribute("target", idea);
        model.addAttribute("type", "idea");
        model.addAttribute("typeName", "พระราชดำริ");
        model.addAttribute("canEdit", true);

        return "partial/uploadFile";
    }

    @RequestMapping(value = "{id}/uploadPhoto", method = RequestMethod.POST)
    public String uploadPhotoPost(HttpServletRequest request, @PathVariable("id") Long id) {
        Idea idea = ideaService.findOne(id);

        processIdeaWithFile(idea, idea, request, null);
        ideaService.update(id, idea);

        return "redirect:/admin/idea/" + id + "/uploadPhoto";
    }

    @RequestMapping(value = "{id}/uploadFile", method = RequestMethod.POST)
    public String uploadFilePost(HttpServletRequest request, @PathVariable("id") Long id, @RequestParam("fileName") String fileName) {
        Idea idea = ideaService.findOne(id);

        processIdeaWithFile(idea, idea, request, fileName);
        ideaService.update(id, idea);

        return "redirect:/admin/idea/" + id + "/uploadFile";
    }

    @RequestMapping(value = "gallery/{id}/caption", method = RequestMethod.POST)
    public String galleryCaption(@PathVariable("id") Long id, Model model, HttpServletRequest request,
                                 @RequestParam("captionIndex") int index, @RequestParam("caption") String caption) {

        Idea one = ideaService.findOne(id);

        RoyalProjectImage target = one.getImages().get(index);

        target.setCaption(caption);

        ideaService.update(id, one);

        return "redirect:/admin/idea/" + id + "/uploadPhoto";
    }
}
