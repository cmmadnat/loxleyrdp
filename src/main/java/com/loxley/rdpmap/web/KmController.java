package com.loxley.rdpmap.web;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.*;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Knowledge;
import com.loxley.rdpmap.service.KnowledgeService;
import com.loxley.rdpmap.service.LogService;
import com.loxley.rdpmap.service.RoyaltyService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/km")
public class KmController {
    private static final int LIMIT_LENGTH = 250;
    @Autowired
    KnowledgeService knowledgeService;
    @Autowired
    RoyaltyService royaltyService;

    @Autowired
    LogService logService;

    @RequestMapping
    public String index(Model model, Principal principal) {
        logService.searchKnowledge(principal);
        model.addAttribute("royalties", royaltyService.findAll());
        model.addAttribute("types", knowledgeService.findAllType());
        return "km";
    }

    @RequestMapping("new")
    public String newKM(Model model) {
        model.addAttribute("target", new Knowledge());
        model.addAttribute("royalty", royaltyService.findAll());
        model.addAttribute("type", knowledgeService.findAllType());
        model.addAttribute("uploadUrl", "/admin/km/new");
        return "kmNew";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    @ResponseBody
    public String newKMPost(Model model, @Validated Knowledge knowledge,
                            BindingResult bindingResult, HttpServletRequest request, Principal principal) {
//        Long id = ideaService.save(knowledge);
//
//        processIdea(knowledge, knowledge, request);
//        ideaService.update(id, knowledge);
        Long id = knowledgeService.save(knowledge);
        knowledge.setUuid(id);

        logService.createKnowledge(knowledge, principal);
        return id + "";
    }

    @RequestMapping("json")
    @ResponseBody
    public List<Knowledge> list(@RequestParam(defaultValue = "0", required = false) int page, @RequestParam(defaultValue = "10", required = false) int limit) {

        List<Knowledge> all = knowledgeService.findAll(page, limit);
//        for (Idea idea : all) {
//            idea.setThaiDate(ThaiDateFormat.formatDateShort(idea.getDate()));
//            if (idea.getDetail().length() > LIMIT_LENGTH)
//                idea.setDetail(idea.getDetail().substring(0, LIMIT_LENGTH) + "...");
//        }
        for (Knowledge knowledge : all) {
            if (knowledge.getDetail().length() > LIMIT_LENGTH)
                knowledge.setDetail(knowledge.getDetail().substring(0, LIMIT_LENGTH) + "...");
        }
        return all;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public String findOne(@PathVariable("id") Long id, Model model) {
//        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

        Knowledge target = knowledgeService.findOne(id);
        if (target == null) return "kmDeleted";

        model.addAttribute("target", target);
        model.addAttribute("royalty", royaltyService.findAll());
        model.addAttribute("type", knowledgeService.findAllType());
        model.addAttribute("uploadUrl", "/admin/km/" + id);
        return "kmNew";

//        DateTime beDate = new DateTime(target.getDate());
//        beDate = beDate.withYear(target.getYear());
//        target.setDate(beDate.toDate());
//
//        model.addAttribute("target", target);
//        model.addAttribute("royalty", royaltyService.findAll());
//        model.addAttribute("provinces", locationService.findAllProvince());
//        model.addAttribute("relatedProject", ideaService.findRelatedProject(target));
//        model.addAttribute("topImage", FluentIterable.from(target.getImages()).limit(7).toList());
//        model.addAttribute("uploadUrl", blobstoreService.createUploadUrl("/admin/idea/" + id));
//        model.addAttribute("uploadUrl", "/admin/idea/" + id);
//        return "ideaNew";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String update(@PathVariable("id") Long id,
                         @Validated Knowledge knowledge, BindingResult bindingResult, HttpServletRequest request, Principal principal) {
        Knowledge one = knowledgeService.findOne(id);

//        processIdea(one, idea, request);
        knowledge.setImages(one.getImages());
        knowledge.setFiles(one.getFiles());

        knowledgeService.update(id, knowledge);
        logService.updateKnowledge(knowledge, principal);
        return "redirect:/admin#/admin/km/" + id;
    }

    @RequestMapping(value = "{id}/uploadPhoto", method = RequestMethod.GET)
    public String uploadPhoto(@PathVariable("id") Long id, Model model) {
        Knowledge knowledge = knowledgeService.findOne(id);

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        final String gsBucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName()
                + "/kmPhoto";
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(gsBucketName);


        model.addAttribute("uploadURL", blobstoreService.createUploadUrl("/admin/km/" + id + "/uploadPhoto", option));
        model.addAttribute("target", knowledge);
        model.addAttribute("type", "km");
        model.addAttribute("typeName", "แนวคิดทฤษฎีฯ");
        model.addAttribute("canEdit", true);
        return "partial/uploadPhoto";
    }

    @RequestMapping(value = "{id}/uploadFile", method = RequestMethod.GET)
    public String uploadFile(@PathVariable("id") Long id, Model model) {
        Knowledge knowledge = knowledgeService.findOne(id);
        processLogo(knowledge.getFiles());

        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        final String gsBucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName()
                + "/kmFile";
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(gsBucketName);


        model.addAttribute("uploadURL", blobstoreService.createUploadUrl("/admin/km/" + id + "/uploadFile", option));
        model.addAttribute("target", knowledge);
        model.addAttribute("type", "km");
        model.addAttribute("typeName", "แนวคิดทฤษฎีฯ");
        model.addAttribute("canEdit", true);
        return "partial/uploadFile";
    }

    @RequestMapping(value = "{id}/uploadPhoto", method = RequestMethod.POST)
    public String uploadPhotoPost(HttpServletRequest request, @PathVariable("id") Long id) {
        Knowledge knowledge = knowledgeService.findOne(id);

        processKmWithFile(knowledge, knowledge, request, null);
        knowledgeService.update(id, knowledge);

        return "redirect:/admin/km/" + id + "/uploadPhoto";
    }

    private void processKmWithFile(Knowledge newOne, Knowledge original, HttpServletRequest request, String fileName) {
        try {
            BeanUtils.copyProperty(newOne, "files", original.getFiles());
            BeanUtils.copyProperty(newOne, "images", original.getImages());
            BeanUtils.copyProperty(newOne, "id", original.getId());
            newOne.setUuid(original.getUuid());

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();

        Map<String, List<BlobKey>> uploads = service.getUploads(request);
        Map<String, List<BlobInfo>> blobInfos = service.getBlobInfos(request);

        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        int counter = 0;

        List<BlobKey> file = uploads.get("file");
        if (file != null && !file.isEmpty() && fileName != null)
            for (BlobKey blobKey : file) {
                if (blobInfos.get("file").get(counter).getSize() == 0) continue;
                RoyalProjectFile e = new RoyalProjectFile();
                e.setKey(blobKey.getKeyString());
                e.setFileName(fileName);
                original.getFiles().add(e);
            }
        counter = 0;
        List<BlobKey> images = uploads.get("image");
        if (images != null && !images.isEmpty())
            for (BlobKey blobKey : images) {
                try {
                    if (blobInfos.get("image").get(counter++).getSize() == 0) continue;
                    RoyalProjectImage e = new RoyalProjectImage();
                    e.setKey(blobKey.getKeyString());
                    ServingUrlOptions option = ServingUrlOptions.Builder.withBlobKey(blobKey);
                    e.setServeURL(imagesService.getServingUrl(option));
                    original.getImages().add(e);
                } catch (IllegalArgumentException e) {

                }
            }

    }


    @RequestMapping(value = "{id}/uploadFile", method = RequestMethod.POST)
    public String uploadFilePost(HttpServletRequest request, @PathVariable("id") Long id, @RequestParam("fileName") String fileName) {
        Knowledge knowledge = knowledgeService.findOne(id);

        processKmWithFile(knowledge, knowledge, request, fileName);
        knowledgeService.update(id, knowledge);

        return "redirect:/admin/km/" + id + "/uploadFile";
    }

    @RequestMapping(value = "delete")
    public String delete(@RequestParam("id") Long id, Principal principal, @RequestParam(value = "close", required = false) Boolean close) {
        Knowledge one = knowledgeService.findOne(id);

        if (one == null) return "kmDeleted";

        knowledgeService.delete(id);
        logService.deleteKnowledge(one, principal);
        if (close != null)
            return "closeTab";
        else return "redirect:/admin#/admin/km";

    }


    @RequestMapping("count")
    @ResponseBody
    public int count() {
        return knowledgeService.count();
    }

    // search
    @RequestMapping(value = "searchCount", headers = "Accept=application/json")
    @ResponseBody
    public long countSearch(@RequestParam("query") String query) {
        return knowledgeService.countSearch(query);
    }

    @RequestMapping("search")
    @ResponseBody
    public List<Knowledge> search(@RequestParam(defaultValue = "0", required = false) int page,
                                  @RequestParam(defaultValue = "10", required = false) int limit,
                                  @RequestParam("query") String query) {
        final List<Knowledge> search = knowledgeService.search(query, page, limit);
        for (Knowledge knowledge : search) {
            if (knowledge.getDetail().length() > LIMIT_LENGTH) {
                knowledge.setDetail(knowledge.getDetail().substring(0, LIMIT_LENGTH) + "...");
            }
        }
        return search;
    }

    @RequestMapping(value = "gallery/{id}/delete", method = RequestMethod.POST)
    public String galleryDelete(@PathVariable("id") Long id, Model model, HttpServletRequest request,
                                @RequestParam("deleteIndex") int index) {

        Knowledge knowledge = knowledgeService.findOne(id);

        RoyalProjectImage target = knowledge.getImages().get(index);

        if (target.getKey() != null)
            try {
                BlobstoreService service = BlobstoreServiceFactory.getBlobstoreService();
                service.delete(new BlobKey(target.getKey()));
            } catch (Exception e) {

            }
        knowledge.getImages().remove(index);

        knowledgeService.update(id, knowledge);

        return "redirect:/admin/km/" + id + "/uploadPhoto";
    }


    @RequestMapping("{id}/deleteFile/file")
    public String deleteFile(@PathVariable("id") Long id, @RequestParam("deleteIndex") List<Integer> deleteIndex) {
        BlobstoreService service = BlobstoreServiceFactory.getBlobstoreService();
        Knowledge knowledge = knowledgeService.findOne(id);

        List<RoyalProjectFile> files = knowledge.getFiles();

        ArrayList<RoyalProjectFile> deleteItem = new ArrayList<RoyalProjectFile>();
        for (Integer integer : deleteIndex) {
            RoyalProjectFile royalProjectFile = files.get(integer);
            deleteItem.add(royalProjectFile);
        }

        for (RoyalProjectFile royalProjectFile : deleteItem) {
            if (royalProjectFile.getKey() == null) continue;
            BlobKey key = new BlobKey(royalProjectFile.getKey());
            service.delete(key);
        }

        files.removeAll(deleteItem);
        knowledge.setFiles(files);
        knowledgeService.update(id, knowledge);

        return "redirect:/admin/km/" + id + "/uploadFile";
    }

    @RequestMapping(value = "gallery/{id}/caption", method = RequestMethod.POST)
    public String galleryCaption(@PathVariable("id") Long id, Model model, HttpServletRequest request,
                                 @RequestParam("captionIndex") int index, @RequestParam("caption") String caption) {

        Knowledge one = knowledgeService.findOne(id);

        RoyalProjectImage target = one.getImages().get(index);

        target.setCaption(caption);

        knowledgeService.update(id, one);


        return "redirect:/admin/km/" + id + "/uploadPhoto";
    }

    private void processLogo(List<RoyalProjectFile> files) {
        for (RoyalProjectFile file : files) {
            if (file.getFileName().endsWith("pdf"))
                file.setLogo("fa fa-file-pdf-o");
            else if (file.getFileName().toLowerCase().endsWith("doc") || file.getFileName().toLowerCase().endsWith("docx"))
                file.setLogo("fa fa-file-word-o");
            else if (file.getFileName().toLowerCase().endsWith("xls") || file.getFileName().toLowerCase().endsWith("xlsx"))
                file.setLogo("fa fa-file-excel-o");
            else if (file.getFileName().toLowerCase().endsWith("zip") || file.getFileName().toLowerCase().endsWith("rar"))
                file.setLogo("fa fa-file-zip-o");
            else if (file.getFileName().toLowerCase().endsWith("jpg") || file.getFileName().toLowerCase().endsWith("png") || file.getFileName().toLowerCase().endsWith("gif"))
                file.setLogo("fa fa-file-photo-o");
            else file.setLogo("fa fa-file-o");
            file.setLogo(file.getLogo() + " ");
        }
    }

    @RequestMapping("download/{key}/{fileName}")
    public void download(@PathVariable("key") String key, HttpServletResponse response) throws IOException {
        final BlobstoreService service = BlobstoreServiceFactory.getBlobstoreService();
        BlobKey blobKey = new BlobKey(key);
        service.serve(blobKey, response);
    }
}
