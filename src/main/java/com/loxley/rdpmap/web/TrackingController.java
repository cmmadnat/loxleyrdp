package com.loxley.rdpmap.web;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.ProjectTracker;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.service.LogService;
import com.loxley.rdpmap.service.ProjectService;
import com.loxley.rdpmap.service.TrackingService;
import com.loxley.rdpmap.util.IconGenerator;
import com.loxley.rdpmap.util.LinkExtractor;
import org.opengraph.MetaElement;
import org.opengraph.OpenGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by cmmad_000 on 5/5/2015.
 */
@Controller
@RequestMapping("/admin/tracking")
public class TrackingController {

    @Autowired
    IconGenerator iconGenerator;

    @Autowired
    TrackingService trackingService;
    @Autowired
    ProjectService projectService;

    @Autowired
    LogService logService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Principal principal) {
        logService.searchTracking(principal);
        return "tracking";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<ProjectTracker> findForProject(@PathVariable("id") Long id) {
        List<ProjectTracker> trackers = trackingService.findForProject(id);
        for (ProjectTracker tracker : trackers) {
            final ArrayList<RoyalProjectFile> files = tracker.getFiles();
            for (RoyalProjectFile file : files) {
                file.setLogo(iconGenerator.getIcon(file.getFileName()));
                file.setColor(iconGenerator.getIconColor(file.getFileName()));
            }
        }
        if (trackers.size() != 0) {
            Project project = projectService.findOne(id);

            String userRegion = getUserRegion();
            projectService.checkRight(userRegion, project);
            trackers.get(0).setCanEdit(project.isCanEdit());
//            trackers.get(0).setProject(project);
        }

        return trackers;
    }


    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    @ResponseBody
    public String postStatus(@PathVariable("id") Long id, @RequestBody ProjectTracker projectTracker, Principal
            principal) {
        logService.createTracking(principal);
        trackingService.postStatus(id, projectTracker, principal.getName());
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "tracker/{id}", method = RequestMethod.GET)
    public ProjectTracker findOne(@PathVariable("id") Long id) {
        final ProjectTracker one = trackingService.findOne(id);
        return one;
    }

    @ResponseBody
    @RequestMapping(value = "tracker/{id}", method = RequestMethod.DELETE)
    public String removeTracker(@PathVariable("id") Long id, Principal principal) {
        trackingService.delete(id);
        logService.deleteTracking(principal);
        return "success";
    }

    @RequestMapping(value = "tracker/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String updateStatus(@PathVariable("id") Long id, @RequestBody ProjectTracker projectTracker, Principal
            principal) {
        ProjectTracker old = trackingService.findOne(id);
        old.setName(projectTracker.getName());
        old.setStatus(projectTracker.getStatus());
        old.setListDate(projectTracker.getListDate());
        old.setYoutube(null);
        old.setTitle(null);
        old.setDescription(null);
        old.setLink(null);
        old.setImageURL(null);
        trackingService.update(old);

        logService.updateTracking(principal);

        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "tracker/file/{id}", method = RequestMethod.DELETE)
    public String removeTrackerFile(@RequestParam("deleteID") Integer[] deleteIDs, @PathVariable("id") Long id) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        ProjectTracker one = trackingService.findOne(id);

        ArrayList<RoyalProjectFile> royalProjectFiles = new ArrayList<RoyalProjectFile>();
        for (Integer deleteID : deleteIDs) {
            royalProjectFiles.add(one.getFiles().get(deleteID));
            blobstoreService.delete(new BlobKey(one.getFiles().get(deleteID).getKey()));
        }
        one.getFiles().removeAll(royalProjectFiles);

        trackingService.update(one);
        return "success";
    }


    @ResponseBody
    @RequestMapping(value = "tracker/image/{id}", method = RequestMethod.POST)
    public String updateTrackerImageCaption(@RequestParam("captionID") String captionID,
                                            @RequestParam("caption") String caption, @PathVariable("id") Long id) {

        ProjectTracker one = trackingService.findOne(id);
        RoyalProjectImage royalProjectImage = one.getImages().get(Integer.parseInt(captionID));
        royalProjectImage.setCaption(caption);

        trackingService.update(one);
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "tracker/image/{id}", method = RequestMethod.DELETE)
    public String removeTrackerImage(@RequestParam("deleteID") Integer[] deleteIDs, @PathVariable("id") Long id) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        ImagesService imagesService = ImagesServiceFactory.getImagesService();
        ProjectTracker one = trackingService.findOne(id);

        ArrayList<RoyalProjectImage> royalProjectImages = new ArrayList<RoyalProjectImage>();
        for (Integer deleteID : deleteIDs) {
            RoyalProjectImage royalProjectImage = one.getImages().get(deleteID);
            royalProjectImages.add(royalProjectImage);
            try {
                BlobKey blobKey = new BlobKey(one.getImages().get(deleteID).getKey());
                imagesService.deleteServingUrl(blobKey);
                blobstoreService.delete(blobKey);
            } catch (Exception e) {
            }
        }
        one.getImages().removeAll(royalProjectImages);

        trackingService.update(one);
        return "success";
    }


    @ResponseBody
    @RequestMapping("parseText")
    public ParseLinkResult tryParse(@RequestParam("query") String query) throws Exception {
        List<String> strings = LinkExtractor.extractUrls(query);

        ParseLinkResult target = new ParseLinkResult();
        if (!strings.isEmpty()) {
            OpenGraph site =
                    new OpenGraph(strings.get(0), true);
            MetaElement[] properties = site.getProperties();
            for (MetaElement property : properties) {
                System.out.println(property.getProperty() + " " + property.getContent());

            }
            MetaElement[] titles = site.getProperties("title");
            MetaElement[] images = site.getProperties("image");
            MetaElement[] descriptions = site.getProperties("description");
            if (titles != null && titles.length != 0) target.setName(titles[0].getContent());
            if (images != null && images.length != 0) target.setImage(images[0].getContent());
            if (descriptions != null && descriptions.length != 0)
                target.setDescription(descriptions[0].getContent());
        }

        return target;
    }

    @RequestMapping(value = "upload/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getUploadURL(@PathVariable("id") Long trackingID) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(
                AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName() + "/trackingFile");
        return blobstoreService.createUploadUrl("/admin/tracking/upload/" + trackingID, option);
    }

    @RequestMapping(value = "upload/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String getUploadURLPost(@PathVariable("id") Long trackingID, HttpServletRequest
            request, @RequestParam("name") String fileName) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        Map<String, List<BlobKey>> uploads = blobstoreService.getUploads(request);
        List<BlobKey> file = uploads.get("file");
        if (file != null && !file.isEmpty() && fileName != null) {

            if (fileName.toLowerCase().endsWith("jpeg") || fileName.toLowerCase().endsWith("jpg") ||
                    fileName.toLowerCase().endsWith("png") || fileName.toLowerCase().endsWith("gif")) {
                BlobKey blobKey = file.get(0);
                ImagesService imagesService = ImagesServiceFactory.getImagesService();

                RoyalProjectImage e = new RoyalProjectImage();
                e.setKey(blobKey.getKeyString());
                ServingUrlOptions option = ServingUrlOptions.Builder.withBlobKey(blobKey);
                e.setServeURL(imagesService.getServingUrl(option));

                ProjectTracker one = trackingService.findOne(trackingID);
                one.getImages().add(e);
                trackingService.update(one);
            } else {
                BlobKey blobKey = file.get(0);

                RoyalProjectFile e = new RoyalProjectFile();
                e.setKey(blobKey.getKeyString());
                e.setFileName(fileName);

                ProjectTracker one = trackingService.findOne(trackingID);
                one.getFiles().add(e);
                trackingService.update(one);
            }

        }


        return "success";
    }

    class ParseLinkResult {
        String image, name, description;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    private String getUserRegion() {
        final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        String region = "0";

        for (GrantedAuthority authority : authorities) {
            final String authority1 = authority.getAuthority();
            if (authority1.startsWith("ROLE_REGION_")) {
                region = authority1.substring(12);
            }
        }
        return region;
    }

}
