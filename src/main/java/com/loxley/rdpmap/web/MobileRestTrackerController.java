package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.ProjectTracker;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.service.TrackingService;
import com.loxley.rdpmap.util.LinkExtractor;
import org.opengraph.MetaElement;
import org.opengraph.OpenGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * Created by cmmad_000 on 5/19/2015.
 */
@Controller
@RequestMapping("/mobile/tracking")
public class MobileRestTrackerController {

    @Autowired
    TrackingService trackingService;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "tracking";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<ProjectTracker> findForProject(@PathVariable("id") Long id) {
        List<ProjectTracker> trackers = trackingService.findForProject(id);
        return trackers;
    }

    @RequestMapping(value = "upload/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String postStatus(@PathVariable("id") Long id, @RequestBody ProjectTracker projectTracker, Principal principal) {
        long l = trackingService.postStatus(id, projectTracker, principal.getName());
        return l + "";
    }

    @ResponseBody
    @RequestMapping(value = "upload/tracker/{id}", method = RequestMethod.DELETE)
    public String removeTracker(@PathVariable("id") Long id) {
        trackingService.delete(id);
        return "success";
    }


    @RequestMapping(value = "upload/tracker/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getUploadURL(@PathVariable("id") Long trackingID) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        return blobstoreService.createUploadUrl("/admin/tracking/upload/" + trackingID);
    }

    @RequestMapping(value = "upload/tracker/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String getUploadURLPost(@PathVariable("id") Long trackingID, HttpServletRequest request, @RequestParam("name") String fileName) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        Map<String, List<BlobKey>> uploads = blobstoreService.getUploads(request);
        List<BlobKey> file = uploads.get("file");
        if (file != null && !file.isEmpty() && fileName != null) {

            if (fileName.toLowerCase().endsWith("jpg") || fileName.toLowerCase().endsWith("png") || fileName.toLowerCase().endsWith("gif")) {
                BlobKey blobKey = file.get(0);
                ImagesService imagesService = ImagesServiceFactory.getImagesService();

                RoyalProjectImage e = new RoyalProjectImage();
                e.setKey(blobKey.getKeyString());
                ServingUrlOptions option = ServingUrlOptions.Builder.withBlobKey(blobKey);
                e.setServeURL(imagesService.getServingUrl(option));

                ProjectTracker one = trackingService.findOne(trackingID);
                one.getImages().add(e);
                trackingService.update(one);
            } else {
                BlobKey blobKey = file.get(0);

                RoyalProjectFile e = new RoyalProjectFile();
                e.setKey(blobKey.getKeyString());
                e.setFileName(fileName);

                ProjectTracker one = trackingService.findOne(trackingID);
                one.getFiles().add(e);
                trackingService.update(one);
            }

        }

        return "success";
    }


    @ResponseBody
    @RequestMapping("parseText")
    public ParseLinkResult tryParse(@RequestParam("query") String query) throws Exception {
        List<String> strings = LinkExtractor.extractUrls(query);

        ParseLinkResult target = new ParseLinkResult();
        if (!strings.isEmpty()) {
            OpenGraph site =
                    new OpenGraph(strings.get(0), true);
            MetaElement[] properties = site.getProperties();
            for (MetaElement property : properties) {
                System.out.println(property.getProperty() + " " + property.getContent());

            }
            MetaElement[] titles = site.getProperties("title");
            MetaElement[] images = site.getProperties("image");
            MetaElement[] descriptions = site.getProperties("description");
            if (titles != null && titles.length != 0) target.setName(titles[0].getContent());
            if (images != null && images.length != 0) target.setImage(images[0].getContent());
            if (descriptions != null && descriptions.length != 0) target.setDescription(descriptions[0].getContent());
        }

        return target;
    }

    class ParseLinkResult {
        String image, name, description;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
