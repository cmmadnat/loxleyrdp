package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Knowledge;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.service.*;
import com.loxley.rdpmap.ui.SearchResult;
import com.loxley.rdpmap.ui.Slider;
import com.loxley.rdpmap.util.ThaiDateFormat;
import cz.jiripinkas.jsitemapgenerator.WebPageBuilder;
import cz.jiripinkas.jsitemapgenerator.generator.SitemapGenerator;
import cz.jiripinkas.jsitemapgenerator.generator.SitemapIndexGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/")
public class HomeController extends HomeControllerBase {
    private static final int KNOWLEDGE_PER_PAGE = 4;
    private static final int PROJECT_PER_PAGE = 12;
    private static final int IDEA_PER_PAGE = 12;
    private static final int IDEA_DETAIL_LENGTH_LIMIT = 250;
    private static final int KNOWLEDGE_DETAIL_LENGTH_LIMIT = 250;
    private static final int SITE_MAP_LIMIT = 500;

    @Autowired
    SettingService settingService;
    @Autowired
    ProjectService projectService;
    @Autowired
    private IdeaService ideaService;
    @Autowired
    KnowledgeService knowledgeService;

    private static final Logger logger = LoggerFactory
            .getLogger(HomeController.class);

    private String apiKey = "AIzaSyD5St6SnCpddeGs1I4lYlGHdypGCgGFWFc";
    @Autowired
    private LocationService locationService;


    @ModelAttribute
    public void addApiKey(Model model) {
        model.addAttribute("apiKey", apiKey);
    }

    @RequestMapping("index")
    public String redirectHome() {
        return "redirect:/";
    }

    @RequestMapping("sitemap.xml")
    public ResponseEntity<String> sitemap() {
        HttpHeaders headers = new HttpHeaders();


        SitemapIndexGenerator sitemapIndexGenerator = new SitemapIndexGenerator("http://projects.rdpb.go.th");
        // idea sitemap
        int count = projectService.count();
        int numPage = count / SITE_MAP_LIMIT + 1;
        for (int i = 0; i < numPage; i++) {
            sitemapIndexGenerator.addPage(new WebPageBuilder().name(i + "/sitemap-project.xml").build());
        }
        // idea sitemap
        count = ideaService.count();
        numPage = count / SITE_MAP_LIMIT + 1;
        for (int i = 0; i < numPage; i++) {
            sitemapIndexGenerator.addPage(new WebPageBuilder().name(i + "/sitemap-idea.xml").build());
        }
        // idea knowledge
        count = knowledgeService.count();
        numPage = count / SITE_MAP_LIMIT + 1;
        for (int i = 0; i < numPage; i++) {
            sitemapIndexGenerator.addPage(new WebPageBuilder().name(i + "/sitemap-knowledge.xml").build());
        }
        String string = sitemapIndexGenerator.constructSitemapString();


        headers.setContentType(MediaType.APPLICATION_XML);
        return new ResponseEntity(string, headers, HttpStatus.OK);
    }

    @RequestMapping("{page}/sitemap-project.xml")
    public ResponseEntity<String> sitemapProjectPage(@PathVariable("page") String page) {
        HttpHeaders headers = new HttpHeaders();

        // create web sitemap for web http://www.javavids.com
        SitemapGenerator sitemapGenerator = new SitemapGenerator("http://projects.rdpb.go.th");
        // add some URLs

        List<Project> all = projectService.findAll(Integer.parseInt(page), SITE_MAP_LIMIT);
        processLogoAndURL(all);
        for (Project project : all) {
            sitemapGenerator.addPage(new WebPageBuilder().name(project.getUrl())
                    .priorityMax().changeFreqMonthly().lastModNow().build());
        }

        // generate sitemap and save it to file /var/www/sitemap.xml

        String string = sitemapGenerator.constructSitemapString();

        headers.setContentType(MediaType.APPLICATION_XML);
        return new ResponseEntity(string, headers, HttpStatus.OK);
    }

    @RequestMapping("{page}/sitemap-idea.xml")
    public ResponseEntity<String> sitemapIdeaPage(@PathVariable("page") String page) {
        HttpHeaders headers = new HttpHeaders();

        // create web sitemap for web http://www.javavids.com
        SitemapGenerator sitemapGenerator = new SitemapGenerator("http://projects.rdpb.go.th");
        // add some URLs

        List<Idea> all = ideaService.findAll(Integer.parseInt(page), SITE_MAP_LIMIT);
        processIdea(all);
        for (Idea idea : all) {
            sitemapGenerator.addPage(new WebPageBuilder().name(idea.getUrl())
                    .priorityMax().changeFreqMonthly().lastModNow().build());
        }

        // generate sitemap and save it to file /var/www/sitemap.xml

        String string = sitemapGenerator.constructSitemapString();

        headers.setContentType(MediaType.APPLICATION_XML);
        return new ResponseEntity(string, headers, HttpStatus.OK);
    }

    @RequestMapping("{page}/sitemap-knowledge.xml")
    public ResponseEntity<String> sitemapKnowledgePage(@PathVariable("page") String page) {
        HttpHeaders headers = new HttpHeaders();

        // create web sitemap for web http://www.javavids.com
        SitemapGenerator sitemapGenerator = new SitemapGenerator("http://projects.rdpb.go.th");
        // add some URLs

        List<Knowledge> all = knowledgeService.findAll(Integer.parseInt(page), SITE_MAP_LIMIT);
        processKnowledge(all);
        for (Knowledge knowledge : all) {
            sitemapGenerator.addPage(new WebPageBuilder().name(knowledge.getUrl())
                    .priorityMax().changeFreqMonthly().lastModNow().build());
        }

        // generate sitemap and save it to file /var/www/sitemap.xml

        String string = sitemapGenerator.constructSitemapString();

        headers.setContentType(MediaType.APPLICATION_XML);
        return new ResponseEntity(string, headers, HttpStatus.OK);
    }

    @RequestMapping
    public String index(Model model) {
        List<Slider> sliders = settingService.getSlider();
        List<Project> projects = projectService.getFirstpageProjects();
        List<Idea> ideas = ideaService.getFirstpageIdeas();
        List<Knowledge> kms = knowledgeService.getFirstPageKnowledges();
        processLogoAndURL(projects);
        processIdea(ideas);
        processKnowledge(kms);

//        List<List<Project>> partition = Lists.partition(projects, 4);
        if (projects.size() > 4) projects = Lists.partition(projects, 4).get(0);

        model.addAttribute("slider", sliders);
        model.addAttribute("newProjects", projects);
//        model.addAttribute("randomProjects", partition.get(1));
        model.addAttribute("ideas", ideas);
        model.addAttribute("km", kms);

        return "home/index";
    }

    private void processKnowledge(List<Knowledge> kms) {
        for (Knowledge km : kms) {
            processKnowledge(km);
            if (km.getDetail().length() > KNOWLEDGE_DETAIL_LENGTH_LIMIT) {
                km.setDetail(km.getDetail().substring(0, KNOWLEDGE_DETAIL_LENGTH_LIMIT) + "...");
            }
            try {
                km.setUrl("/knowledges/" + km.getUuid() + "/" + URLEncoder.encode(km.getName().replace("/", "-"), "UTF-8"));
            } catch (UnsupportedEncodingException e) {

            }
        }
    }

    private void processIdea(List<Idea> all) {
        for (Idea idea : all) {
            try {
                idea.setUrl("/ideas/" + idea.getUuid() + "/" + URLEncoder.encode(idea.getAddress().replace("/", "-"), "UTF-8"));
            } catch (UnsupportedEncodingException e) {

            }
            processIdea(idea);
            idea.setThaiDate(ThaiDateFormat.formatDateFull(idea.getDate()));
            if (idea.getDetail().length() > IDEA_DETAIL_LENGTH_LIMIT)
                idea.setDetail(idea.getDetail().substring(0, IDEA_DETAIL_LENGTH_LIMIT) + "...");
        }
    }

    @RequestMapping("ideas")
    public String idea(@RequestParam(value = "page", defaultValue = "1") Integer page, Model model) {

        if (page < 0) page = 1;
        page--;

        List<Idea> all = ideaService.findAll(page, IDEA_PER_PAGE);
        processIdea(all);

        model.addAttribute("list", all);
        model.addAttribute("page", page + 1);
        model.addAttribute("pageCount", ideaService.count() / IDEA_PER_PAGE + 1);

        return "home/idea";
    }


    @RequestMapping("projects")
    public String project(@RequestParam(value = "page", defaultValue = "1") Integer page, Model model) {
        if (page < 0) page = 1;
        page--;

        List<Project> all = projectService.findAll(page, PROJECT_PER_PAGE);
        processLogoAndURL(all);
        model.addAttribute("list", all);
        model.addAttribute("page", page + 1);
        model.addAttribute("pageCount", projectService.count() / PROJECT_PER_PAGE + 1);

        return "home/project";
    }

    @RequestMapping("knowledges")
    public String knowledge(@RequestParam(value = "page", defaultValue = "1") Integer page, Model model) {
        if (page < 0) page = 1;
        page--;

        List<Knowledge> all = knowledgeService.findAll(page, KNOWLEDGE_PER_PAGE);
//        processLogoAndURL(all);
        processKnowledge(all);
        model.addAttribute("list", all);
        model.addAttribute("page", page + 1);
        model.addAttribute("pageCount", knowledgeService.count() / KNOWLEDGE_PER_PAGE + 1);

        return "home/knowledge";

    }


    @RequestMapping(value = {"projects/{id}/{name}", "project/{id}"})
    public String projectOne(@PathVariable("id") Long id, Model model) {
        Project project = projectService.findOne(id);
        processProject(project);
        if (project.getStartDate() != null)
            project.setStartDateThaiDate(ThaiDateFormat.formatDateFull(project.getStartDate()));

        processLogo(project.getFiles());

        model.addAttribute("target", project);

        return "home/projectSingle";
    }

    @RequestMapping(value = {"ideas/{id}/{name}", "ideas/{id}"})
    public String ideaOne(@PathVariable("id") Long id, Model model) {
        Idea idea = ideaService.findOne(id);
        idea.setThaiDate(ThaiDateFormat.formatDateFull(idea.getDate()));
        processIdea(idea);
        processLogo(idea.getFiles());
        model.addAttribute("target", idea);

        return "home/ideaSingle";
    }

    @RequestMapping(value = {"knowledges/{id}/{name}", "knowledges/{id}"})
    public String knowledgeOne(@PathVariable("id") Long id, Model model) {
        Knowledge knowledge = knowledgeService.findOne(id);
//        knowledge.setThaiDate(ThaiDateFormat.formatDateFull(knowledge.getDate()));
        processKnowledge(knowledge);
        processLogo(knowledge.getFiles());
        model.addAttribute("target", knowledge);

        return "home/knowledgeSingle";
    }


    @RequestMapping("map")
    public String map() {
        return "home/mapNaked";
    }

    @RequestMapping("search")
    public String search(@RequestParam("s") String query, Model model,
                         @RequestParam(value = "type", defaultValue = "project") String type,
                         @RequestParam(value = "limit", defaultValue = "10") int limit,
                         @RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "region", defaultValue = "none") String region) {

        ArrayList<SearchResult> list = new ArrayList<SearchResult>();

        final String[] split = query.split(" ");
        for (String s : split) {
            if (s.startsWith("region=")){
                query = query.replace(s, "");
            }
        }

        long l = 0;
        if (type.equals("project"))
            try {
                List<Project> projects = new ArrayList<Project>();
                if (!region.equals("none")) {
                    query = query + " region=" + region;
                }
                projects = projectService.search(query, page - 1, limit);
                for (Project project : projects) {
                    SearchResult obj = new SearchResult();
                    obj.setName(project.getName());
                    obj.setUrl("/projects/" + project.getUuid() + "/" + URLEncoder.encode(project.getName(), "UTF-8"));
                    obj.setDetail(project.getDetail());
                    obj.setImageURL(project.getImages().size() != 0 ? project.getImages().get(0).getServeURL() : project.getImgFront());
                    obj.setProvince(project.getProvinceID());
                    obj.setRoyalty(project.getRoyaltyID());

                    list.add(obj);

                }
                l = projectService.countSearch(query);
                model.addAttribute("query", query);
                model.addAttribute("list", list);
                model.addAttribute("searchCount", l);
                model.addAttribute("region", locationService.findAllRegion());
                model.addAttribute("page", page);
                model.addAttribute("pageCount", l / limit + 1);
            } catch (Exception e) {

            }
        else if (type.equals("idea")) try {
            List<Idea> ideas = new ArrayList<Idea>();
            ideas = ideaService.search(query, page - 1, limit);
            for (Idea project : ideas) {
                SearchResult obj = new SearchResult();
                obj.setName(project.getAddress());
                obj.setUrl("/ideas/" + project.getUuid() + "/" + URLEncoder.encode(project.getAddress(), "UTF-8"));
                obj.setDetail(project.getDetail());
                obj.setImageURL(project.getImages().size() != 0 ? project.getImages().get(0).getServeURL() : project.getImgFront());
                obj.setProvince(project.getProvinceID());
                obj.setRoyalty(project.getRoyaltyID());

                list.add(obj);

            }
            l = ideaService.countSearch(query);
            model.addAttribute("query", query);
            model.addAttribute("list", list);
            model.addAttribute("searchCount", l);

            model.addAttribute("page", page);
            model.addAttribute("pageCount", l / limit + 1);
        } catch (Exception e) {
        }
        else try {
                List<Knowledge> knowledges = new ArrayList<Knowledge>();
                knowledges = knowledgeService.search(query, page - 1, limit);
                for (Knowledge project : knowledges) {
                    SearchResult obj = new SearchResult();
                    obj.setName(project.getName());
                    obj.setUrl("/knowledges/" + project.getUuid() + "/" + URLEncoder.encode(project.getName(), "UTF-8"));
                    obj.setDetail(project.getDetail());
                    obj.setImageURL(project.getImages().size() != 0 ? project.getImages().get(0).getServeURL() : project.getImgFront());
                    obj.setRoyalty(project.getRoyaltyID());

                    list.add(obj);

                }
                l = knowledgeService.countSearch(query);
                model.addAttribute("query", query);
                model.addAttribute("list", list);
                model.addAttribute("searchCount", l);

                model.addAttribute("page", page);
                model.addAttribute("pageCount", l / limit + 1);
            } catch (Exception e) {
            }

        model.addAttribute("type", type);

        return "home/search";
    }

    @RequestMapping("single")
    public String single() {
        return "home/single";
    }

    @RequestMapping("favicon.ico")
    String favicon() {
        return "forward:/assets/favicon.ico";
    }

    @RequestMapping("mapNaked")
    public String mapNaked() {
        return "home/mapNaked";

    }

    @RequestMapping("error")
    public String error() {
        return "home/error";
    }

    @RequestMapping("download/{key}/{filename}")
    public void downloadFile(@PathVariable("key") String blobKey, HttpServletResponse response, @PathVariable("filename") String filename) throws IOException {
        BlobstoreServiceFactory.getBlobstoreService().serve(new BlobKey(blobKey), response);
    }


}
