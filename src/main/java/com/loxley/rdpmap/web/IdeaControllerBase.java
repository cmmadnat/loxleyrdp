package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Idea;
import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * Created by cmmad_000 on 2/29/2016.
 */
public class IdeaControllerBase {

    protected void processLogo(List<RoyalProjectFile> files) {
        for (RoyalProjectFile file : files) {
            if (file.getFileName().endsWith("pdf"))
                file.setLogo("fa fa-file-pdf-o");
            else if (file.getFileName().toLowerCase().endsWith("doc") || file.getFileName().toLowerCase().endsWith("docx"))
                file.setLogo("fa fa-file-word-o");
            else if (file.getFileName().toLowerCase().endsWith("xls") || file.getFileName().toLowerCase().endsWith("xlsx"))
                file.setLogo("fa fa-file-excel-o");
            else if (file.getFileName().toLowerCase().endsWith("zip") || file.getFileName().toLowerCase().endsWith("rar"))
                file.setLogo("fa fa-file-zip-o");
            else if (file.getFileName().toLowerCase().endsWith("jpg") || file.getFileName().toLowerCase().endsWith("png") || file.getFileName().toLowerCase().endsWith("gif"))
                file.setLogo("fa fa-file-photo-o");
            else file.setLogo("fa fa-file-o");
            file.setLogo(file.getLogo() + " ");
        }
    }
    protected void processIdeaWithFile(Idea original, Idea newOne, HttpServletRequest request, String fileName) {
        try {
            BeanUtils.copyProperty(newOne, "files", original.getFiles());
            BeanUtils.copyProperty(newOne, "images", original.getImages());
            BeanUtils.copyProperty(newOne, "id", original.getId());
            newOne.setUuid(original.getUuid());

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();

        Map<String, List<BlobKey>> uploads = service.getUploads(request);
        Map<String, List<BlobInfo>> blobInfos = service.getBlobInfos(request);

        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        int counter = 0;

        List<BlobKey> file = uploads.get("file");
        if (file != null && !file.isEmpty() && fileName != null)
            for (BlobKey blobKey : file) {
                if (blobInfos.get("file").get(counter).getSize() == 0) continue;
                RoyalProjectFile e = new RoyalProjectFile();
                e.setKey(blobKey.getKeyString());
                e.setFileName(fileName);
                original.getFiles().add(e);
            }
        counter = 0;
        List<BlobKey> images = uploads.get("image");
        if (images != null && !images.isEmpty())
            for (BlobKey blobKey : images) {
                try {
                    if (blobInfos.get("image").get(counter++).getSize() == 0) continue;
                    RoyalProjectImage e = new RoyalProjectImage();
                    e.setKey(blobKey.getKeyString());
                    ServingUrlOptions option = ServingUrlOptions.Builder.withBlobKey(blobKey);
                    e.setServeURL(imagesService.getServingUrl(option));
                    original.getImages().add(e);
                } catch (IllegalArgumentException e) {

                }
            }
        Integer year = new DateTime(newOne.getDate()).getYear() + 543;
        newOne.setYear(year.shortValue());


    }

    protected void processIdea(Idea original, Idea newOne, HttpServletRequest request) {
        try {
            BeanUtils.copyProperty(newOne, "files", original.getFiles());
            BeanUtils.copyProperty(newOne, "images", original.getImages());
            BeanUtils.copyProperty(newOne, "id", original.getId());
            newOne.setUuid(original.getUuid());

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        Integer year = new DateTime(newOne.getDate()).getYear() + 543;
        newOne.setYear(year.shortValue());


    }
}
