package com.loxley.rdpmap.web;

import com.loxley.rdpmap.domain.mobile.MobileImage;
import com.loxley.rdpmap.domain.mobile.MobileLocation;
import com.loxley.rdpmap.service.LogService;
import com.loxley.rdpmap.service.MobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("admin/approval")
public class ApprovalController {

    @Autowired
    MobileService mobileService;
    @Autowired
    LogService logService;

    @RequestMapping
    public String index(Principal principal) {
        logService.approve(principal);
        return "approval";
    }

    @RequestMapping("image")
    @ResponseBody
    public List<MobileImage> findAllImage() {
        final List<MobileImage> allImage = mobileService.findAllImage();
        List<MobileImage> output = new ArrayList<MobileImage>();

        final String userRegion = getUserRegion();

        for (MobileImage mobileImage : allImage) {
            final String name = mobileImage.getProject().getProvinceID().getRegionID().getName();
            Boolean canEdit = checkRight(userRegion, name);
            if (canEdit) output.add(mobileImage);
        }
        return output;
    }

    @RequestMapping("location")
    @ResponseBody
    public List<MobileLocation> findAllLocation() {

        final List<MobileLocation> allImage = mobileService.findAllLocation();
        List<MobileLocation> output = new ArrayList<MobileLocation>();

        final String userRegion = getUserRegion();

        for (MobileLocation mobileLocation : allImage) {
            final String name = mobileLocation.getProject().getProvinceID().getRegionID().getName();
            Boolean canEdit = checkRight(userRegion, name);
            if (canEdit) output.add(mobileLocation);
        }
        return output;
    }

    @RequestMapping(value = "image/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String commitChangeImage(@PathVariable("id") Long id) {
        mobileService.commitChangePhoto(id);
        return "success";
    }

    @RequestMapping(value = "image/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String discardChangeImage(@PathVariable("id") Long id) {
        mobileService.discardChangePhoto(id);
        return "success";
    }

    @RequestMapping(value = "location/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String commitChangeLocation(@PathVariable("id") Long id) {
        mobileService.commitChangeLocation(id);
        return "success";
    }

    @RequestMapping(value = "location/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String discardChangeLocation(@PathVariable("id") Long id) {
        mobileService.discardChangeLocation(id);
        return "success";
    }

    private Boolean checkRight(String region, String name) {
        if (region.equals("0") || region.equals("_0")) {
            return true;
        }

        if (name.equals("หลายพื้นที่ดำเนินการ") || name.equals("สปป.ลาว")) {
            return true;
        }
        if (name.equals("ภาคกลาง")) {
            if (region.equals("1")) return true;
        }
        if (name.equals("ภาคตะวันออกเฉียงเหนือ")) {
            if (region.equals("2")) return true;
        }

        if (name.equals("ภาคเหนือ")) {
            if (region.equals("3")) return true;
        }
        if (name.equals("ภาคใต้")) {
            if (region.equals("4")) return true;
        }
        return false;
    }

    private String getUserRegion() {
        final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        String region = "0";

        for (GrantedAuthority authority : authorities) {
            final String authority1 = authority.getAuthority();
            if (authority1.startsWith("ROLE_REGION_")) {
                region = authority1.substring(12);
            }
        }
        return region;
    }

}
