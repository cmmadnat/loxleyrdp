package com.loxley.rdpmap.web;

import com.loxley.rdpmap.domain.old.Department;
import com.loxley.rdpmap.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by cmmad_000 on 3/28/2015.
 */
@RestController
@RequestMapping("/admin/department")
public class DepartmentRestController {

    @Autowired
    DepartmentService departmentService;

    @RequestMapping("search")
    public List<Department> departmentList(@RequestParam("query") String query) {
        return departmentService.search(query);
    }
}
