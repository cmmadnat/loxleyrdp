package com.loxley.rdpmap.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin/report")
public class ReportController {

    @RequestMapping("1")
    public String ideaRoyal() {
        return "report1";
    }

    @RequestMapping("2")
    public String idea() {
        return "report2";
    }

    @RequestMapping("3")
    public String projectRegion() {
        return "report3";
    }

    @RequestMapping("4/mix")
    public String projectProvinceMix() {
        return "report4mix";
    }

    @RequestMapping("4/north")
    public String projectProvinceNorth() {
        return "report4north";
    }

    @RequestMapping("4/south")
    public String projectProvinceSouth() {
        return "report4south";
    }

    @RequestMapping("4/northeast")
    public String projectProvinceNortheast() {
        return "report4northeast";
    }

    @RequestMapping("4/central")
    public String projectProvinceCentral() {
        return "report4central";
    }

    @RequestMapping("5")
    public String projectStatus() {
        return "report5";
    }

    @RequestMapping("6")
    public String projectMinistry() {
        return "report6";
    }

    @RequestMapping("7")
    public String project() {
        return "report7";
    }

    @RequestMapping("8")
    public String kmRoyal() {
        return "report8";
    }

    @RequestMapping("9")
    public String km() {
        return "report9";
    }

    @RequestMapping("10/{region}")
    public String provinceStatus(Model model, @PathVariable("region") String region){
        model.addAttribute("name", region);
        return "report10";
    }
}
