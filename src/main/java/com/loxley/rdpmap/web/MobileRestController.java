package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.RoyalProject;
import com.loxley.rdpmap.domain.old.*;
import com.loxley.rdpmap.service.*;
import com.loxley.rdpreport.model.report.ProjectProvinceReport;
import com.loxley.rdpreport.model.report.ProjectRegionReport;
import com.loxley.rdpreport.model.report.ProjectRegionTypeReport;
import com.loxley.rdpreport.service.PDFDetailService;
import com.loxley.rdpreport.service.ProjectReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mobile")
public class MobileRestController {
    @Autowired
    RoyalProjectService royalProjectService;
    @Autowired
    ProjectService projectService;
    @Autowired
    IdeaService ideaService;
    @Autowired
    KnowledgeService knowledgeService;


    @Autowired
    MobileService mobileService;
    @Autowired
    LocationService locationService;
    @Autowired
    RoyaltyService royaltyService;

    @Autowired
    ProjectReportService projectReportService;
    @Autowired
    PDFDetailService pdfDetailService;
    private List<ProjectRegionReport> reportByRegion;
    private List<ProjectProvinceReport> reportByProvince;
    private List<ProjectRegionTypeReport> reportByRegionAndType;
    private int refreshCounter = 0;

    @RequestMapping(value = "project/{id}/display", produces = "text/html;charset=UTF-8")
    public String projectDetail(@PathVariable("id") Long id) {
        final Project one = projectService.findOne(id);
        return one.getDisplay();
    }

    @RequestMapping(value = "idea/{id}/display", produces = "text/html;charset=UTF-8")
    public String ideaDetail(@PathVariable("id") Long id) {
        final Idea one = ideaService.findOne(id);
        return one.getDisplay();
    }

    @RequestMapping(value = "knowledge/{id}/display", produces = "text/html;charset=UTF-8")
    public String knowledgeDetail(@PathVariable("id") Long id) {
        final Knowledge one = knowledgeService.findOne(id);
        return one.getDisplay();
    }


    @RequestMapping("projectType")
    public List<ProjectType> projectTypes() {
        return projectService.findAllType();
    }

    @RequestMapping("royalty")
    public List<Royalty> royalties() {
        return royaltyService.findAll();
    }

    @RequestMapping
    public List<RoyalProject> list(@RequestParam("page") int page,
                                   @RequestParam("limit") int limit) {
        List<RoyalProject> list = royalProjectService.findAll(page, limit);
        return list;
    }

    @RequestMapping(value = "search", method = RequestMethod.POST)
    public List<RoyalProject> search(@RequestParam("query") String query, @RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "limit", defaultValue = "10") int limit) {
        List<Project> search = projectService.search(query, page, limit);
        List<RoyalProject> result = royalProjectService.convert(search);
        return result;
    }

    @RequestMapping("{id}")
    public RoyalProject findOne(@PathVariable("id") Long id) {
        return royalProjectService.findOne(id);
    }

    @RequestMapping(value = "upload/{id}/location", method = RequestMethod.POST)
    public void setLocation(@RequestParam("lat") String lat, @RequestParam("lng") String lng,
                            @PathVariable("id") Long id, Principal principal) {
//        Project target = projectService.findOne(id);
//        target.setLocation(new GeoPt(Float.valueOf(lat), Float.valueOf(lng)));
//        projectService.update(id, target);
        mobileService.saveLocation(id, lat, lng, principal.getName());
    }

    @RequestMapping(value = "upload/{id}/photo", method = RequestMethod.GET)
    public String setPhoto(@PathVariable("id") Long id) {
        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();
        return service.createUploadUrl("/mobile/" + id + "/photo");
    }

    @RequestMapping(value = "{id}/photo", method = RequestMethod.POST)
    public void setPhoto(HttpServletRequest req, @PathVariable("id") Long id, Principal principal) {
        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();
        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        Map<String, List<BlobKey>> blobs = service.getUploads(req);
        List<BlobKey> blobKeys = blobs.get("photo");

        if (blobKeys == null || blobKeys.isEmpty()) {
        } else {
            ServingUrlOptions option = ServingUrlOptions.Builder
                    .withBlobKey(blobKeys.get(0));
            mobileService.saveImage(id, blobKeys.get(0).getKeyString(), imagesService.getServingUrl(option), principal.getName());
        }
    }

    @RequestMapping("region")
    public List<Region> findAllRegion() {
        return locationService.findAllRegion();
    }

    @RequestMapping("province")
    public List<Province> findAllProvince() {
        return locationService.findAllProvince();
    }

    @RequestMapping("regionReport")
    public List<ProjectRegionReport> regionReport() {
        if (reportByRegion == null)
            reportByRegion = projectReportService.reportByRegion();
        this.refreshCounter++;
        if (this.refreshCounter == 200) {
            reportByRegion = null;
            reportByProvince = null;
            reportByRegionAndType = null;
        }
        return reportByRegion;
    }

    @RequestMapping("provinceReport")
    public List<ProjectProvinceReport> provinceReport() {
        if (reportByProvince == null)
            reportByProvince = projectReportService.reportByProvince();
        return reportByProvince;
    }

    @RequestMapping("regionAndTypeReport")
    public List<ProjectRegionTypeReport> regionAndTypeReport() {
        if (reportByRegionAndType == null)
            reportByRegionAndType = projectReportService.reportByRegionAndType();
        return reportByRegionAndType;
    }
}