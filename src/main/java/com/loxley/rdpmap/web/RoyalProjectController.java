package com.loxley.rdpmap.web;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.*;
import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.domain.old.Province;
import com.loxley.rdpmap.domain.old.Region;
import com.loxley.rdpmap.domain.old.embedded.EProvince;
import com.loxley.rdpmap.service.*;
import com.loxley.rdpmap.util.ThaiDateFormat;
import org.apache.commons.beanutils.BeanUtils;
import org.joda.time.DateTime;
import org.joda.time.chrono.BuddhistChronology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("admin/royalproject")
public class RoyalProjectController extends HomeControllerBase{
    @Autowired
    ProjectService projectService;
    @Autowired
    LocationService locationService;
    @Autowired
    RoyaltyService royaltyService;
    @Autowired
    IdeaService ideaService;

    @Autowired
    LogService logService;

    BlobstoreService service = BlobstoreServiceFactory
            .getBlobstoreService();
    private Logger logger = LoggerFactory.getLogger(RoyalProjectController.class);


    @ModelAttribute
    public void addThisYear(Model model) {
        model.addAttribute("thisYear", DateTime.now().withChronology(BuddhistChronology.getInstance()).getYear());
        model.addAttribute("provinces", locationService.findAllProvince());
        model.addAttribute("type", projectService.findAllType());

        model.addAttribute("royalty", royaltyService.findAll());

    }

    @RequestMapping("json")
    @ResponseBody
    public List<Project> list(@RequestParam(defaultValue = "0", required = false) int page,
                              @RequestParam(defaultValue = "10", required = false) int limit) {
        final List<Project> all = projectService.findAll(page, limit);

        String region = getUserRegion();
        for (Project project : all) {
            projectService.checkRight(region, project);
        }


        return all;
    }

    private String getUserRegion() {
        final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        String region = "0";

        for (GrantedAuthority authority : authorities) {
            final String authority1 = authority.getAuthority();
            if (authority1.startsWith("ROLE_REGION_")) {
                region = authority1.substring(12);
            }
        }
        return region;
    }

    private void checkRight(String region, Project project) {
        final String name = project.getProvinceID().getRegionID().getName();
        if (region.equals("0") || region.equals("_0")) {
            project.setCanEdit(true);
            return;
        }

        if (name.equals("หลายพื้นที่ดำเนินการ") || name.equals("สปป.ลาว")) {
            project.setCanEdit(true);
            return;
        }
        if (name.equals("ภาคกลาง")) {
            if (region.equals("1")) project.setCanEdit(true);
            return;
        }
        if (name.equals("ภาคตะวันออกเฉียงเหนือ")) {
            if (region.equals("2")) project.setCanEdit(true);
            return;
        }

        if (name.equals("ภาคเหนือ")) {
            if (region.equals("3")) project.setCanEdit(true);
            return;
        }
        if (name.equals("ภาคใต้")) {
            if (region.equals("4")) project.setCanEdit(true);
            return;
        }
    }

    @RequestMapping(headers = "Accept=application/json", value = "count")
    @ResponseBody
    public int count() {
        return projectService.count();
    }

    @RequestMapping(value = "searchCount", headers = "Accept=application/json")
    @ResponseBody
    public long countSearch(@RequestParam("query") String query) {
        return projectService.countSearch(query);
    }

    @RequestMapping("search")
    @ResponseBody
    public List<Project> search(@RequestParam(defaultValue = "0", required = false) int page,
                                @RequestParam(defaultValue = "10", required = false) int limit,
                                @RequestParam("query") String query) {
        List<Project> all = projectService.search(query, page, limit);
        String region = getUserRegion();

        for (Project project : all) {
            projectService.checkRight(region, project);
        }
        return all;
    }

    @RequestMapping
    public String index(Model model, Principal principal) {
        logService.searchProject(principal);

        model.addAttribute("provinces", locationService.findAllProvince());
        model.addAttribute("types", projectService.findAllType());
        model.addAttribute("states", projectService.findAllState());
        return "royalProject";
    }

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String newRoyalProject(Model model) {

        List<Region> allRegion = locationService.findAllRegion();
        String uploadUrl = "/admin/royalproject/new";
        model.addAttribute("uploadUrl", uploadUrl);
        Project o = new Project();
        o.setCanEdit(true);
        model.addAttribute("target", o);
        model.addAttribute("topImage", new ArrayList<RoyalProjectImage>());
        model.addAttribute("states", projectService.getProjectStates());
        model.addAttribute("countries", locationService.findAllCountry());
        model.addAttribute("region", allRegion);

        return "royalProjectNew";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    @ResponseBody
    public String newRoyalProjectPost(Model model, @RequestParam(value = "lat", required = false) Float lat, @RequestParam(value = "lng", required = false) Float lng,
                                      @Validated Project project, BindingResult bindingResult, HttpServletRequest request, Principal principal, @RequestParam("country") String country) {
        if (lat != null && lng != null) {
            GeoPt latLng = new GeoPt(lat, lng);
            project.setLocation(latLng);
        }

        Long id = projectService.save(project);

//        processProject(id, project, request, project);

        project.setUuid(id);
        logService.createProject(project, principal);

        return "" + id;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public Project findOneJson(Model model, @PathVariable("id") Long id) {
        Project project = projectService.findOne(id);
        ArrayList<Idea> ideas = project.getIdeas();
        for (Idea idea : ideas) {
            idea.setThaiDate(ThaiDateFormat.formatDateShort(idea.getDate()));
        }
        return project;
    }


    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public String findOne(Model model, @PathVariable("id") Long id) {
        BlobstoreService service = BlobstoreServiceFactory
                .getBlobstoreService();
        String uploadUrl = "/admin/royalproject/" + id;

        Project target = projectService.findOne(id);
        List<Region> allRegion = locationService.findAllRegion();

        // can edit or not
        final String userRegion = getUserRegion();
        projectService.checkRight(userRegion, target);
        // end can edit

        if (target == null) return "projectDeleted";

        processLogoAndDate(target);

        model.addAttribute("region", allRegion);
        model.addAttribute("uploadUrl", uploadUrl);
        model.addAttribute("target", target);
        model.addAttribute("thisYear", DateTime.now().withChronology(BuddhistChronology.getInstance()).getYear());
        model.addAttribute("states", projectService.getProjectStates());
        if (target.getProvinceID() != null && target.getProvinceID().getUuid() == null) {
            target.setProvinceID(locationService.findOneProvinceByOldID(target.getProvinceID().getId()));
        }

        if (target.getProvinceID() != null) {
            try {
                model.addAttribute("amphurs", locationService.findAmphurByProvince(target.getProvinceID().getUuid()));
            } catch (NullPointerException e) {
                if (target.getProvinceID().getId() != null)
                    target.setProvinceID(locationService.findOneProvinceByOldID(target.getProvinceID().getId()));
                model.addAttribute("amphurs", locationService.findAmphurByProvince(target.getProvinceID().getUuid()));
            }
        } else model.addAttribute("amphurs", null);

        if (target.getAmphurID() != null) {
            try {
                model.addAttribute("tumbon", locationService.findTumbonByAmphur(target.getAmphurID().getUuid()));
            } catch (NullPointerException e) {
                if (target.getAmphurID().getId() != null)
                    target.setAmphurID(locationService.findOneAmphurByOldID(target.getAmphurID().getId()));
                model.addAttribute("tumbon", locationService.findTumbonByAmphur(target.getAmphurID().getUuid()));
            }
        } else model.addAttribute("tumbon", null);

        model.addAttribute("countries", locationService.findAllCountry());

        return "royalProjectNew";
    }



    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String update(@PathVariable("id") Long id,
                         @RequestParam(value = "lat", required = false) Float lat, @RequestParam(value = "lng", required = false) Float lng,
                         @Validated Project project, BindingResult bindingResult, HttpServletRequest request, Principal principal, @RequestParam("country") String country) {


        if (!country.equals("ไทย")) {
            Province provinceByName = locationService.findProvinceByName(country);
            EProvince eProvince = new EProvince();
            try {
                BeanUtils.copyProperties(eProvince, provinceByName);
            } catch (IllegalAccessException e) {

            } catch (InvocationTargetException e) {

            }
            project.setProvinceID(eProvince);
        }

        Project one = projectService.findOne(id);
        if (lat != null && lng != null) {
            GeoPt latLng = new GeoPt(lat, lng);
            one.setLocation(latLng);
        }

        processProject(id, project, request, one);

        logService.updateProject(project, principal);
        return "redirect:/admin#admin/royalproject/" + id;
    }






    @RequestMapping(value = "gallery/{id}/delete", method = RequestMethod.POST)
    public String galleryDelete(@PathVariable("id") Long id, Model model, HttpServletRequest request,
                                @RequestParam("deleteIndex") int index) {

        Project one = projectService.findOne(id);

        RoyalProjectImage target = one.getImages().get(index);

        if (target.getKey() != null)
            try {
                service.delete(new BlobKey(target.getKey()));
            } catch (Exception e) {

            }
        one.getImages().remove(index);

        projectService.update(id, one);

        return "redirect:/admin/royalproject/" + id + "/uploadPhoto";
    }


    @RequestMapping("delete")
    public String delete(@RequestParam("id") Long id, Principal pricipal, @RequestParam(value = "close", required = false) Boolean close) {
        Project project = projectService.findOne(id);

        if (project == null) return "projectDeleted";


        projectService.delete(project);
        logService.deleteProject(project, pricipal);
        if (close != null)
            return "closeTab";
        else return "redirect:/admin#/admin/royalproject";
    }

    @RequestMapping("download/{key}/{fileName}")
    public void download(@PathVariable("key") String key, HttpServletResponse response) throws IOException {
        BlobKey blobKey = new BlobKey(key);
        service.serve(blobKey, response);
    }

    @RequestMapping("{id}/deleteFile/file")
    public String deleteFile(@PathVariable("id") Long id, @RequestParam("deleteIndex") List<Integer> deleteIndex) {
        Project project = projectService.findOne(id);

        List<RoyalProjectFile> files = project.getFiles();

        ArrayList<RoyalProjectFile> deleteItem = new ArrayList<RoyalProjectFile>();
        for (Integer integer : deleteIndex) {
            RoyalProjectFile royalProjectFile = files.get(integer);
            deleteItem.add(royalProjectFile);
        }

        for (RoyalProjectFile royalProjectFile : deleteItem) {
            if (royalProjectFile.getKey() == null) continue;
            BlobKey key = new BlobKey(royalProjectFile.getKey());
            service.delete(key);
        }

        files.removeAll(deleteItem);
        project.setFiles(files);
        projectService.update(id, project);

        return "redirect:/admin/royalproject/" + id + "/uploadFile";
    }

    @RequestMapping(value = "linkIdea/{id}", method = RequestMethod.GET)
    public String linkIdea(@PathVariable("id") Long id, Model model) {
        Project target = projectService.findOne(id);
        model.addAttribute("target", target);
        return "ideaLink";
    }

    @RequestMapping(value = "linkIdea/{id}", method = RequestMethod.POST)
    public String linkIdea(@PathVariable("id") Long id, @RequestParam("ideaID") Long ideaId) {
        Project one = projectService.findOne(id);
        Idea idea = ideaService.findOne(ideaId);

        ArrayList<Idea> ideas = one.getIdeas();
//        if (!ideas.contains(idea))
//            ideas.add(idea);
        boolean found = false;
        for (Idea idea1 : ideas) {
            if (idea1.getUuid().equals(ideaId)) {
                found = true;
                break;
            }
        }
        if (!found) {
            ideas.add(idea);
            one.setIdeas(ideas);

        }

        projectService.update(id, one);
        return "redirect:/admin#/admin/royalproject/" + id;
    }


    @RequestMapping(value = "linkIdea/delete/{id}", method = RequestMethod.POST)
    public String linkIdeaDelete(@PathVariable("id") Long id, @RequestParam("ideaID") Long ideaId) {
        Project one = projectService.findOne(id);

        ArrayList<Idea> ideas = one.getIdeas();
        ArrayList<Idea> deleteItem = new ArrayList<Idea>();
        int counter = 0;
        for (Idea idea1 : ideas) {
            if (idea1.getUuid().equals(ideaId)) {
                break;
            }
            counter++;
        }

        ideas.remove(counter);

        one.setIdeas(ideas);

        projectService.update(id, one);
        return "redirect:/admin#/admin/royalproject/" + id;
    }

    @RequestMapping(value = "{id}/uploadPhoto", method = RequestMethod.GET)
    public String uploadPhoto(@PathVariable("id") Long id, Model model) {
        Project project = projectService.findOne(id);
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

        final String gsBucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName()
                + "/projectPhoto";
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(gsBucketName);
        model.addAttribute("uploadURL", blobstoreService.createUploadUrl("/admin/royalproject/" + id + "/uploadPhoto", option));
        model.addAttribute("target", project);
        model.addAttribute("type", "royalproject");
        model.addAttribute("typeName", "โครงการ");

        final String userRegion = getUserRegion();
        projectService.checkRight(userRegion, project);
        model.addAttribute("canEdit", project.isCanEdit());

        return "partial/uploadPhoto";
    }

    @RequestMapping(value = "{id}/uploadFile", method = RequestMethod.GET)
    public String uploadFile(@PathVariable("id") Long id, Model model) {
        Project project = projectService.findOne(id);
        processLogoAndDate(project);
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

        final String gsBucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName()
                + "/projectFile";
        UploadOptions option = UploadOptions.Builder.withGoogleStorageBucketName(gsBucketName);
        model.addAttribute("uploadURL", blobstoreService.createUploadUrl("/admin/royalproject/" + id + "/uploadFile", option));
        model.addAttribute("target", project);
        model.addAttribute("type", "royalproject");
        model.addAttribute("typeName", "โครงการ");

        final String userRegion = getUserRegion();
        projectService.checkRight(userRegion, project);
        model.addAttribute("canEdit", project.isCanEdit());

        return "partial/uploadFile";
    }

    @RequestMapping(value = "{id}/uploadPhoto", method = RequestMethod.POST)
    public String uploadPhotoPost(HttpServletRequest request, @PathVariable("id") Long id) {
        Project project = projectService.findOne(id);

        processProjectWithFile(id, project, request, project, null);

        return "redirect:/admin/royalproject/" + id + "/uploadPhoto";
    }

    @RequestMapping(value = "{id}/uploadFile", method = RequestMethod.POST)
    public String uploadFilePost(HttpServletRequest request, @PathVariable("id") Long id, @RequestParam("fileName") String fileName) {
        Project project = projectService.findOne(id);

        processProjectWithFile(id, project, request, project, fileName);

        return "redirect:/admin/royalproject/" + id + "/uploadFile";
    }

    @RequestMapping(value = "gallery/{id}/caption", method = RequestMethod.POST)
    public String galleryCaption(@PathVariable("id") Long id, Model model, HttpServletRequest request,
                                 @RequestParam("captionIndex") int index, @RequestParam("caption") String caption) {

        Project one = projectService.findOne(id);

        RoyalProjectImage target = one.getImages().get(index);

        target.setCaption(caption);

        projectService.update(id, one);

        return "redirect:/admin/royalproject/" + id + "/uploadPhoto";
    }

}
