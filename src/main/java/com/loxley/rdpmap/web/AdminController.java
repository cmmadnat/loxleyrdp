package com.loxley.rdpmap.web;

import com.loxley.rdpmap.service.MobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;

@Controller
@RequestMapping("/admin")
public class AdminController {
    /**
     * Simply selects the home view to render by returning its name.
     */
    @Autowired
    MobileService mobileService;
    @Value("${version}")
    String version;

    @RequestMapping(method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
        model.addAttribute("version", version);
        model.addAttribute("countApprove", mobileService.countAwaitApprove());
        return "index";
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String search(@RequestParam("param") String query) {
        return "search";
    }
}
