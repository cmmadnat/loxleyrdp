package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import com.loxley.rdpmap.service.SettingService;
import com.loxley.rdpmap.ui.Slider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by cmmad_000 on 3/11/2015.
 */
@Controller
@RequestMapping("/admin/other")
public class OtherController {

    @Autowired
    SettingService settingService;

    Logger logger = LoggerFactory.getLogger(OtherController.class);


    @RequestMapping
    public String index(Model model) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        model.addAttribute("slideUpload", blobstoreService.createUploadUrl("/admin/other/slider"));
        model.addAttribute("slider", new Slider());
        return "other";
    }

    @RequestMapping(value = "slider", method = RequestMethod.GET)
    @ResponseBody
    public List<Slider> sliderList() {
        return settingService.getSlider();
    }

    @RequestMapping(value = "slider", method = RequestMethod.POST)
    public String sliderListPost(HttpServletRequest request, Slider slider) {
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        ImagesService imagesService = ImagesServiceFactory.getImagesService();

        List<BlobKey> files = blobstoreService.getUploads(request).get("file");
        List<BlobInfo> blobInfos = blobstoreService.getBlobInfos(request).get("file");

        if (files != null && files.size() != 0 && blobInfos.get(0).getSize() != 0) {
            ServingUrlOptions option = ServingUrlOptions.Builder.withBlobKey(new BlobKey(files.get(0).getKeyString()));
            String servingUrl = imagesService.getServingUrl(option);
            slider.setImgURL(servingUrl);
            slider.setKey(files.get(0).getKeyString());
        }

        settingService.putSlider(Lists.newArrayList(slider));
        return "redirect:/admin#/admin/other";
    }

    @RequestMapping("slider/delete/{index}")
    public String deleteSlider(@PathVariable("index") Integer index) {
        settingService.deleteSlider(index);
        return "redirect:/admin#/admin/other";
    }
}
