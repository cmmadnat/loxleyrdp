package com.loxley.rdpmap.web;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.service.IdeaService;
import com.loxley.rdpmap.service.KnowledgeService;
import com.loxley.rdpmap.service.LocationService;
import com.loxley.rdpmap.service.ProjectService;
import com.loxley.rdpmap.ui.Slider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by cmmad_000 on 3/4/2015.
 */
@Controller
@RequestMapping("admin/uploadJSON")
public class UploadJsonController {
    @Autowired
    ProjectService projectService;
    @Autowired
    IdeaService ideaService;
    @Autowired
    LocationService locationService;
    @Autowired
    private KnowledgeService knowledgeService;


    private final BlobstoreService blobService;

    public UploadJsonController() {
        blobService = BlobstoreServiceFactory.getBlobstoreService();
    }

    @RequestMapping
    public String index(Model model) {
//        String uploadProject = blobService.createUploadUrl("/admin/uploadJSON/project");
//        String uploadIdea = blobService.createUploadUrl("/admin/uploadJSON/idea");

        model.addAttribute("uploadProject", "/admin/uploadJSON/project");
        model.addAttribute("uploadIdea", "/admin/uploadJSON/idea");
        model.addAttribute("uploadKnowledge", "/admin/uploadJSON/knowledge");
        return "jsonUpload";
    }

    @RequestMapping("wipeProject")
    public String wipeProject() {
        projectService.wipe();
//        List<Project> all = projectService.findAll(0, 1000);
//        for (Project project : all) {
//            projectService.delete(project);
//        }
        return "redirect:/admin";
    }

    @RequestMapping("wipeIdea")
    public String wipeIdea() {

        ideaService.wipe();
        return "redirect:/admin";
    }

    @RequestMapping("updateProjectIndex")
    public String updateProject() {
        projectService.updateIndex();
        return "redirect:/admin#/admin/uploadJSON";
    }

    @RequestMapping("updateIdeaIndex")
    public String updateIdea() {
        ideaService.updateIndex();
        return "redirect:/admin#/admin/uploadJSON";

    }

    @RequestMapping("updateKnowledgeIndex")
    public String uploadKnowledge() {
        knowledgeService.updateIndex();
        return "redirect:/admin#/admin/uploadJSON";
    }

    @RequestMapping("project")
    public String uploadProject(HttpServletRequest request,
                                @RequestParam("page") Integer page) throws IOException {
        for (int i = 0; i < page; i++) {
            Queue queue = QueueFactory.getDefaultQueue();
            queue.add(TaskOptions.Builder.withUrl("/import/project/" + i));
        }
//        System.out.println(page);
//        Map<String, List<BlobKey>> uploads = blobService.getUploads(request);
//        BlobKey key = uploads.get("file").get(0);
//
//        byte[] bytes = blobService.fetchData(key, 0, BlobstoreService.MAX_BLOB_FETCH_SIZE - 1);
//        ObjectMapper mapper = new ObjectMapper();
//        List<Project> values = mapper.readValue(bytes, new TypeReference<List<Project>>() {
//        });
//        projectService.saveProjects(values);

        //        delete the file after use
//        blobService.delete(key);

        return "redirect:/admin";
    }

    @RequestMapping("idea")
    public String uploadIdea(HttpServletRequest request,
                             @RequestParam("page") Integer page) throws IOException {
//        System.out.println(page);
        for (int i = 0; i < page; i++) {
            Queue queue = QueueFactory.getDefaultQueue();
            queue.add(TaskOptions.Builder.withUrl("/import/idea/" + i));
        }
//        Map<String, List<BlobKey>> uploads = blobService.getUploads(request);
//        BlobKey key = uploads.get("file").get(0);
//
//        byte[] bytes = blobService.fetchData(key, 0, BlobstoreService.MAX_BLOB_FETCH_SIZE - 1);
//        ObjectMapper mapper = new ObjectMapper();
//        List<Idea> values = mapper.readValue(bytes, new TypeReference<List<Idea>>() {
//        });
//        ideaService.saveIdeas(values);

        //        delete the file after use
//        blobService.delete(key);

        return "redirect:/admin";
    }

    @RequestMapping("knowledge")
    public String uploadKnowledge(HttpServletRequest request,
                                  @RequestParam("page") Integer page) throws IOException {
//        System.out.println(page);
        for (int i = 0; i < page; i++) {
            Queue queue = QueueFactory.getDefaultQueue();
            queue.add(TaskOptions.Builder.withUrl("/import/knowledge/" + i));
        }
//        Map<String, List<BlobKey>> uploads = blobService.getUploads(request);
//        BlobKey key = uploads.get("file").get(0);
//
//        byte[] bytes = blobService.fetchData(key, 0, BlobstoreService.MAX_BLOB_FETCH_SIZE - 1);
//        ObjectMapper mapper = new ObjectMapper();
//        List<Idea> values = mapper.readValue(bytes, new TypeReference<List<Idea>>() {
//        });
//        ideaService.saveIdeas(values);

        //        delete the file after use
//        blobService.delete(key);

        return "redirect:/admin";
    }

    @RequestMapping("updateTumbon")
    public String updateTumbon() {
        locationService.updateTumbon();
        return "redirect:/admin";
    }

}
