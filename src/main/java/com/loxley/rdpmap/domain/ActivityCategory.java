package com.loxley.rdpmap.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class ActivityCategory {
	@Id
	Long id;
	String name;
}
