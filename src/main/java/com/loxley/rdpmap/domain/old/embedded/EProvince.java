/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.EGeoPt;

import java.io.Serializable;

/**
 *
 * @author cmmad_000
 */
@JsonIgnoreProperties({"location"})
public class EProvince implements Serializable {
	private static final long serialVersionUID = 1L;
    Long uuid;
    @Index
	private Integer id;
	private int code;
	private String name;
	private ERegion regionID;

    boolean isCountry = false;

    GeoPt location = new GeoPt(13.76864f, 100.494501f);


    public GeoPt getLocation() {
        return location;
    }



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ERegion getRegionID() {
        return regionID;
    }

    public void setRegionID(ERegion regionID) {
        this.regionID = regionID;
    }

    @Override
    public String toString() {
        if (uuid == null) return "";
        return  uuid.toString();
    }

    public boolean getIsCountry() {
        return isCountry;
    }

    public void setIsCountry(boolean isCountry) {
        this.isCountry = isCountry;
    }

    public void setLocation(GeoPt location) {
        this.location = location;
    }
}
