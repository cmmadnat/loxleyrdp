/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.old.embedded.EMinistry;

import java.io.Serializable;

/**
 *
 * @author cmmad_000
 */
@JsonIgnoreProperties({ "projectDepartmentCollection", "projectCollection" })
@Entity
public class Department implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    Long uuid;

    @Index
	private Integer id;
	private String code;
    @Index
	private String name;
	private EMinistry ministryID;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EMinistry getMinistryID() {
        return ministryID;
    }

    public void setMinistryID(EMinistry ministryID) {
        this.ministryID = ministryID;
    }

    @Override
    public String toString() {
        return uuid.toString();
    }
}
