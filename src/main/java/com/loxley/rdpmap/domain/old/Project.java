/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.*;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.embedded.*;
import org.joda.time.DateTime;
import org.joda.time.chrono.BuddhistChronology;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author cmmad_000
 */
@JsonIgnoreProperties({"projectDepartmentCollection", "projectTagCollection",
        "projectIdeaCollection"})
@Entity
@Cache
public class Project implements Serializable {

    @Ignore
    boolean canEdit = false;

    // thought : 1, deeka : 2, department : 3
    int source = 1;

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    Boolean hasTracker = false;

    public Boolean getHasTracker() {
        return hasTracker;
    }

    public void setHasTracker(Boolean hasTracker) {
        this.hasTracker = hasTracker;
    }

    //    for ui
    @Ignore
    String projectTypeLogo;
    @Ignore
    String url;
    @Ignore
    String startDateThaiDate;

    public String getProjectTypeLogo() {
        return projectTypeLogo;
    }

    public void setProjectTypeLogo(String projectTypeLogo) {
        this.projectTypeLogo = projectTypeLogo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStartDateThaiDate() {
        return startDateThaiDate;
    }

    public void setStartDateThaiDate(String startDateThaiDate) {
        this.startDateThaiDate = startDateThaiDate;
    }
    //    end for ui

    @Index
    List<Ref<Idea>> ideas = new ArrayList<Ref<Idea>>();

    public ArrayList<Idea> getIdeas() {

        ArrayList<Idea> target = new ArrayList<Idea>();
        for (Ref<Idea> idea : ideas) {
            Idea obj = idea.get();
            if (obj != null)
                target.add(obj);
        }
        return target;
    }

    public void setIdeas(ArrayList<Idea> ideas) {
        ArrayList<Ref<Idea>> target = new ArrayList<Ref<Idea>>();

        for (Idea idea : ideas) {
            target.add(Ref.create(idea));
        }

        this.ideas = target;
    }

    public Ref<Project> mainProject;
    boolean continuous = false;


    @DateTimeFormat(pattern = "dd-MM-yyyy")
    Date startDate;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    Date finishDate;

    int totalMonth, totalYear = 1;

    String result;

    List<EDepartment> departments = new ArrayList<EDepartment>();

    List<RoyalProjectFile> files = new ArrayList<RoyalProjectFile>();

    public List<RoyalProjectFile> getFiles() {
        return files;
    }

    public void setFiles(List<RoyalProjectFile> files) {
        this.files = files;
    }

    public List<EDepartment> getDepartments() {
        return departments;
    }

    public void setDepartments(List<EDepartment> departments) {
        this.departments = departments;
    }

    public int getTotalYear() {
        return totalYear;
    }

    public void setTotalYear(int totalYear) {
        this.totalYear = totalYear;
    }

    public int getTotalMonth() {
        return totalMonth;
    }

    public void setTotalMonth(int totalMonth) {
        this.totalMonth = totalMonth;
    }


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Project getMainProject() {
        if (mainProject != null)
            return mainProject.get();
        else return null;
    }

    public void setMainProject(Project mainProject) {
        if (mainProject == null) {
            this.mainProject = null;
            return;
        }
        this.mainProject = Ref.create(mainProject);
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    // old entity


    @Id
    Long uuid;
    GeoPt location = new GeoPt(0, 0);

    private List<RoyalProjectImage> images = new ArrayList<RoyalProjectImage>();
    private Integer id;
    private String code;
    private String name;
    private String address;
    private Integer areaRainy;
    private Integer areaSummer;
    private Integer village;
    private Integer household;
    private Integer citizen;
    private String villageName;
    private String detail;
    @Index
    private short year = (short) DateTime.now().withChronology(BuddhistChronology.getInstance()).getYear();
    private String display = "";
    private String imgCover;
    private String imgFront;
    @Index
    private Date time = new Date();
    private boolean active;
    private List<EProjectDepartment> projectDepartmentCollection = new ArrayList<EProjectDepartment>();
    private List<EProjectTag> projectTagCollection = new ArrayList<EProjectTag>();
    private List<EProjectIdea> projectIdeaCollection = new ArrayList<EProjectIdea>();
    private EAmphur amphurID;
    private EDepartment departmentID;
    private EDistrict districtID;
    private EProjectState stateID;
    private EProjectType typeID;
    private EProvince provinceID;
    private ERoyalty royaltyID;


    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public GeoPt getLocation() {
        return location;
    }

    public void setLocation(GeoPt location) {
        this.location = location;
    }

    public List<RoyalProjectImage> getImages() {
        return images;
    }

    public void setImages(List<RoyalProjectImage> images) {
        this.images = images;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAreaRainy() {
        return areaRainy;
    }

    public void setAreaRainy(Integer areaRainy) {
        this.areaRainy = areaRainy;
    }

    public Integer getAreaSummer() {
        return areaSummer;
    }

    public void setAreaSummer(Integer areaSummer) {
        this.areaSummer = areaSummer;
    }

    public Integer getVillage() {
        return village;
    }

    public void setVillage(Integer village) {
        this.village = village;
    }

    public Integer getHousehold() {
        return household;
    }

    public void setHousehold(Integer household) {
        this.household = household;
    }

    public Integer getCitizen() {
        return citizen;
    }

    public void setCitizen(Integer citizen) {
        this.citizen = citizen;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImgCover() {
        return imgCover;
    }

    public void setImgCover(String imgCover) {
        this.imgCover = imgCover;
    }

    public String getImgFront() {
        return imgFront;
    }

    public void setImgFront(String imgFront) {
        this.imgFront = imgFront;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<EProjectDepartment> getProjectDepartmentCollection() {
        return projectDepartmentCollection;
    }

    public void setProjectDepartmentCollection(List<EProjectDepartment> projectDepartmentCollection) {
        this.projectDepartmentCollection = projectDepartmentCollection;
    }

    public List<EProjectTag> getProjectTagCollection() {
        return projectTagCollection;
    }

    public void setProjectTagCollection(List<EProjectTag> projectTagCollection) {
        this.projectTagCollection = projectTagCollection;
    }

    public List<EProjectIdea> getProjectIdeaCollection() {
        return projectIdeaCollection;
    }

    public void setProjectIdeaCollection(List<EProjectIdea> projectIdeaCollection) {
        this.projectIdeaCollection = projectIdeaCollection;
    }

    public EAmphur getAmphurID() {
        return amphurID;
    }

    public void setAmphurID(EAmphur amphurID) {
        this.amphurID = amphurID;
    }

    public EDepartment getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(EDepartment departmentID) {
        this.departmentID = departmentID;
    }

    public EDistrict getDistrictID() {
        return districtID;
    }

    public void setDistrictID(EDistrict districtID) {
        this.districtID = districtID;
    }

    public EProjectState getStateID() {
        return stateID;
    }

    public void setStateID(EProjectState stateID) {
        this.stateID = stateID;
    }

    public EProjectType getTypeID() {
        return typeID;
    }

    public void setTypeID(EProjectType typeID) {
        this.typeID = typeID;
    }

    public EProvince getProvinceID() {
        return provinceID;
    }

    public void setProvinceID(EProvince provinceID) {
        this.provinceID = provinceID;
    }

    public ERoyalty getRoyaltyID() {
        return royaltyID;
    }

    public void setRoyaltyID(ERoyalty royaltyID) {
        this.royaltyID = royaltyID;
    }

    @Override
    public String toString() {

        return uuid.toString();
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }
}
