/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.embedded.EKnowledgeType;
import com.loxley.rdpmap.domain.old.embedded.ERoyalty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author cmmad_000
 */
@JsonIgnoreProperties({"knowledgeTagCollection", "knowledgeIdeaCollection"})
@Entity
public class Knowledge implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    Long uuid;

    @Ignore
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    List<RoyalProjectFile> files = new ArrayList<RoyalProjectFile>();
    List<RoyalProjectImage> images = new ArrayList<RoyalProjectImage>();


    private Integer id;
    private String name;
    private String detail;
    private String display;
    private String imgCover;
    private String imgFront;
    @Index
    private Date time = new Date();
    private boolean active;
    private EKnowledgeType typeID;
    private ERoyalty royaltyID;
//	private Collection<EKnowledgeTag> knowledgeTagCollection;
//	private Collection<EKnowledgeIdea> knowledgeIdeaCollection;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public List<RoyalProjectFile> getFiles() {
        return files;
    }

    public void setFiles(List<RoyalProjectFile> files) {
        this.files = files;
    }

    public List<RoyalProjectImage> getImages() {
        return images;
    }

    public void setImages(List<RoyalProjectImage> images) {
        this.images = images;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImgCover() {
        return imgCover;
    }

    public void setImgCover(String imgCover) {
        this.imgCover = imgCover;
    }

    public String getImgFront() {
        return imgFront;
    }

    public void setImgFront(String imgFront) {
        this.imgFront = imgFront;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public EKnowledgeType getTypeID() {
        return typeID;
    }

    public void setTypeID(EKnowledgeType typeID) {
        this.typeID = typeID;
    }

    public ERoyalty getRoyaltyID() {
        return royaltyID;
    }

    public void setRoyaltyID(ERoyalty royaltyID) {
        this.royaltyID = royaltyID;
    }
}