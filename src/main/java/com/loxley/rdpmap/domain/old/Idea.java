/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.RoyalProjectFile;
import com.loxley.rdpmap.domain.RoyalProjectImage;
import com.loxley.rdpmap.domain.old.embedded.EProvince;
import com.loxley.rdpmap.domain.old.embedded.ERoyalty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author cmmad_000
 */
@JsonIgnoreProperties({"ideaTagCollection", "projectIdeaCollection",
        "knowledgeIdeaCollection"})
@Entity
public class Idea implements Serializable {
    private static final long serialVersionUID = 1L;

    @Ignore
    String url;

    List<RoyalProjectFile> files = new ArrayList<RoyalProjectFile>();
    List<RoyalProjectImage> images = new ArrayList<RoyalProjectImage>();

    // thought : 1, deeka : 2, department : 3
    int source = 1;

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public List<RoyalProjectFile> getFiles() {
        return files;
    }

    public void setFiles(List<RoyalProjectFile> files) {
        this.files = files;
    }

    public List<RoyalProjectImage> getImages() {
        return images;
    }

    public void setImages(List<RoyalProjectImage> images) {
        this.images = images;
    }

    @Ignore
    String thaiDate;

    public String getThaiDate() {
        return thaiDate;
    }

    public void setThaiDate(String thaiDate) {
        this.thaiDate = thaiDate;
    }

    @Id
    Long uuid;

    private Integer id;
    private Short day;
    private Short month;
    private Short year = Short.valueOf(DateTime.now().plusYears(543).getYear() + "");
    @Index
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date date = new Date();
    private String address;
    private String detail;
    private String display;
    private String imgCover;
    private String imgFront;
    @Index
    private Date time;
    private boolean active;
    private EProvince provinceID;
    private ERoyalty royaltyID;

    public ERoyalty getRoyaltyID() {
        return royaltyID;
    }

    public void setRoyaltyID(ERoyalty royaltyID) {
        this.royaltyID = royaltyID;
    }

    public EProvince getProvinceID() {
        return provinceID;
    }

    public void setProvinceID(EProvince provinceID) {
        this.provinceID = provinceID;
    }

    public Idea() {
    }

    public Idea(Integer id) {
        this.id = id;
    }

    public Idea(Integer id, Date time, boolean active) {
        this.id = id;
        this.time = time;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getDay() {
        return day;
    }

    public void setDay(Short day) {
        this.day = day;
    }

    public Short getMonth() {
        return month;
    }

    public void setMonth(Short month) {
        this.month = month;
    }

    public Short getYear() {
        return year;
    }

    public void setYear(Short year) {
        this.year = year;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImgCover() {
        return imgCover;
    }

    public void setImgCover(String imgCover) {
        this.imgCover = imgCover;
    }

    public String getImgFront() {
        return imgFront;
    }

    public void setImgFront(String imgFront) {
        this.imgFront = imgFront;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof Idea)) {
            return false;
        }
        Idea other = (Idea) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.Idea[ id=" + id + " ]";
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
