/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.io.Serializable;

/**
 *
 * @author cmmad_000
 */
@Entity
public class IdeaTag implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    Long uuid;

    private Integer id;
    private Idea ideaID;
    private Tag tagID;

    public IdeaTag() {
    }

    public IdeaTag(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Idea getIdeaID() {
        return ideaID;
    }

    public void setIdeaID(Idea ideaID) {
        this.ideaID = ideaID;
    }

    public Tag getTagID() {
        return tagID;
    }

    public void setTagID(Tag tagID) {
        this.tagID = tagID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IdeaTag)) {
            return false;
        }
        IdeaTag other = (IdeaTag) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.IdeaTag[ id=" + id + " ]";
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }
}
