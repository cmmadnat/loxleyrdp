/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author cmmad_000
 */
@JsonIgnoreProperties({ "ideaTagCollection", "projectIdeaCollection",
		"knowledgeIdeaCollection" })
public class EIdea implements Serializable {
	private static final long serialVersionUID = 1L;
    Long uuid;

	private Integer id;
	private Short day;
	private Short month;
	private Short year;
	private Date date;
	private String address;
	private String detail;
	private String display;
	private String imgCover;
	private String imgFront;
	private Date time;
	private boolean active;
	private EProvince provinceID;
	private ERoyalty royaltyID;

	public EIdea() {
	}

	public EIdea(Integer id) {
		this.id = id;
	}

	public EIdea(Integer id, Date time, boolean active) {
		this.id = id;
		this.time = time;
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Short getDay() {
		return day;
	}

	public void setDay(Short day) {
		this.day = day;
	}

	public Short getMonth() {
		return month;
	}

	public void setMonth(Short month) {
		this.month = month;
	}

	public Short getYear() {
		return year;
	}

	public void setYear(Short year) {
		this.year = year;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getImgCover() {
		return imgCover;
	}

	public void setImgCover(String imgCover) {
		this.imgCover = imgCover;
	}

	public String getImgFront() {
		return imgFront;
	}

	public void setImgFront(String imgFront) {
		this.imgFront = imgFront;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public boolean isActive() {
        return active;
    }

    public EProvince getProvinceID() {
        return provinceID;
    }

    public void setProvinceID(EProvince provinceID) {
        this.provinceID = provinceID;
    }

    public ERoyalty getRoyaltyID() {
        return royaltyID;
    }

    public void setRoyaltyID(ERoyalty royaltyID) {
        this.royaltyID = royaltyID;
    }

    @Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof EIdea)) {
			return false;
		}
		EIdea other = (EIdea) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "javaapplication1.Idea[ id=" + id + " ]";
	}

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }
}
