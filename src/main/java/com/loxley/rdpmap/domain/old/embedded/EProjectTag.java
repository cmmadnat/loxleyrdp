/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

import java.io.Serializable;

/**
 *
 * @author cmmad_000
 */
public class EProjectTag implements Serializable {
    private static final long serialVersionUID = 1L;
    Long uuid;
    private Integer id;
    private ETag tagID;

    public EProjectTag() {
    }

    public EProjectTag(Integer id) {
        this.id = id;
    }

    public EProjectTag(Integer id, byte[] rowVersion) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ETag getTagID() {
        return tagID;
    }

    public void setTagID(ETag tagID) {
        this.tagID = tagID;
    }

    public static long getSerialVersionUID() {

        return serialVersionUID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EProjectTag)) {
            return false;
        }
        EProjectTag other = (EProjectTag) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.ProjectTag[ id=" + id + " ]";
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }
}
