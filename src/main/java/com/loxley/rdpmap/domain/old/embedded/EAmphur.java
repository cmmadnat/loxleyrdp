/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

/**
 * @author cmmad_000
 */
public class EAmphur implements Serializable {
    private static final long serialVersionUID = 1L;
    Long uuid;
    @Index
    private Integer id;
    private int code;
    private String name;

    private EProvince provinceID;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EProvince getProvinceID() {
        return provinceID;
    }

    public void setProvinceID(EProvince provinceID) {
        this.provinceID = provinceID;
    }

    @Override
    public String toString() {
        if (uuid == null) return "";
        return uuid.toString();
    }
}
