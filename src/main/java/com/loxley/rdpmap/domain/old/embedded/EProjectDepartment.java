/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

/**
 * @author cmmad_000
 */
public class EProjectDepartment {
    Long uuid;
    private Integer id;
    private EDepartment departmentID;


    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EDepartment getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(EDepartment departmentID) {
        this.departmentID = departmentID;
    }
}
