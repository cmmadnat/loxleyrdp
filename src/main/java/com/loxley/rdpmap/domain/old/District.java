/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.old.embedded.EAmphur;

import java.io.Serializable;

/**
 * @author cmmad_000
 */
@JsonIgnoreProperties({"projectCollection"})
@Entity
@Cache
public class District implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    Long uuid;
    @Index
    private Integer id;
    private int code;
    @Index
    private String name;
    private EAmphur amphurID;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EAmphur getAmphurID() {
        return amphurID;
    }

    public void setAmphurID(EAmphur amphurID) {
        this.amphurID = amphurID;
    }

    @Override
    public String toString() {

        return uuid.toString();
    }
}
