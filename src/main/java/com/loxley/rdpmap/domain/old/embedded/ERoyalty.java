/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

import java.io.Serializable;

/**
 * @author cmmad_000
 */
public class ERoyalty implements Serializable {
    private static final long serialVersionUID = 1L;
    Long uuid;

    private Integer id;
    private String code;
    private Short priority;
    private Boolean petition;
    private String name;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Short getPriority() {
        return priority;
    }

    public void setPriority(Short priority) {
        this.priority = priority;
    }

    public Boolean getPetition() {
        return petition;
    }

    public void setPetition(Boolean petition) {
        this.petition = petition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return uuid.toString();
    }
}
