/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loxley.rdpmap.domain.old.embedded;

import java.io.Serializable;

/**
 *
 * @author cmmad_000
 */
public class EProjectIdea implements Serializable {
    private static final long serialVersionUID = 1L;
    Long uuid;
    private Integer id;
    private String detail;
    private EIdea ideaID;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public EIdea getIdeaID() {
        return ideaID;
    }

    public void setIdeaID(EIdea ideaID) {
        this.ideaID = ideaID;
    }
}
