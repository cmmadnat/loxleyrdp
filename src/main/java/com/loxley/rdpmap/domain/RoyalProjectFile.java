package com.loxley.rdpmap.domain;

import com.googlecode.objectify.annotation.Ignore;

import java.io.Serializable;

public class RoyalProjectFile implements Serializable {
    private String fileName;
    String key;

    @Ignore
    String logo;
    @Ignore
    String color;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
