package com.loxley.rdpmap.domain;

/**
 * Created by cmmad_000 on 6/10/2015.
 */
public class EGeoPt {
    float lat, lng;

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
