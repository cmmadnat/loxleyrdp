package com.loxley.rdpmap.domain;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.old.Project;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by cmmad_000 on 5/10/2015.
 */
@Entity
public class ProjectTracker {
    @Ignore
    boolean canEdit;

    @Id
    Long uuid;

    @Index
    Ref<Project> project;

    String name;
    String status;

    String title, description, link;
    String youtube;
    String key;
    String imageURL;

    String author = "admin";

    Date postDate = new Date();
    @Index
    Date listDate = new Date();

    ArrayList<RoyalProjectFile> files = new ArrayList<RoyalProjectFile>();
    ArrayList<RoyalProjectImage> images = new ArrayList<RoyalProjectImage>();

    public ArrayList<RoyalProjectImage> getImages() {
        return images;
    }

    public void setImages(ArrayList<RoyalProjectImage> images) {
        this.images = images;
    }

    public ArrayList<RoyalProjectFile> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<RoyalProjectFile> files) {
        this.files = files;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Date getListDate() {
        return listDate;
    }

    public void setListDate(Date listDate) {
        this.listDate = listDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Project getProject() {
        return project.get();
    }

    public void setProject(Project project) {
        this.project = Ref.create(project);
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }
}
