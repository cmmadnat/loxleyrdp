package com.loxley.rdpmap.domain;

import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.annotation.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RoyalProject implements Serializable {

    @Ignore
    String icon;

    Long id;
    String name;
    GeoPt location = new GeoPt(0, 0);


    private List<RoyalProjectImage> images = new ArrayList<RoyalProjectImage>();
    ArrayList<RoyalProjectFile> files = new ArrayList<RoyalProjectFile>();

    ArrayList<String> departments = new ArrayList<String>();

    String royal, category, activityCategory, province, district, subDistrict,
            url;
    Long categoryId, activityCategoryId, royalId, projectOwnerId;
    Boolean publish;
    Date createDate = new Date(), modifiedDate = new Date();

    String detail;

    String villageName, address;

    String region;

    String status;
    String source;

    String result;

    private Integer areaRainy;
    private Integer areaSummer;
    private Integer village;
    private Integer household;
    private Integer citizen;

    String ministry;

    Integer year;
    private boolean isMainProject;

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<String> getDepartments() {
        return departments;
    }

    public void setDepartments(ArrayList<String> departments) {
        this.departments = departments;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getActivityCategory() {
        return activityCategory;
    }

    public Long getActivityCategoryId() {
        return activityCategoryId;
    }

    public String getCategory() {
        return category;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getDistrict() {
        return district;
    }

    public Long getId() {
        return id;
    }

    public List<RoyalProjectImage> getImages() {
        return images;
    }

    public GeoPt getLocation() {
        return location;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getName() {
        return name;
    }

    public Long getProjectOwnerId() {
        return projectOwnerId;
    }

    public String getProvince() {
        return province;
    }

    public Boolean getPublish() {
        return publish;
    }

    public String getRoyal() {
        return royal;
    }

    public Long getRoyalId() {
        return royalId;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public String getUrl() {
        return url;
    }

    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    public void setActivityCategoryId(Long activityCategoryId) {
        this.activityCategoryId = activityCategoryId;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setImages(List<RoyalProjectImage> images) {
        this.images = images;
    }

    public void setLocation(GeoPt location) {
        this.location = location;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProjectOwnerId(Long projectOwnerId) {
        this.projectOwnerId = projectOwnerId;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public void setRoyal(String royal) {
        this.royal = royal;
    }

    public void setRoyalId(Long royalId) {
        this.royalId = royalId;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getAreaRainy() {
        return areaRainy;
    }

    public void setAreaRainy(Integer areaRainy) {
        this.areaRainy = areaRainy;
    }

    public Integer getAreaSummer() {
        return areaSummer;
    }

    public void setAreaSummer(Integer areaSummer) {
        this.areaSummer = areaSummer;
    }

    public Integer getVillage() {
        return village;
    }

    public void setVillage(Integer village) {
        this.village = village;
    }

    public Integer getHousehold() {
        return household;
    }

    public void setHousehold(Integer household) {
        this.household = household;
    }

    public Integer getCitizen() {
        return citizen;
    }

    public void setCitizen(Integer citizen) {
        this.citizen = citizen;
    }

    public String getMinistry() {
        return ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setIsMainProject(boolean isMainProject) {
        this.isMainProject = isMainProject;
    }

    public boolean isMainProject() {
        return isMainProject;
    }

    public ArrayList<RoyalProjectFile> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<RoyalProjectFile> files) {
        this.files = files;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
