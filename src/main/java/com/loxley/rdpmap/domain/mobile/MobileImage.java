package com.loxley.rdpmap.domain.mobile;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.old.Project;

import java.util.Date;

/**
 * Created by cmmad_000 on 5/21/2015.
 */
@Entity
public class MobileImage {
    @Id
    Long id;
    private String key;
    private String servingUrl;
    private String username;

    Ref<Project> project;


    @Index
    Date date = new Date();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setServingUrl(String servingUrl) {
        this.servingUrl = servingUrl;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getKey() {
        return key;
    }

    public String getServingUrl() {
        return servingUrl;
    }

    public String getUsername() {
        return username;
    }


    public Project getProject() {
        return project.get();
    }

    public void setProject(Project project) {
        this.project = Ref.create(project);
    }
}
