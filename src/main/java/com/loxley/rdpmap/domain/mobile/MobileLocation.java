package com.loxley.rdpmap.domain.mobile;

import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.loxley.rdpmap.domain.old.Project;

import java.util.Date;

/**
 * Created by cmmad_000 on 5/21/2015.
 */
@Entity
public class MobileLocation {
    @Id
    Long id;
    private GeoPt location;
    private String username;
    private Ref<Project> project;

    @Index
    Date date = new Date();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLocation(GeoPt location) {
        this.location = location;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public GeoPt getLocation() {
        return location;
    }

    public String getUsername() {
        return username;
    }

    public Project getProject() {
        return project.get();
    }

    public void setProject(Project project) {
        this.project = Ref.create(project);
    }
}

