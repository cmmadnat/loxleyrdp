package com.loxley.rdpmap.domain;

import java.io.Serializable;

public class RoyalProjectImage implements Serializable {
	private String serveURL;
	String key;

	String caption = "";


	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getServeURL() {
		return serveURL;
	}

	public void setServeURL(String serveURL) {
		this.serveURL = serveURL;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
}
