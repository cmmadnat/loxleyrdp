package com.loxley.rdpmap.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.Date;

@Entity
public class ProjectOwner {
	@Id
	Long id;
	String username, email, password, firstName, lastName, salt;
	Boolean admin;
	private Integer gender;

	Date registerDate = new Date(), modifiedDate = new Date();

	public Boolean getAdmin() {
		return admin;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public Integer getGender() {
		return gender;
	}

	public Long getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public String getPassword() {
		return password;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public String getSalt() {
		return salt;
	}

	public String getUsername() {
		return username;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
