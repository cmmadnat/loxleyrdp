package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Idea;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;

/**
 * Created by thitiwat on 4/19/15.
 */
public class ListIdeaToString implements Converter<ArrayList<Idea>, String> {
    @Override
    public String convert(ArrayList<Idea> ideas) {
        String target = "";

        for (Idea idea : ideas) {
            target += idea.getUuid() + ",";
        }

        if (target.length() > 0)
            return target.substring(0, target.length() - 1);
        else
            return "";
    }
}
