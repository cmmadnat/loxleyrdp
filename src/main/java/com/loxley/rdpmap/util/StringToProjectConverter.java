package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Project;
import com.loxley.rdpmap.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by cmmad_000 on 3/22/2015.
 */
public class StringToProjectConverter implements Converter<String, Project> {
    @Autowired
    ProjectService projectService;

    @Override
    public Project convert(String aLong) {
        return projectService.findOne(Long.valueOf(aLong));
    }
}
