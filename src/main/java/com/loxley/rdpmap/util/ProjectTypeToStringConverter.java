package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.ProjectType;
import com.loxley.rdpmap.domain.old.embedded.EProjectType;
import com.loxley.rdpmap.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

/**
 * Created by thitiwat on 3/22/15.
 */
public class ProjectTypeToStringConverter implements Converter<EProjectType, String> {
    @Autowired
    ProjectService projectService;

    @Override
    public String convert(EProjectType projectType) {

        if (projectType.getUuid() == null) {
            List<ProjectType> list = projectService.findAllType();
            for (ProjectType type : list) {
                if (type.getId() == projectType.getId()) {
                    projectType.setUuid(type.getUuid());
                }
            }
        }
        if (projectType.getUuid() == null) return null;
        return projectType.getUuid().toString();
    }
}
