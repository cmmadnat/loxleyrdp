package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Province;
import com.loxley.rdpmap.domain.old.embedded.EProvince;
import com.loxley.rdpmap.service.LocationService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by thitiwat on 3/23/15.
 */
public class StringToProvinceConverter implements Converter<String, EProvince> {
    @Autowired
    LocationService locationService;

    @Override
    public EProvince convert(String s) {
        if (s.isEmpty()) return new EProvince();
        Province province = locationService.findOneProvince(Long.valueOf(s));
        EProvince target = new EProvince();
        try {
            BeanUtils.copyProperties(target, province);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return target;
    }
}
