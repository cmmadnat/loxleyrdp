package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Royalty;
import com.loxley.rdpmap.domain.old.embedded.ERoyalty;
import com.loxley.rdpmap.service.RoyaltyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

/**
 * Created by thitiwat on 3/22/15.
 */
public class RoyaltyToStringConverter implements Converter<ERoyalty, String> {

    @Autowired
    RoyaltyService royaltyService;

    @Override
    public String convert(ERoyalty eRoyalty) {
        if (eRoyalty.getUuid() == null){
            List<Royalty> list = royaltyService.findAll();
            for (Royalty royalty : list) {
                if (royalty.getId() == eRoyalty.getId()){
                    eRoyalty.setUuid(royalty.getUuid());
                }
            }
        }
        return eRoyalty.getUuid().toString();
    }
}
