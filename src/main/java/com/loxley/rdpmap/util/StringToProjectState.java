package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.embedded.EProjectState;
import com.loxley.rdpmap.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;

/**
 * Created by thitiwat on 3/22/15.
 */
public class StringToProjectState implements Converter<String, EProjectState> {
    @Autowired
    ProjectService projectService;

    @Override
    public EProjectState convert(String s) {
        if (s.isEmpty()) return null;
        ArrayList<EProjectState> projectStates = projectService.getProjectStates();
        for (EProjectState projectState : projectStates) {
            if (projectState.toString().equals(s)) return projectState;
        }
        return null;
    }
}
