package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Department;
import com.loxley.rdpmap.domain.old.embedded.EDepartment;
import com.loxley.rdpmap.service.DepartmentService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by cmmad_000 on 3/28/2015.
 */
public class StringToDepartmentConverter implements Converter<String, EDepartment> {
    @Autowired
    DepartmentService departmentService;

    @Override
    public EDepartment convert(String s) {
        Department department = departmentService.findOne(Long.valueOf(s));
        EDepartment target = new EDepartment();
        try {
            BeanUtils.copyProperties(target, department);
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
        return target;
    }
}
