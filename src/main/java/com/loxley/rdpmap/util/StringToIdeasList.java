package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Idea;
import com.loxley.rdpmap.service.IdeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;

/**
 * Created by thitiwat on 4/19/15.
 */
public class StringToIdeasList implements Converter<String, ArrayList<Idea>> {
    @Autowired
    IdeaService ideaService;

    @Override
    public ArrayList<Idea> convert(String s) {
        ArrayList<Idea> target = new ArrayList<Idea>();

        String[] split = s.split(",");
        for (String s1 : split) {
            Idea one = ideaService.findOne(Long.valueOf(s1));
            if (one != null) {
                target.add(one);

            }
        }

        return target;
    }
}
