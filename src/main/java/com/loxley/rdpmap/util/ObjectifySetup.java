package com.loxley.rdpmap.util;

import com.googlecode.objectify.ObjectifyService;
import com.loxley.rdpmap.domain.LogEntry;
import com.loxley.rdpmap.domain.ProjectTracker;
import com.loxley.rdpmap.domain.Setting;
import com.loxley.rdpmap.domain.mobile.MobileImage;
import com.loxley.rdpmap.domain.mobile.MobileLocation;
import com.loxley.rdpmap.domain.old.*;

import javax.annotation.PostConstruct;

public class ObjectifySetup {
    @PostConstruct
    public void init() {
        ObjectifyService.register(Project.class);
        ObjectifyService.register(Setting.class);
        ObjectifyService.register(Amphur.class);
        ObjectifyService.register(Province.class);
        ObjectifyService.register(Region.class);
        ObjectifyService.register(District.class);
        ObjectifyService.register(ProjectType.class);
        ObjectifyService.register(Royalty.class);
        ObjectifyService.register(Department.class);

        // idea
        ObjectifyService.register(Idea.class);

        // tracker
        ObjectifyService.register(ProjectTracker.class);

        // mobile
        ObjectifyService.register(MobileImage.class);
        ObjectifyService.register(MobileLocation.class);

        // km
        ObjectifyService.register(Knowledge.class);
        ObjectifyService.register(KnowledgeType.class);

        // log
        ObjectifyService.register(LogEntry.class);


    }
}
