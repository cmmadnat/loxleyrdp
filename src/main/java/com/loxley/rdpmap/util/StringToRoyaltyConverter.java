package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Royalty;
import com.loxley.rdpmap.domain.old.embedded.ERoyalty;
import com.loxley.rdpmap.service.RoyaltyService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by thitiwat on 3/22/15.
 */
public class StringToRoyaltyConverter implements Converter<String, ERoyalty> {
    @Autowired
    RoyaltyService royaltyService;

    @Override
    public ERoyalty convert(String s) {
        if (s.isEmpty()) return null;
        Royalty royalty = royaltyService.findOne(Long.valueOf(s));
        ERoyalty eRoyalty = new ERoyalty();

        try {
            BeanUtils.copyProperties(eRoyalty, royalty);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return eRoyalty;
    }
}
