package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.ProjectType;
import com.loxley.rdpmap.domain.old.embedded.EProjectType;
import com.loxley.rdpmap.service.ProjectService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by thitiwat on 3/22/15.
 */
public class StringToProjectTypeConverter implements Converter<String, EProjectType> {
    @Autowired
    ProjectService projectService;

    @Override
    public EProjectType convert(String s) {
        if (s.isEmpty()) return null;
        ProjectType type = projectService.findOneType(Long.valueOf(s));
        EProjectType target = new EProjectType();
        try {
            BeanUtils.copyProperties(target, type);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();

        }
        return target;
    }
}
