package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.Amphur;
import com.loxley.rdpmap.domain.old.embedded.EAmphur;
import com.loxley.rdpmap.service.LocationService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by thitiwat on 3/22/15.
 */
public class StringToAmphurConverter implements Converter<String, EAmphur> {
    @Autowired
    LocationService locationService;

    @Override
    public EAmphur convert(String s) {
        if (s.isEmpty()) return new EAmphur();

        Amphur amphur = locationService.findOneAmphur(Long.valueOf(s));
        EAmphur eAmphur = new EAmphur();

        try {
            BeanUtils.copyProperties(eAmphur, amphur);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return eAmphur;
    }
}
