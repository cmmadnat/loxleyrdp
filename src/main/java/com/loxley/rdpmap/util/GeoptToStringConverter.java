package com.loxley.rdpmap.util;

import com.google.appengine.api.datastore.GeoPt;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by cmmad_000 on 3/29/2015.
 */
public class GeoptToStringConverter implements Converter<GeoPt, String> {

    @Override
    public String convert(GeoPt geoPt) {
        return geoPt.getLatitude() + "," + geoPt.getLongitude();
    }
}
