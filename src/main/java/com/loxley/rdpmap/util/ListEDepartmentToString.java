package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.embedded.EDepartment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cmmad_000 on 3/28/2015.
 */
public class ListEDepartmentToString implements Converter<List<EDepartment>, String> {
    @Override
    public String convert(List<EDepartment> eDepartments) {
        try {
            ArrayList<String> output = new ArrayList<String>();
            for (EDepartment eDepartment : eDepartments) {
                output.add(String.valueOf(eDepartment.getUuid()));
            }
            String join = StringUtils.join(output);
            return join.substring(1, join.length() - 1);
        } catch (ClassCastException e) {
            return "";
        }
    }
}
