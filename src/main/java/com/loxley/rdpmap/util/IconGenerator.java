package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.RoyalProjectFile;

/**
 * Created by thitiwat on 9/2/15.
 */
public class IconGenerator {

    public String getIcon(String fileName) {
        String output = "";

        if (fileName.endsWith("pdf"))
            output = "fa fa-file-pdf-o";
        else if (fileName.toLowerCase().endsWith("doc") || fileName.toLowerCase().endsWith("docx"))
            output = "fa fa-file-word-o";
        else if (fileName.toLowerCase().endsWith("xls") || fileName.toLowerCase().endsWith("xlsx"))
            output = "fa fa-file-excel-o";
        else if (fileName.toLowerCase().endsWith("ppt") || fileName.toLowerCase().endsWith("pptx"))
            output = "fa fa-file-powerpoint-o";
        else if (fileName.toLowerCase().endsWith("zip") || fileName.toLowerCase().endsWith("rar"))
            output = "fa fa-file-zip-o";
        else if (fileName.toLowerCase().endsWith("jpg") || fileName.toLowerCase().endsWith("jpeg") || fileName.toLowerCase().endsWith("png") || fileName.toLowerCase().endsWith("gif"))
            output = "fa fa-file-photo-o";
        else output = "fa fa-file-o";

        return output;
    }

    public String getIconColor(String fileName) {
        String output = "";

        if (fileName.endsWith("pdf"))
            output = "#BB0706";
        else if (fileName.toLowerCase().endsWith("doc") || fileName.toLowerCase().endsWith("docx"))
            output = "#2A5699";
        else if (fileName.toLowerCase().endsWith("xls") || fileName.toLowerCase().endsWith("xlsx"))
            output = "#207245";
        else if (fileName.toLowerCase().endsWith("ppt") || fileName.toLowerCase().endsWith("pptx"))
//            output = "#D04727";
            output = "#FF9901";
        else if (fileName.toLowerCase().endsWith("zip") || fileName.toLowerCase().endsWith("rar"))
            output = "purple";
        else if (fileName.toLowerCase().endsWith("jpg") || fileName.toLowerCase().endsWith("jpeg") || fileName.toLowerCase().endsWith("png") || fileName.toLowerCase().endsWith("gif"))
            output = "darkblue";
        else output = "gray";

        return output;
    }


}
