package com.loxley.rdpmap.util;

import com.google.appengine.api.datastore.GeoPt;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by cmmad_000 on 3/29/2015.
 */
public class StringToGeoptConverter implements Converter<String, GeoPt> {


    @Override
    public GeoPt convert(String s) {
        String[] split = s.split(",");
        return new GeoPt(Float.valueOf(split[0]), Float.valueOf(split[1]));
    }
}

