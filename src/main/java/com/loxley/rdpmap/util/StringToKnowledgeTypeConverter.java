package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.KnowledgeType;
import com.loxley.rdpmap.domain.old.embedded.EKnowledgeType;
import com.loxley.rdpmap.domain.old.embedded.EProjectType;
import com.loxley.rdpmap.service.KnowledgeService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by thitiwat on 3/22/15.
 */
public class StringToKnowledgeTypeConverter implements Converter<String, EKnowledgeType> {
    @Autowired
    KnowledgeService knowledgeService;

    @Override
    public EKnowledgeType convert(String s) {
        if (s.isEmpty()) return null;
        KnowledgeType type = knowledgeService.findOneType(Long.valueOf(s));
        EKnowledgeType target = new EKnowledgeType();
        try {
            BeanUtils.copyProperties(target, type);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();

        }
        return target;
    }
}
