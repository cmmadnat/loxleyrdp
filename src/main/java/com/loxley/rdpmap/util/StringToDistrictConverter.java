package com.loxley.rdpmap.util;

import com.loxley.rdpmap.domain.old.District;
import com.loxley.rdpmap.domain.old.embedded.EDistrict;
import com.loxley.rdpmap.service.LocationService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by cmmad_000 on 3/28/2015.
 */
public class StringToDistrictConverter implements Converter<String, EDistrict> {
    @Autowired
    LocationService locationService;

    @Override
    public EDistrict convert(String s) {
        if (s.isEmpty()) return new EDistrict();

        District district = locationService.findOneDistrict(Long.valueOf(s));
        EDistrict eDistrict = new EDistrict();
        try {
            BeanUtils.copyProperties(eDistrict, district);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return eDistrict;
    }
}
