package com.loxley.rdpmap.util;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by cmmad_000 on 4/9/2015.
 */
public class ThaiDateFormat {
    static String[] months = {"มกราคม", "กุมภาพันธ์", "มีนาคม",
            "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน",
            "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

    public static String[] getMonths() {
        return months;
    }

    public static String formatDateFull(Date date) {
        DateTime dateTime = new DateTime(date);

        return dateTime.getDayOfMonth() + " " + getMonth(dateTime.getMonthOfYear()) + " " + (dateTime.getYear() + 543);
    }

    public static String formatDateShort(Date date) {
        DateTime dateTime = new DateTime(date);

        return (dateTime.getDayOfMonth() < 10 ? "0" : "") + dateTime.getDayOfMonth() + "/" + (dateTime.getMonthOfYear() < 10 ? "0" : "") + (dateTime.getMonthOfYear()) + "/" + (dateTime.getYear() + 543);
    }

    private static String getMonthShort(int monthOfYear) {
        String[] months = {"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.",
                "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."};
        return months[monthOfYear - 1];
    }

    public static String getMonth(int monthOfYear) {

        return months[monthOfYear - 1];
    }
}
