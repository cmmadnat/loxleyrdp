package com.loxley.rdpmap.domain;


import com.loxley.rdpmap.domain.old.embedded.EAmphur;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.Columns;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.datatype.DataTypes;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-context.xml")
public class ReportTest {
    @Test
    @Ignore
    public void testReport() {
        Assert.assertTrue(true);

        JasperReportBuilder report = DynamicReports.report();
        JRDataSource dataSource = new DataSource();
        report
                .columns(
                        Columns.column("Id", "id", DataTypes.integerType()),
                        Columns.column("Name", "name", DataTypes.stringType()))
                .title(//title of the report
                        Components.text("SimpleReportExample")
                                .setHorizontalAlignment(HorizontalAlignment.CENTER))
                .pageFooter(Components.pageXofY())//show page number on the page footer
                .setDataSource(dataSource);

        try {
            //show the report
//            report.show();

            //export the report to a pdf file
            File file = File.createTempFile("something", ".pdf");
            report.toPdf(new FileOutputStream(file));
            Desktop.getDesktop().open(file);
        } catch (DRException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    class DataSource implements JRDataSource {

        private int cursor;
        ArrayList<EAmphur> amphurs = new ArrayList<EAmphur>();

        public DataSource() {
            EAmphur amphur1 = new EAmphur();
            EAmphur amphur2 = new EAmphur();

            amphur1.setName("Bangkae");
            amphur2.setName("Bangrak");
            amphurs.add(amphur1);
            amphurs.add(amphur2);
            cursor = -1;
        }

        @Override
        public boolean next() throws JRException {
            return ++cursor < amphurs.size();
        }

        @Override
        public Object getFieldValue(JRField jrField) throws JRException {

            String fieldName = jrField.getName();
            EAmphur current = amphurs.get(cursor);
            if (fieldName.equals("id")) return current.getId();
            else return current.getName();
        }
    }
}