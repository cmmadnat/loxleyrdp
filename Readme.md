# RDP map
## Sample
* โครงการในพระองค์ : พระบาทสมเด็จพระเจ้าอยู่หัว / Royal
* ชื่อโครงการ : ศูนย์ศึกษาการพัฒนาภูพานอันเนื่องมาจากพระราชดำริ อำเภอเมือง จังหวัดสกลนคร 
* ประเภท : โครงการอันเนื่องมาจากพระราชดำริ / Category
* ประเภทกิจกรรม : อื่นๆ / ActivityCategory
* จังหวัด : สกลนคร / Province
* อำเภอ : เมืองสกลนคร / District
* ตำบล : ห้วยยาง / SubDistrict
* Latitude : 17.04 
* Longitude : 104 
* เผยแพร่ : ใช่ / Publish
* URL : http://km.rdpb.go.th/Project/View/7103 / url 
* วันที่ปรับปรุงล่าสุด : 19/11/2556 - 12:43 / modifiedDate

## Meeting

### 12/11/2014
New attributes : 
* กิจกรรมย่อย ในโครงการ
* เป็นโครงการย่อยในโครงการใหญ่
* โครงการเสร็จรึยัง
New Features
* TOPO -> lat long conversion
* Find the way to pull data from KM

เป็น แบบที่ สอง (ย้ายทั้งหมด)
1. โครงการ / กิจกรรม
2. พระราชดำริ
3. องค์ความรู้

เพิ่มเติม
* โครงการกับกิจกรรม เหมือนกัน (ต่างกันแค่ชื่อนิดหน่อย)
* โครงการ สามารถมี แม่ได้ และต้องมี relationship กันได้
* โครงการ ต้องมี field เกี่ยวกับเรื่องที่ว่า จะนับไหมว่าเป็นหนึ่งโครงการ
* โครงการ จะต้องมี field ใหม่ทั้งหมด รวมไปถึง html ที่ใช้แสดงผล (ไม่ตัดอะไรทั้งสิ้น)

version
* 1.2
** add filter region
** add excel download button
* 1.0
** release

-----------
* 0.37
** change exporter location
** change the tracking ui
* 0.36
** change project state name
* 0.35
** switch to light theme
* 0.34
** fix ajax cache bug
** add interface enhancement
* 0.33
** fix bug
* 0.32
** add version
** fix bugs and ui
** add project status report
* 0.31
** fix  ui and report
* 0.30
** fix bug
** add project tracking caption
* 0.29
** add right
** add is main to royal project
* 0.28
** add icon to royal project service
* 0.27
** bug fixes
* 0.26 
** fix design
* 0.25
** fix interfaces
* 0.24
** fix from meeting
* 0.23
** add pdf rdp report
* 0.22
** change to slick
** add caption to image
* 0.21
** fix first page, first page service
* 0.20
** add country
* 0.19
** fix ui 
* 0.18
** fix ui and integrate designer changes
* 0.17
** pure bug fixes
** prepare for pdf display conversion
* 0.16
** fix various design issue
** rearrange dashboard
* 0.15
** fix design and many improvment to layout
* 0.14
** update to new design
* 0.13
** add several new report
** add log service to log 20 events
** fix dashboard
* 0.12
** add user management setting
* 0.11
** switch to role based login system
* 0.10
** add multiple improvement to the front page
** add new import the previous image feature
* 0.9
** add real report sample
** various fix to front page layout and wording
** add index to the location of the project so map can be search more convenient
* 0.8
** improve front page search
** various improvement to the map
** fix layout to match the style
* 0.7
** add dashboard table tab
* 0.6
** add dashboard graph and chart
** add google analytics